package tests

import v1 "binan/internal/controller/http/v1"

// import (
//
//	"bytes"
//	"context"
//	"encoding/json"
//	"fmt"
//	"github.com/gin-gonic/gin"
//	"github.com/go-playground/validator/v10"
//	"log"
//	"net/http"
//	"net/http/httptest"
//	"os"
//	"testing"
//	"binan/config"
//	v1 "binan/internal/controller/http/v1"
//	"binan/internal/entity"
//	"binan/internal/usecase"
//	"binan/internal/usecase/repo"
//	"binan/internal/usecase/repo/seeding"
//	"binan/pkg/logger"
//	pgDb "binan/pkg/postgres"
//
// )
//
// var pg *pgDb.Postgres
var userRoute *v1.UserRoutes

//
//func TestMain(m *testing.M) {
//	Database()
//	userUseCase := usecase.NewUserUseCase(
//		repo.NewUserRepo(pg),
//		repo.NewTOSRepo(pg),
//	)
//	l := logger.New("debug")
//	v := validator.New()
//	userRoute = &v1.UserRoutes{
//		U: userUseCase,
//		L: l,
//		V: v,
//	}
//
//	os.Exit(m.Run())
//}
//
//func Database() {
//	cfg, _ := config.Get()
//	fmt.Println(cfg)
//
//	var err error
//	//pg, err = pgDb.New("postgres://postgres:backhug@localhost:55432/tomokari-test?sslmode=disable", pgDb.MaxPoolSize(3))
//	pg, err = pgDb.New(cfg.PG.TestUrl, pgDb.MaxPoolSize(3))
//	if err != nil {
//		fmt.Printf("Cannot connect to %s database\n", "postgres")
//		log.Fatal("This is the error:", err)
//	} else {
//		fmt.Printf("We are connected to the %s database\n", "postgres")
//	}
//
//	_ = deleteData(entity.UserTable)
//	_ = seeding.SeedUsers(pg)
//	_ = seeding.SeedTos(pg)
//}
//
//func deleteData(tableName string) error {
//	query, args, err := pg.Builder.Delete(tableName).ToSql()
//	if err != nil {
//		return err
//	}
//	res, err := pg.Pool.Exec(context.Background(), query, args...)
//	if err != nil {
//		return err
//	}
//	log.Printf("%d rows deleted from %s table", res.RowsAffected(), tableName)
//	return nil
//}
//
//func refreshUserTable() error {
//	query, args, err := pg.Builder.Delete("users").ToSql()
//	if err != nil {
//		return err
//	}
//	res, err := pg.Pool.Exec(context.Background(), query, args...)
//	if err != nil {
//		return err
//	}
//	log.Printf("%d rows deleted from users table", res.RowsAffected())
//	return nil
//}
//
//func userLogin() string {
//	var buf bytes.Buffer
//	_ = json.NewEncoder(&buf).Encode(seeding.Users[0])
//	req, err := http.NewRequest("POST", "/login", &buf)
//	if err != nil {
//		log.Fatalf("this is the error: %v", err)
//	}
//	recorder := httptest.NewRecorder()
//	handler := gin.New()
//	handler.POST("/login", userRoute.Login)
//	handler.ServeHTTP(recorder, req)
//
//	body := recorder.Body
//	var res map[string]interface{}
//	_ = json.Unmarshal(body.Bytes(), &res)
//	return res["data"].(map[string]interface{})["accessToken"].(string)
//}
