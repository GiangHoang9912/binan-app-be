package tests

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/usecase/repo/seeding"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/assert.v1"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestRegister(t *testing.T) {
	samples := []struct {
		inputJson  entity.PreRegReqBody
		statusCode int
		message    string
	}{
		{
			inputJson: entity.PreRegReqBody{
				Email:    "user",
				Password: "",
			},
			statusCode: http.StatusBadRequest,
			message:    "Error:Field validation",
		},
		{
			inputJson: entity.PreRegReqBody{
				Email:    "usertest1@gmail.com",
				Password: "pass12",
			},
			statusCode: http.StatusCreated,
			message:    "",
		},
	}

	for _, sample := range samples {
		var buf bytes.Buffer
		_ = json.NewEncoder(&buf).Encode(sample.inputJson)
		req, err := http.NewRequest("POST", "/register", &buf)
		if err != nil {
			t.Errorf("this is the error: %v", err)
		}
		recorder := httptest.NewRecorder()
		handler := gin.New()
		handler.POST("/register", userRoute.PreRegister)
		handler.ServeHTTP(recorder, req)

		body := recorder.Body
		var res map[string]interface{}
		_ = json.Unmarshal(body.Bytes(), &res)
		fmt.Println(res)
		assert.Equal(t, strings.Contains(res["message"].(string), sample.message), true)
		assert.Equal(t, recorder.Code, sample.statusCode)
	}
}

func TestLogin(t *testing.T) {
	// invalid credentials
	var buf bytes.Buffer
	inputJson := struct {
	}{}

	_ = json.NewEncoder(&buf).Encode(inputJson)
	req, err := http.NewRequest("POST", "/login", &buf)
	if err != nil {
		t.Errorf("this is the error: %v", err)
	}
	recorder := httptest.NewRecorder()
	handler := gin.New()
	handler.POST("/login", userRoute.Login)
	handler.ServeHTTP(recorder, req)

	body := recorder.Body
	var res map[string]interface{}
	_ = json.Unmarshal(body.Bytes(), &res)
	assert.Equal(t, res["success"], false)
	assert.Equal(t, recorder.Code, http.StatusUnauthorized)

	// valid credentials
	email, pass := seeding.Users[0].Email, seeding.Users[0].Password
	_ = json.NewEncoder(&buf).Encode(entity.LoginUserReqBody{
		Email:    email,
		Password: pass,
	})
	req, err = http.NewRequest("POST", "/login", &buf)
	if err != nil {
		t.Errorf("this is the error: %v", err)
	}
	recorder = httptest.NewRecorder()
	handler = gin.New()
	handler.POST("/login", userRoute.Login)
	handler.ServeHTTP(recorder, req)

	body = recorder.Body
	_ = json.Unmarshal(body.Bytes(), &res)
	data, _ := res["data"].(map[string]interface{})
	_, ok := data["accessToken"]
	assert.Equal(t, data["email"], email)
	assert.Equal(t, ok, true)
	assert.Equal(t, recorder.Code, http.StatusOK)
}

func TestGetByID(t *testing.T) {
	req, err := http.NewRequest("GET", "/1", nil)
	if err != nil {
		t.Errorf("new request error: %v", err)
	}
	recorder := httptest.NewRecorder()
	handler := gin.New()
	handler.GET("/:id", userRoute.GetUserByID)
	handler.ServeHTTP(recorder, req)

	body := recorder.Body
	var res map[string]interface{}
	_ = json.Unmarshal(body.Bytes(), &res)
	fmt.Println(res)
	assert.Equal(t, res["success"], true)
	assert.Equal(t, recorder.Code, http.StatusOK)

	// not found
	req, err = http.NewRequest("GET", "/1000", nil)
	if err != nil {
		t.Errorf("new request error: %v", err)
	}
	recorder = httptest.NewRecorder()
	handler = gin.New()
	handler.GET("/:id", userRoute.GetUserByID)
	handler.ServeHTTP(recorder, req)

	body = recorder.Body
	//var res map[string]interface{}
	_ = json.Unmarshal(body.Bytes(), &res)
	fmt.Println(res)
	assert.Equal(t, res["success"], false)
	assert.Equal(t, res["message"], constants.UserNotFoundMessage)
	assert.Equal(t, recorder.Code, http.StatusNotFound)
}
