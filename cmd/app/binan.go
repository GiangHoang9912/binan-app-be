package main

import (
	"binan/config"
	"binan/internal/app"
	"log"
)

func main() {
	// Configuration
	cfg, err := config.Get()
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	// Run
	app.Run(cfg)
}
