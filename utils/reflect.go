package utils

import "reflect"

func GetAllTagsAndValues(data any, tagName string) map[string]any {
	//var tags []string
	//var values []any
	res := make(map[string]interface{})
	t := reflect.TypeOf(data)
	v := reflect.ValueOf(data)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
		v = v.Elem()
	}
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		val := v.Field(i)
		tag := field.Tag.Get(tagName)
		if tag != "" {
			res[tag] = val
			//tags = append(tags, tag)
			//values = append(values, val)
		}
	}
	//return tags, values
	return res
}
