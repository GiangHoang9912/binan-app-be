package utils

import (
	"binan/config"
	"binan/internal/constants"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"time"
)

type JwtClaims struct {
	jwt.StandardClaims
	Data interface{}
}

const (
	AccessTokenExpirationHours  = 1
	RefreshTokenExpirationHours = 24 * 7
)

func GenerateToken(data interface{}, tokenType string) (signedToken string, err error) {
	var expiresAt int64
	now := time.Now().Local()
	nowUnix := now.Unix()
	fmt.Printf("Now: %+v\n", nowUnix)
	cfg, err := config.Get()
	if tokenType == constants.AccessToken {
		expiresAt = now.Add(time.Hour * time.Duration(AccessTokenExpirationHours)).Unix()
	} else {
		expiresAt = now.Add(time.Hour * time.Duration(RefreshTokenExpirationHours)).Unix()
	}
	claims := &JwtClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiresAt,
			Issuer:    "tomokari",
		},
		Data: data,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err = token.SignedString([]byte(cfg.JWT.SecretKey))

	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func ValidateToken(signedToken string) (claims *JwtClaims, err error) {
	cfg, _ := config.Get()
	token, err := jwt.ParseWithClaims(
		signedToken,
		&JwtClaims{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(cfg.JWT.SecretKey), nil
		},
	)

	if err != nil {
		return
	}

	claims, ok := token.Claims.(*JwtClaims)

	if !ok {
		return nil, errors.New("couldn't parse claims")
	}

	if claims.ExpiresAt < time.Now().Local().Unix() {
		return nil, errors.New("JWT is expired")
	}

	return claims, nil
}
