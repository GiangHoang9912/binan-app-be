package constants

const (
	DuplicatedEmailErrorMessage           = "this email is already in used"
	DuplicatedPhoneErrorMessage           = "this phone number is already in used"
	TermsOfServiceNotAcceptedErrorMessage = "please accept the terms of service"
	InternalServerErrorMessage            = "some error occurred"
	IncorrectUserCredentialsErrorMessage  = "incorrect credentials"
	Invalid2faCode                        = "invalid 2fa code"
	InvalidVerificationCode               = "invalid verification code"
	UserNotFoundMessage                   = "user not found"
	WalletNotFoundMessage                 = "wallet not found"
	UnAuthorizedErrorMessage              = "unauthorized"
	UserCreatedSuccessfullyMessage        = "user created successfully"
	UserLoginSuccessfullyMessage          = "user login successfully"
	UserInfoErrorMessage                  = "please re-check the information"
	InvalidRefCode                        = "invalid referral code"
	InvalidMaster                         = "invalid master"
	ForbiddenMessage                      = "you don't have permission for this operation"
	NotFound                              = "Not found"
	NotEnoughBalanceToCommission          = "Not Enough Balance To Commission"
)

const (
	AccessToken  = "access_token"
	RefreshToken = "refresh_token"
)
