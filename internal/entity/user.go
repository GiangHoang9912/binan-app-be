package entity

import (
	"binan/internal/enums"
	"binan/utils"
	"time"

	"github.com/pquerna/otp/totp"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	ID uint `gorm:"primarykey" json:"id"`

	Email                string         `json:"email" gorm:"unique;type:varchar(300);not null"`
	Phone                string         `json:"phone" gorm:"unique;type:varchar(50)"`
	Address              string         `json:"address"`
	Password             string         `json:"password" gorm:"type:varchar(255);not null"`
	Role                 enums.UserRole `json:"role"`
	Name                 string         `json:"name" gorm:"type:varchar(100)"`
	GoogleId             string         `json:"googleId" gorm:"column:google_id"`
	TradeAccount         string         `json:"tradeAccount" gorm:"column:trade_account"`
	Balance              float32        `json:"balance" gorm:"column:balance_usdt"`
	BalanceUsdt          float32        `json:"balanceUsdt"`
	ReferralCode         string         `json:"referralCode" gorm:"unique;type:varchar(15);column:referral_code"`
	EmailVerified        bool           `json:"emailVerified" gorm:"column:email_verified"`
	PhoneVerified        bool           `json:"phoneVerified" gorm:"column:phone_verified"`
	AuthenticatorEnabled bool           `json:"authenticatorEnabled" gorm:"column:authenticator_enabled"`
	OtpAuthBackupKey     string         `json:"otpAuthBackupKey" gorm:"column:otp_auth_backup_key"`
	OtpAuthSecret        string         `json:"otpSecret" gorm:"column:otp_auth_secret"`
	OtpAuthUrl           string         `json:"otpAuthUrl" gorm:"otp_auth_url"`

	LevelId uint `json:"levelId" gorm:"column:level_id" form:"level_id"`

	TimeStamp
}

func (*User) TableName() string {
	return "users"
}

func (u *User) AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(u)
}

func (u *User) BeforeCreate(_ *gorm.DB) (err error) {
	u.ReferralCode = utils.RandomString(12)
	return
}

func (u *User) HashPassword() {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	u.Password = string(bytes)
}

func (u *User) CheckHashPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	return err == nil
}

func (u *User) Check2fa(authCode string) bool {
	if !u.AuthenticatorEnabled {
		return true
	}

	valid := totp.Validate(authCode, u.OtpAuthSecret)
	if !valid && !(authCode == u.OtpAuthBackupKey && u.OtpAuthBackupKey != "") {
		return false
	}
	return true
}

type Token struct {
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

type UserTokenData struct {
	ID    uint64         `json:"id"`
	Role  enums.UserRole `json:"role"`
	Email string         `json:"email"`
	Phone string         `json:"phone"`
}

type PreRegReqBody struct {
	Email string `json:"email" validate:"required,email"`
}

type RegReqBody struct {
	PreRegReqBody
	Password         string `json:"password" validate:"required,gt=5"`
	ConfirmationCode string `json:"confirmationCode"`
}

type LoginUserReqBody struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
	AuthCode string `json:"authCode"`
}

type ChangePwInput struct {
	CurrentPassword string `json:"currentPassword" validate:"required"`
	NewPassword     string `json:"newPassword" validate:"required"`
	ConfirmPassword string `json:"confirmPassword" validate:"required"`
}

type ForgetPwInput struct {
	Email                 string `json:"email" validate:"required"`
	EmailVerificationCode string `json:"emailVerificationCode" validate:"required"`
	NewPassword           string `json:"newPassword" validate:"required"`
	ConfirmPassword       string `json:"confirmPassword" validate:"required"`
}

type CreateAccessTokenReqBody struct {
	RefreshToken string `json:"refreshToken"`
}

type UserInfo struct {
	ID                   uint           `json:"id"`
	Role                 enums.UserRole `json:"role"`
	Email                string         `json:"email" validate:"required,email"`
	Phone                string         `json:"phone" validate:"required"`
	AuthenticatorEnabled bool           `json:"authenticatorEnabled"`
	ReferralCode         string         `json:"referralCode,omitempty"`
	TotalInvestment      float64        `json:"totalInvestMent"`
	LevelId              uint           `json:"levelId"`
}

type MasterInfo struct {
	ID             uint           `json:"id" gorm:"column:id"`
	Role           enums.UserRole `json:"role" gorm:"column:role"`
	Email          string         `json:"email" gorm:"column:email"`
	Phone          string         `json:"phone" gorm:"column:phone"`
	TotalFollowers uint           `json:"totalFollowers" gorm:"column:total_followers"`
	CreatedAt      time.Time      `json:"createdAt" gorm:"column:created_at"`
}

type FollowerInfo struct {
	ID    uint           `json:"id" gorm:"column:id"`
	Role  enums.UserRole `json:"role" gorm:"column:role"`
	Email string         `json:"email" gorm:"column:email"`
	Phone string         `json:"phone" gorm:"column:phone"`
}

type AuthUserRes struct {
	UserInfo
	Token
}

type GenerateOtpResponse struct {
	Base32     string `json:"base32"`
	OtpAuthUrl string `json:"otpAuthUrl"`
}

type Activate2faIn struct {
	EmailVerificationCode string `json:"emailVerificationCode" validate:"required"`
	AuthenticatorCode     string `json:"authenticatorCode" validate:"required"`
}

type Activate2faOut struct {
	OtpActivated bool      `json:"otpActivated"`
	User         *UserInfo `json:"user"`
}

type VerifyOtpInput struct {
	AuthenticatorCode string `json:"authenticatorCode"`
}

type VerifyOtpOutput struct {
	OtpVerified bool      `json:"otpVerified"`
	User        *UserInfo `json:"user"`
}

type DisableOtpOutput struct {
	OtpDisabled bool      `json:"otpDisabled"`
	User        *UserInfo `json:"user"`
}

type SetOtpBackupKeyInput struct {
	Code             string `json:"code" validate:"required"`
	ConfirmationCode string `json:"confirmationCode" validate:"required"`
}

type SetOtpBackupKeyOutput struct {
	OtpBackupKeyEnabled bool      `json:"otpBackupKeyEnabled"`
	User                *UserInfo `json:"user"`
}
