package entity

import (
	"gorm.io/gorm"
)

type TipType struct {
	ID uint `gorm:"primarykey" json:"id"`

	Name string `json:"name"`
}

func (*TipType) TableName() string {
	return "tiptypes"
}

func (tt *TipType) AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(tt)
}

func (l *TipType) BeforeCreate(_ *gorm.DB) (err error) {
	return
}
