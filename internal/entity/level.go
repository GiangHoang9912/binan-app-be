package entity

import (
	"gorm.io/gorm"
)

type Level struct {
	ID uint `gorm:"primarykey" json:"id"`

	Name        string `json:"name"`
	Invest      uint   `json:"invest"`
	TotalInvest uint   `json:"totalInvest"`
	TotalRef    uint   `json:"totalRef"`
}

func (*Level) TableName() string {
	return "levels"
}

func (l *Level) BeforeCreate(_ *gorm.DB) (err error) {
	return
}

func (l *Level) AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(l)
}
