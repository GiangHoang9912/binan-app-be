package entity

import (
	"binan/internal/enums"
	"gorm.io/gorm"
)

type Wallet struct {
	ID       uint               `json:"id" gorm:"primarykey"`
	UserID   uint               `json:"userId" gorm:"column:user_id"`
	Type     enums.WalletType   `json:"type" gorm:"column:type"`
	Balance  float64            `json:"balance" gorm:"column:balance"`
	Currency enums.CurrencyType `json:"currency" gorm:"currency"`

	TimeStamp
}

func (*Wallet) TableName() string {
	return "wallets"
}

func (u *Wallet) BeforeCreate(_ *gorm.DB) (err error) {
	return
}

type MakeDepositInput struct {
	WalletId uint               `json:"walletId" validate:"required"`
	Value    float64            `json:"value" validate:"required"`
	Type     enums.WalletType   `json:"type" validate:"required,oneof=FUTURE BINAN_TRADE REWARDING"`
	Currency enums.CurrencyType `json:"currency" validate:"required,oneof=USD VND"`
	Message  string             `json:"message"`
}

type ProcessDepositInput struct {
	DepositID uint                `json:"depositId" validate:"required"`
	Status    enums.DepositStatus `json:"status" validate:"required,oneof=REJECTED COMPLETED" `
}

type PurchasePackageInput struct {
	PackageID   uint   `json:"packageId" validate:"required"`
	TotalMonths uint   `json:"totalMonths" validate:"required,oneof=1 3 6 12"`
	AuthCode    string `json:"authCode"`
}

type WalletInfo struct {
	ID       uint               `json:"id"`
	UserID   uint               `json:"userId" gorm:"column:user_id"`
	Type     enums.WalletType   `json:"type" gorm:"column:type"`
	Balance  float64            `json:"balance" gorm:"column:balance"`
	Currency enums.CurrencyType `json:"currency" gorm:"currency"`

	TimeStamp
}
