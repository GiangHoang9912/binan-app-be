package entity

import (
	"gorm.io/gorm"
)

type CapitalManagements struct {
	ID uint `gorm:"primarykey" json:"id"`

	OrderValue    uint    `json:"order_value" gorm:"column:order_value"`
	Margin        uint    `json:"margin" gorm:"column:margin"`
	Entry         float64 `json:"entry" gorm:"column:entry"`
	Tp            float64 `json:"tp" gorm:"column:tp"`
	StopLoss      float64 `json:"stop_loss" gorm:"column:stop_loss"`
	LimitValue    float64 `json:"limit_value" gorm:"column:limit_value"`
	UserID        uint    `json:"user_id" gorm:"column:user_id"`
	UserPackageID uint    `json:"user_package_id" gorm:"column:user_package_id"`
	Disabled      bool    `json:"disabled" gorm:"column:disabled"`

	TimeStamp
}

func (*CapitalManagements) TableName() string {
	return "capital_managements"
}

func (u *CapitalManagements) BeforeCreate(_ *gorm.DB) (err error) {
	return
}

type CreateCapitalManagementsInput struct {
	OrderValue    uint    `json:"order_value" validate:"required"`
	Margin        uint    `json:"margin" validate:"required"`
	Entry         float64 `json:"entry" validate:"required"`
	Tp            float64 `json:"tp" validate:"required"`
	StopLoss      float64 `json:"stop_loss" validate:"required"`
	LimitValue    float64 `json:"limit_value" validate:"required"`
	UserPackageId uint    `json:"user_package_id" validate:"required"`
	Disabled      bool    `json:"disabled" validate:"required"`
}
