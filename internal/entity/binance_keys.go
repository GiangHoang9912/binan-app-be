package entity

import (
	"gorm.io/gorm"
)

type BinanceKeys struct {
	ID uint `gorm:"primarykey" json:"id"`

	ApiKey    string `json:"apiKey" gorm:"type:varchar(255)"`
	SecretKey string `json:"secretKey" gorm:"type:varchar(255)"`
	UserID    uint   `json:"userId" gorm:"column:user_id"`

	TimeStamp
}

func (*BinanceKeys) TableName() string {
	return "binance_keys"
}

func (u *BinanceKeys) BeforeCreate(_ *gorm.DB) (err error) {
	return
}

type SaveBinanceKeyInput struct {
	ApiKey    string `json:"apiKey" validate:"required"`
	SecretKey string `json:"secretKey" validate:"required"`
}

type BinanceKeysInfo struct {
	ID uint `json:"id"`

	ApiKey    string `json:"apiKey"`
	SecretKey string `json:"secretKey"`
	UserID    uint   `json:"userId"`
}

type CreateBinanceOrderInput struct {
	Symbol        string  `json:"symbol"`
	OrderValue    float64 `json:"orderValue" gorm:"column:order_value"`
	UserPackageID uint    `json:"orderPackageId" gorm:"column:user_package_id"`
	Margin        uint    `json:"margin" gorm:"column:margin"`
	Entry         uint    `json:"entry" gorm:"column:entry"`
	Tp            uint    `json:"tp" gorm:"column:tp"`
	ApiKey        string  `json:"apiKey"`
	SecretKey     string  `json:"secretKey"`
}
