package entity

type UserFollow struct {
	ID uint `gorm:"primarykey" json:"id"`

	UserID     uint    `json:"userId" gorm:"column:user_id"`
	FollowerID uint    `json:"followerId" gorm:"column:following_id"`
	Balance    float64 `json:"balance" gorm:"column:balance"`

	TimeStamp
}

func (*UserFollow) TableName() string {
	return "user_follows"
}

type FollowUserInput struct {
	FollowerID uint    `json:"followerId"`
	Balance    float64 `json:"balance"`
}
