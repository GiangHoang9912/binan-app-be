package entity

import (
	"binan/internal/enums"
	"gorm.io/datatypes"
)

type PackagePrice struct {
	Value       float64 `json:"value"`
	TotalMonths uint    `json:"totalMonths"`
}

type Package struct {
	ID uint `gorm:"primarykey" json:"id"`

	BotID    uint           `json:"botId" gorm:"column:bot_id"`
	Type     enums.CoinType `json:"type" gorm:"column:type"`
	PairWith enums.CoinType `json:"pairWith" gorm:"column:pair_with"`
	Prices   datatypes.JSON `json:"prices" gorm:"column:prices"`

	TimeStamp
}

func (*Package) TableName() string {
	return "packages"
}
