package entity

import (
	"binan/internal/enums"
	"gorm.io/gorm"
)

type Order struct {
	ID        uint `gorm:"primarykey" json:"id"`
	UserID    uint `json:"userId" gorm:"column:user_id"`
	PackageID uint `json:"packageId" gorm:"column:package_id"`

	Price         float64            `json:"price" gorm:"column:price"`
	PriceCurrency string             `json:"priceCurrency" gorm:"price_currency"`
	TotalMonths   uint               `json:"totalMonths" gorm:"column:total_months"`
	Type          enums.Subscription `json:"type" gorm:"column:type"`
	Status        enums.OrderStatus  `json:"status" gorm:"column:status"`

	TimeStamp
}

func (*Order) TableName() string {
	return "orders"
}

func (u *Order) BeforeCreate(_ *gorm.DB) (err error) {
	return
}

type CreateOrderInput struct {
	PackageID     uint               `json:"packageId" validate:"required"`
	Price         float64            `json:"price" validate:"required"`
	PriceCurrency string             `json:"priceCurrency" validate:"required"`
	TotalMonths   uint               `json:"totalMonths" validate:"required"`
	Type          enums.Subscription `json:"type" validate:"required"`
}
