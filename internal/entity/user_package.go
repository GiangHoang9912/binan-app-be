package entity

import "time"

type UserPackage struct {
	ID uint `gorm:"primarykey" json:"id"`

	UserID    uint      `json:"userId" gorm:"column:user_id"`
	PackageID uint      `json:"packageId" gorm:"column:package_id"`
	ExpiredAt time.Time `json:"expiredAt" gorm:"column:expired_at"`

	TimeStamp
}

func (*UserPackage) TableName() string {
	return "user_packages"
}

func (up *UserPackage) IsExpired() bool {
	now := time.Now()
	return up.ExpiredAt.Unix() < now.Unix()
}
