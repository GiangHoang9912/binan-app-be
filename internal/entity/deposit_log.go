package entity

import "binan/internal/enums"

type DepositLog struct {
	ID       uint                `json:"id" gorm:"primarykey"`
	WalletID uint                `json:"walletId" gorm:"column:wallet_id"`
	Value    float64             `json:"value" gorm:"column:value"`
	Currency enums.CurrencyType  `json:"currency" gorm:"column:currency"`
	Status   enums.DepositStatus `json:"status" gorm:"column:status"`
	Message  string              `json:"message" gorm:"column:message"`
	Code     string              `json:"code" gorm:"column:code"`

	TimeStamp
}

func (*DepositLog) TableName() string {
	return "deposit_logs"
}
