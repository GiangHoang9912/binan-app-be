package entity

import (
	"gorm.io/gorm"
)

type TipHistory struct {
	gorm.Model

	ReferralID uint     `json:"referralID"`
	Referral   Referral `json:"referral"`

	TipMechanismID uint         `json:"tipMechanismID"`
	TipMechanism   TipMechanism `json:"tipMechanism"`

	Amount float64 `json:"amount"`
	Tip    float64 `json:"tip"`
}

func (th *TipHistory) AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(th)
}

func (*TipHistory) TableName() string {
	return "tip_historys"
}

func (l *TipHistory) BeforeCreate(_ *gorm.DB) (err error) {
	return
}
