package entity

import (
	"database/sql"
	"time"
)

type Map map[string]any

type Pagination struct {
	Total uint `json:"total"`
}

type TimeStamp struct {
	CreatedAt time.Time    `json:"createdAt" gorm:"column:created_at"`
	UpdatedAt time.Time    `json:"updatedAt" gorm:"column:updated_at"`
	DeletedAt sql.NullTime `json:"deletedAt" gorm:"index;column:deleted_at" swaggertype:"string"`
}
