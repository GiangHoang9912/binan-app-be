package entity

import (
	"github.com/adshao/go-binance/v2"
	"github.com/adshao/go-binance/v2/futures"
)

type CopyTrade struct {
	ID uint `gorm:"primarykey" json:"id"`

	UserID       uint    `json:"userId" gorm:"column:user_id"`
	Value        float64 `json:"value" gorm:"column:value"`
	DailyValue   float64 `json:"dailyValue" gorm:"column:daily_value"`
	TotalDeposit string  `json:"totalDeposit" gorm:"column:total_deposit"`
	StopLossRate float64 `json:"stopLossRate" gorm:"column:stop_loss_rate"`

	TimeStamp
}

func (*CopyTrade) TableName() string {
	return "copy_trades_1"
}

type CopyTradeSettingInput struct {
	Value        float64 `json:"value"`
	DailyValue   float64 `json:"dailyValue"`
	TotalDeposit string  `json:"totalDeposit"`
	StopLossRate float64 `json:"stopLossRate"`
}

type CopyTradeSpotInput struct {
	Symbol   string           `json:"symbol"`
	Type     binance.SideType `json:"type" validate:"required,oneof=BUY SELL"`
	Quantity float64          `json:"quantity"`
	Price    float64          `json:"price"`
	Rate     float64          `json:"rate"`
}

type CopyTradeFutureInput struct {
	Symbol   string           `json:"symbol"`
	Type     futures.SideType `json:"type" validate:"required,oneof=BUY SELL"`
	Quantity float64          `json:"quantity"`
	Price    float64          `json:"price"`
}

type GetSpotInfoInput struct {
	OrderID int64  `json:"orderId"`
	Symbol  string `json:"symbol"`
}
