package entity

import (
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type TipMechanism struct {
	ID uint `gorm:"primarykey" json:"id"`

	LevelID uint `json:"levelID" query:"levelID" form:"level_id" gorm:"level_id"`

	TipTypeID uint `json:"tipTypeID" form:"tip_type_id"`

	Tip         float64 `json:"tip"`
	TotalTipRef uint    `json:"totalTipRef"`

	TipList pq.Float64Array `json:"tipList" gorm:"type:float[]"`
}

func (*TipMechanism) TableName() string {
	return "tip_mechanisms"
}

func (tm *TipMechanism) AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(tm)
}

func (u *TipMechanism) BeforeCreate(_ *gorm.DB) (err error) {
	return
}
