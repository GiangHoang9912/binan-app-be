package entity

import (
	"binan/internal/enums"
)

type Transaction struct {
	ID        uint                    `json:"id" gorm:"primarykey"`
	UserID    uint                    `json:"userId" gorm:"column:user_id"`
	PackageID uint                    `json:"packageId" gorm:"column:package_id"`
	Value     float64                 `json:"value" gorm:"column:value"`
	Currency  enums.CurrencyType      `json:"currency" gorm:"column:currency"`
	Status    enums.TransactionStatus `json:"status" gorm:"column:status"`

	TimeStamp
}

func (*Transaction) TableName() string {
	return "transactions"
}
