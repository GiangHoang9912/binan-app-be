package entity

import "github.com/adshao/go-binance/v2"

type OrderLog struct {
	ID            uint    `json:"id" gorm:"primarykey"`
	OrderID       string  `json:"orderId" gorm:"order_id"`
	UserPackageID uint    `json:"userPackageId" gorm:"user_package_id"`
	Message       string  `json:"message" gorm:"column:message"`
	Symbol        string  `json:"symbol" gorm:"column:symbol"`
	Base          string  `json:"base" gorm:"column:base"`
	OrderType     string  `json:"orderType" gorm:"column:order_type"`
	Quantity      float64 `json:"quantity" gorm:"column:quantity"`
	Price         float64 `json:"price" gorm:"column:price"`
	Rate          float64 `json:"rate" gorm:"column:rate"`
	UserID        uint    `json:"userId" gorm:"column:user_id"`

	TimeStamp
}

const OrderLogTable = "order_logs"

func (*OrderLog) TableName() string {
	return OrderLogTable
}

type CreateOrderLogInput struct {
	Message       string           `json:"message"`
	Type          binance.SideType `json:"type" validate:"required,oneof=BUY SELL"`
	Symbol        string           `json:"symbol"`
	Base          string           `json:"base"`
	OrderType     string           `json:"orderType"`
	OrderID       string           `json:"orderId"`
	UserPackageID uint             `json:"userPackageId"`
	Quantity      float64          `json:"quantity"`
	Price         float64          `json:"price"`
	Rate          float64          `json:"rate"`
}
