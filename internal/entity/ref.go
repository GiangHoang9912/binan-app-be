package entity

import (
	"gorm.io/gorm"
)

type Referral struct {
	gorm.Model

	PublisherID uint `json:"-" gorm:"column:publisher_id" form:"publisher_id"`
	Publisher   User `json:"publisher" gorm:"foreignKey:PublisherID"`

	UserID uint `json:"-" gorm:"column:user_id" form:"user_id"`
	User   User `json:"user" gorm:"foreignKey:UserID"`

	Floor uint `json:"floor" form:"floor"`
}

func (*Referral) TableName() string {
	return "referrals"
}

func (r *Referral) AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(r)
}

func (r *Referral) BeforeCreate(_ *gorm.DB) (err error) {
	return
}
