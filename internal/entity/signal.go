package entity

import (
	"gorm.io/gorm"
)

type Signal struct {
	ID uint `json:"id" gorm:"primarykey"`

	BotId      uint    `json:"botId" gorm:"column:bot_id"`
	Symbol     string  `json:"symbol" gorm:"type:varchar(255);column:symbol"`
	Base       string  `json:"base" gorm:"type:varchar(255);column:base"`
	Message    string  `json:"message" gorm:"type:varchar(255);column:message"`
	Entry1     float64 `json:"entry_1" gorm:"type:numeric;column:entry_1"`
	Entry2     float64 `json:"entry_2" gorm:"type:numeric;column:entry_2"`
	Entry3     float64 `json:"entry_3" gorm:"type:numeric;column:entry_3"`
	Tp1        float64 `json:"tp1" gorm:"type:numeric;column:tp1"`
	Tp2        float64 `json:"tp2" gorm:"type:numeric;column:tp2"`
	Tp3        float64 `json:"tp3" gorm:"type:numeric;column:tp3"`
	Tp4        float64 `json:"tp4" gorm:"type:numeric;column:tp4"`
	Tp5        float64 `json:"tp5" gorm:"type:numeric;column:tp5"`
	Tp6        float64 `json:"tp6" gorm:"type:numeric;column:tp6"`
	Tp7        float64 `json:"tp7" gorm:"type:numeric;column:tp7"`
	Tp8        float64 `json:"tp8" gorm:"type:numeric;column:tp8"`
	Tp9        float64 `json:"tp9" gorm:"type:numeric;column:tp9"`
	Tp10       float64 `json:"tp10" gorm:"type:numeric;column:tp10"`
	Tp11       float64 `json:"tp11" gorm:"type:numeric;column:tp11"`
	EntryPrice float64 `json:"entryPrice" gorm:"type:numeric;column:entry_price"`
	SellPrice  float64 `json:"sellPrice" gorm:"type:numeric;column:sell_price"`
	Profit     float64 `json:"profit" gorm:"type:numeric;column:profit"`
	StopLoss   float64 `json:"stopLoss" gorm:"type:numeric;column:stop_loss"`
	Margin     string  `json:"margin" gorm:"type:varchar(255);column:margin"`
	Safety     string  `json:"safety" gorm:"type:varchar(255);column:safety"`
	IsDelete   bool    `json:"isDelete" gorm:"column:is_delete"`

	TimeStamp
}

func (*Signal) TableName() string {
	return "signal_histories"
}

func (s *Signal) BeforeCreate(_ *gorm.DB) (err error) {
	return
}
