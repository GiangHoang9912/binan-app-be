package entity

import (
	"gorm.io/gorm"
)

type Bot struct {
	ID uint `gorm:"primarykey" json:"id"`

	Name             string  `json:"name" gorm:"type:varchar(255)"`
	Symbol           string  `json:"symbol" gorm:"type:varchar(255)"`
	Base             string  `json:"base" gorm:"type:varchar(255)"`
	WinRate          float32 `json:"winRate" gorm:"column:win_rate"`
	ProfitRate7d     float32 `json:"profitRate7d" gorm:"column:profit_rate_7d"`
	ProfitRate30d    float32 `json:"profitRate30d" gorm:"column:profit_rate_30d"`
	NumberOfRegister int     `json:"numberOfRegister" gorm:"column:number_of_register"`
	DailyTradeNumber int     `json:"dailyTradeNumber" gorm:"column:daily_trade_number"`
	IsActive         bool    `json:"isActive" gorm:"column:is_active"`
	IsDelete         bool    `json:"isDelete" gorm:"column:is_delete"`

	TimeStamp
}

func (*Bot) TableName() string {
	return "bots"
}

func (u *Bot) BeforeCreate(_ *gorm.DB) (err error) {
	return
}
