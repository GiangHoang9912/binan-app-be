// Package app configures and runs application.
package app

import (
	"binan/internal/controller/http/middlewares"
	"binan/internal/usecase/bot_telegram"
	binanceClient "binan/internal/usecase/webapi"
	"binan/pkg/redis"
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-playground/validator/v10"

	"github.com/gin-gonic/gin"

	"binan/config"
	v1 "binan/internal/controller/http/v1"
	"binan/internal/usecase"
	"binan/internal/usecase/repo"
	"binan/pkg/httpserver"
	"binan/pkg/logger"
	"binan/pkg/postgres"
)

// Run creates objects via constructors.
func Run(cfg *config.Config) {
	logger_ := logger.New(cfg.Log.Level)
	validator_ := validator.New()

	// Repository
	pg, err := postgres.New(postgres.MaxPoolSize(cfg.PG.PoolMax))
	if err != nil {
		logger_.Fatal(fmt.Errorf("app - Run - postgres.New: %w", err))
	}
	defer pg.Close()
	rdb := redis.New()

	// Mail use-case
	mailUseCase := usecase.NewMailUseCase(repo.NewUserRepo(pg), rdb)

	// User use-case
	userUseCase := usecase.NewUserUseCase(
		repo.NewUserRepo(pg),
		repo.NewReferralRepo(pg),
		repo.NewBinanceKeysRepo(pg),
		rdb,
		pg,
		cfg,
	)

	// Bot use-case
	botUseCase := usecase.NewBotUseCase(
		repo.NewUserRepo(pg),
		pg,
	)

	// Signal use-case
	signalUseCase := usecase.NewSignalUseCase(
		repo.NewUserRepo(pg),
		pg,
	)

	// Order use-case
	orderUseCase := usecase.NewOrderUseCase(
		pg,
	)
	// Wallet use-case
	walletUseCase := usecase.NewWalletUseCase(
		repo.NewUserRepo(pg),
		repo.NewWalletRepo(pg),
		repo.NewBinanceKeysRepo(pg),
		pg,
		logger_,
	)

	// Package use-case
	packageUseCase := usecase.NewPackageUseCase(repo.NewUserRepo(pg), pg, logger_)

	// Account-payment use-case
	accountPaymentUseCase := usecase.NewAccountPaymentUseCase()

	// User-Package use-case
	userPackageUseCase := usecase.NewUserPackagesUseCase(
		repo.NewUserRepo(pg),
		pg,
	)

	// capital-managements use-case
	capitalManagementsUserCase := usecase.NewCapitalManagementsUseCase(
		repo.NewUserRepo(pg),
		pg,
	)

	// Binance-Keys use-case
	binanceKeysUserCase := usecase.NewBinanceKeysUseCase(
		repo.NewUserRepo(pg),
		pg,
	)

	orderLogUseCase := usecase.NewOrderLogUseCase(
		repo.NewOrderLogRepo(pg),
		repo.NewBinanceKeysRepo(pg),
		pg,
	)

	copyTradeUseCase := usecase.NewCopyTradeUseCase(
		repo.NewUserRepo(pg),
		repo.NewBinanceKeysRepo(pg),
		repo.NewOrderLogRepo(pg),
		pg,
	)

	// Account-payment use-case
	levelUseCase := usecase.NewLevelUseCase(repo.NewLevelRepo(pg), pg, logger_)

	tipUseCase := usecase.NewTipUseCase(repo.NewUserRepo(pg), repo.NewWalletRepo(pg), repo.NewLevelRepo(pg), repo.NewTipHistoryRepo(pg), repo.NewTipTypeRepo(pg), repo.NewTipMechanismRepo(pg), repo.NewReferralRepo(pg), pg, logger_)

	tipMechanismUseCase := usecase.NewMechanismUseCase(repo.NewTipMechanismRepo(pg), pg, logger_)

	tipTypeUseCase := usecase.NewTipTypeUseCase(repo.NewTipTypeRepo(pg), pg, logger_)

	tipHistoryUseCase := usecase.NewTipHistoryUseCase(repo.NewTipHistoryRepo(pg), repo.NewReferralRepo(pg), pg, logger_)

	referralUseCase := usecase.NewRefUseCase(repo.NewReferralRepo(pg), pg, logger_)

	// HTTP Server
	handler := gin.New()
	handler.Use(middlewares.CORSMiddleware)
	v1.NewRouter(
		handler,
		logger_,
		validator_,
		mailUseCase,
		userUseCase,
		botUseCase,
		signalUseCase,
		orderUseCase,
		walletUseCase,
		packageUseCase,
		accountPaymentUseCase,
		userPackageUseCase,
		capitalManagementsUserCase,
		binanceKeysUserCase,
		orderLogUseCase,
		copyTradeUseCase,
		levelUseCase,
		tipUseCase,
		tipMechanismUseCase,
		tipTypeUseCase,
		tipHistoryUseCase,
		referralUseCase,
	)
	httpServer := httpserver.New(handler, httpserver.Port(cfg.HTTP.Port))

	bc := binanceClient.NewBinanceClient(repo.NewBinanceKeysRepo(pg), repo.NewOrderLogRepo(pg), logger_)
	botTele := bot_telegram.NewBotTelegram(rdb, bc, logger_)
	go botTele.Listen(context.Background())

	//consumer := make(chan binance.WsKline)
	//go binanceClient.WebSocket(consumer, make(chan struct{}))

	// Waiting for interrupt signal
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-interrupt:
		logger_.Info("app - Run - signal: " + s.String())
	case err = <-httpServer.Notify():
		logger_.Error(fmt.Errorf("app - Run - httpServer.Notify: %w", err))
	}

	// Shutdown
	err = httpServer.Shutdown()
	if err != nil {
		logger_.Error(fmt.Errorf("app - Run - httpServer.Shutdown: %w", err))
	}
}
