package bot_telegram

import (
	"binan/internal/entity"
	binance "binan/internal/usecase/webapi"
	"binan/pkg/logger"
	"binan/pkg/redis"
	"context"
	"encoding/json"
	"fmt"

	redis2 "github.com/go-redis/redis/v8"
)

// BotTelegram -.
type BotTelegram struct {
	*redis.Redis
	binanceClient *binance.Binance
	logger        *logger.Logger
}

// NewBotTelegram -.
func NewBotTelegram(rdb *redis.Redis, bc *binance.Binance, l *logger.Logger) *BotTelegram {
	return &BotTelegram{
		Redis:         rdb,
		binanceClient: bc,
		logger:        l,
	}
}

func (bt *BotTelegram) Listen(ctx context.Context) {
	bt.logger.Info("bot - start listening")
	pubSub := bt.Rdb.Subscribe(ctx, "signal")

	// Close the subscription when we are done.
	defer func(ps *redis2.PubSub) {
		err := ps.Close()
		if err != nil {
			bt.logger.Error("error closing pub-sub ", err)
		}
		bt.logger.Info("close channel")
	}(pubSub)

	ch := pubSub.Channel()
	for msg := range ch {
		signalHistory := entity.Signal{}
		err := json.Unmarshal([]byte(msg.Payload), &signalHistory)
		fmt.Println("payload", msg.Payload)
		if err != nil {
			bt.logger.Error(
				"Unmarshal payload error: ",
				msg.Channel,
				msg.Payload,
				err.Error(),
			)
		} else {
			orders, err := bt.binanceClient.CreateBinanceOrders(signalHistory)
			if err != nil {
				bt.logger.Error("CreateBinanceOrders Error: ", err.Error())
			} else {
				bt.logger.Info("CreateBinanceOrders: ", orders)
			}
		}
	}
}
