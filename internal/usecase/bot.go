package usecase

import (
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// BotUseCase -.
type BotUseCase struct {
	botRepo IBotRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewBotUseCase -.
func NewBotUseCase(b IBotRepo, p *postgres.Postgres) *BotUseCase {
	return &BotUseCase{
		botRepo:  b,
		Postgres: p,
	}
}

func (bu *BotUseCase) GetAllBot(ctx context.Context) (error, []entity.Bot) {
	bots := make([]entity.Bot, 0)
	bu.Postgres.Db.WithContext(ctx).Where("is_delete = ?", false).Where("is_active = ?", true).Find(&bots)
	return nil, bots
}

func (bu *BotUseCase) GetBot(ctx context.Context, botId uint) (error, *entity.Bot) {
	var bot entity.Bot
	err := bu.Postgres.GetById(ctx, botId, &bot)
	if err != nil {
		return err, nil
	}
	return nil, &bot
}
