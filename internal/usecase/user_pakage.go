package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"fmt"
	"net/http"
)

// UserPackagesUseCase -.
type UserPackagesUseCase struct {
	userRepo IUserRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewUserPackagesUseCase -.
func NewUserPackagesUseCase(ur IUserRepo, p *postgres.Postgres) *UserPackagesUseCase {
	return &UserPackagesUseCase{
		userRepo: ur,
		Postgres: p,
	}
}

func (uc *UserPackagesUseCase) GetUserPackageUser(ctx context.Context, userId uint) ([]entity.UserPackage, Status, error) {
	user, _ := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, fmt.Errorf(constants.UserNotFoundMessage)
	}

	userPackage := make([]entity.UserPackage, 0)
	uc.Postgres.Db.WithContext(ctx).Where("user_id = ?", userId).Find(&userPackage)

	return userPackage, http.StatusOK, fmt.Errorf(constants.UserNotFoundMessage)
}
