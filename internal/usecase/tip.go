package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"fmt"
	"log"

	"errors"
	"net/http"
)

// WalletUseCase -.
type TipUseCase struct {
	userRepo     IUserRepo
	walletRepo   IWalletRepo
	levelRepo    ILevelRepo
	tipRepo      ITipHistoryRepo
	tipTypeRepo  ITipTypeRepo
	tipMechanism ITipMechanismRepo
	refRepo      IReferralRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewTipUseCase -.
func NewTipUseCase(ur IUserRepo, wr IWalletRepo, lr ILevelRepo, tr ITipHistoryRepo, ttr ITipTypeRepo, tmr ITipMechanismRepo, rr IReferralRepo, p *postgres.Postgres, l *logger.Logger) *TipUseCase {
	return &TipUseCase{
		userRepo:     ur,
		levelRepo:    lr,
		walletRepo:   wr,
		tipRepo:      tr,
		tipTypeRepo:  ttr,
		refRepo:      rr,
		tipMechanism: tmr,
		Postgres:     p,
		logger:       l,
	}
}

func (tu *TipUseCase) Commission(ctx context.Context, userId uint, amount float64, typeTipId uint) (Status, error) {
	user, _ := tu.userRepo.GetByID(ctx, userId)

	refs, _ := tu.refRepo.GetRef(ctx, entity.Referral{UserID: user.ID})

	mechanism, _ := tu.tipMechanism.GetByLevelAndType(ctx, user.LevelId, typeTipId)

	wallet, _ := tu.walletRepo.GetByType(ctx, userId, enums.BinanTrade)

	log.Println(wallet.Balance)
	log.Println(mechanism)
	tip := amount * mechanism.Tip / 100
	if wallet.Balance < tip {
		tip = wallet.Balance
	}

	wallet.Balance -= amount
	err := tu.Postgres.Update(ctx, wallet)
	if err != nil {
		return http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	if tip < 0 {
		return http.StatusBadRequest, errors.New(constants.NotEnoughBalanceToCommission)
	}

	for _, ref := range refs {
		fmt.Println("handle commission", ref)
		tu.SplitCommission(ctx, ref, tip, typeTipId)
	}

	return http.StatusOK, nil
}

func (tu *TipUseCase) SplitCommission(ctx context.Context, ref *entity.Referral, amount float64, typeTipId uint) (bool, error) {
	user, _ := tu.userRepo.GetByID(ctx, ref.PublisherID)
	floor := ref.Floor
	log.Println("split ", amount, ref.Floor)
	mechanism, _ := tu.tipMechanism.GetByLevelAndType(ctx, user.LevelId, typeTipId)
	log.Println("mechanism.TipList ", mechanism.TipList)
	if len(mechanism.TipList) >= int(floor) {
		tip := amount * mechanism.TipList[floor-1] / 100

		log.Println("tip", tip)
		success, err := tu.AddCommission(ctx, ref.PublisherID, tip)
		if !success {
			return false, err
		}

		if err != nil {
			return false, err
		}

		tipHistory := &entity.TipHistory{
			ReferralID:     ref.ID,
			TipMechanismID: mechanism.ID,
			Amount:         amount,
			Tip:            tip,
		}

		err = tu.tipRepo.Create(ctx, tipHistory)
		if err != nil {
			return false, err
		}

		return true, err
	}
	return false, nil
}

func (tu *TipUseCase) AddCommission(ctx context.Context, userId uint, amount float64) (bool, error) {
	log.Println("add ", userId, amount)
	wallet, _ := tu.walletRepo.GetByType(ctx, userId, enums.BinanTrade)

	wallet.Balance += amount

	err := tu.Postgres.Update(ctx, wallet)
	if err != nil {
		return false, errors.New(constants.InternalServerErrorMessage)
	}
	return true, nil
}
