package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"binan/utils"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"net/http"
	"time"
)

// WalletUseCase -.
type WalletUseCase struct {
	userRepo        IUserRepo
	walletRepo      IWalletRepo
	binanceKeysRepo IBinanceKeysRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewWalletUseCase -.
func NewWalletUseCase(ur IUserRepo, wr IWalletRepo, bkr IBinanceKeysRepo, p *postgres.Postgres, l *logger.Logger) *WalletUseCase {
	return &WalletUseCase{
		userRepo:        ur,
		walletRepo:      wr,
		binanceKeysRepo: bkr,
		Postgres:        p,
		logger:          l,
	}
}

func (wu *WalletUseCase) GetUserWallets(ctx context.Context, userId uint) ([]entity.Wallet, Status, error) {
	user, _ := wu.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, fmt.Errorf(constants.UserNotFoundMessage)
	}

	wallet, _ := wu.walletRepo.GetMany(ctx, userId)
	if wallet == nil {
		return nil, http.StatusNotFound, fmt.Errorf(constants.WalletNotFoundMessage)
	}
	return wallet, http.StatusOK, nil
}

func (wu *WalletUseCase) MakeDeposit(ctx context.Context, depositInput entity.MakeDepositInput) (*entity.DepositLog, Status, error) {
	wallet, err := wu.walletRepo.GetByID(ctx, depositInput.WalletId)
	if wallet == nil {
		return nil, http.StatusNotFound, fmt.Errorf(constants.WalletNotFoundMessage)
	}

	depositLog := &entity.DepositLog{
		WalletID: wallet.ID,
		Value:    depositInput.Value,
		Currency: depositInput.Currency,
		Status:   enums.DepositStatus(enums.Pending),
		Message:  depositInput.Message,
		Code:     utils.RandomString(12),
	}
	err = wu.Postgres.Create(ctx, depositLog)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	return depositLog, http.StatusOK, nil
}

func (wu *WalletUseCase) ProcessDeposit(ctx context.Context, depositInput entity.ProcessDepositInput) (*entity.DepositLog, Status, error) {
	var depositLog entity.DepositLog
	err := wu.Postgres.GetById(ctx, depositInput.DepositID, &depositLog)

	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf(constants.InternalServerErrorMessage)
	}

	wallet, err := wu.walletRepo.GetByID(ctx, depositLog.WalletID)
	if wallet == nil {
		return nil, http.StatusNotFound, err
	}

	// TODO: convert currency unit first
	//wallet.Balance += depositLog.Value
	//err = wu.Postgres.Update(ctx, wallet)
	//if err != nil {
	//	return nil, http.StatusInternalServerError, err
	//}

	depositLog.Status = depositInput.Status
	err = wu.Postgres.Update(ctx, &depositLog)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return &depositLog, http.StatusOK, nil
}

func (wu *WalletUseCase) PurchasePackage(ctx context.Context, userId uint, purchasePackageInput entity.PurchasePackageInput) (*entity.UserPackage, Status, error) {
	packageId := purchasePackageInput.PackageID
	user, _ := wu.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, fmt.Errorf(constants.UserNotFoundMessage)
	}

	if !user.Check2fa(purchasePackageInput.AuthCode) {
		return nil, http.StatusUnauthorized, fmt.Errorf(constants.Invalid2faCode)
	}

	var pkg entity.Package
	err := wu.Postgres.GetById(ctx, packageId, &pkg)
	if err != nil {
		return nil, http.StatusNotFound, err
	}

	wallet, err := wu.walletRepo.GetByType(ctx, userId, enums.BinanTrade)
	if err != nil {
		return nil, http.StatusNotFound, err
	}

	s := pkg.Prices.String()
	wu.logger.Info(s)
	var pkgPrices []entity.PackagePrice
	_ = json.Unmarshal([]byte(s), &pkgPrices)

	// get the chosen package
	var chosenPkg entity.PackagePrice
	for _, v := range pkgPrices {
		if v.TotalMonths == purchasePackageInput.TotalMonths {
			chosenPkg = v
		}
	}
	// check if balance is enough
	if wallet.Balance < chosenPkg.Value {
		return nil, http.StatusForbidden, fmt.Errorf("not enough balance")
	}

	var userPkg entity.UserPackage
	err = wu.Postgres.Db.WithContext(ctx).Where("user_id", userId).Where("package_id", packageId).First(&userPkg).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		userPkg = entity.UserPackage{
			UserID:    userId,
			PackageID: packageId,
			ExpiredAt: time.Now(),
		}
		wu.Postgres.Db.WithContext(ctx).Create(&userPkg)
	}
	// update package expired time
	if userPkg.IsExpired() {
		userPkg.ExpiredAt = time.Now()
	}
	userPkg.ExpiredAt = userPkg.ExpiredAt.AddDate(0, int(chosenPkg.TotalMonths), 0)
	// update user wallet balance
	wallet.Balance -= chosenPkg.Value
	_ = wu.Postgres.Update(ctx, &userPkg)
	_ = wu.Postgres.Update(ctx, &wallet)
	// create a transaction
	transaction := entity.Transaction{
		UserID:    userId,
		PackageID: packageId,
		Value:     chosenPkg.Value,
		Currency:  enums.Usd,
		Status:    enums.Success,
	}
	_ = wu.Postgres.Create(ctx, &transaction)

	return &userPkg, http.StatusOK, nil
}

func (wu *WalletUseCase) GetDepositLogs(ctx context.Context) ([]entity.DepositLog, Status, error) {
	depositLogs := make([]entity.DepositLog, 0)
	wu.Postgres.Db.WithContext(ctx).Find(&depositLogs)
	return depositLogs, http.StatusOK, nil
}

func (wu *WalletUseCase) GetTransactions(ctx context.Context, userId uint) ([]entity.Transaction, Status, error) {
	transactions := make([]entity.Transaction, 0)
	wu.Postgres.Db.WithContext(ctx).Where("user_id = ?", userId).Find(&transactions)
	return transactions, http.StatusOK, nil
}

func (wu *WalletUseCase) GetAvailableBinanceKeys(ctx context.Context) ([]entity.CreateBinanceOrderInput, error) {
	res, err := wu.binanceKeysRepo.GetAvailableKeys(ctx)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (wu *WalletUseCase) GetUserWalletWithType(ctx context.Context, userId uint, balanceType string) (*entity.WalletInfo, Status, error) {
	walletInfo := new(entity.WalletInfo)
	wu.Postgres.Db.WithContext(ctx).
		Table("users").
		Joins("INNER JOIN wallets as ws ON users.id = ws.user_id").
		Select("ws.id, ws.type, ws.balance").
		Where("ws.type", balanceType).
		Where("users.id = ?", userId).
		First(&walletInfo)

	return walletInfo, http.StatusOK, nil
}

func (wu *WalletUseCase) SetNewBalance(ctx context.Context, userId uint, balance float64, balanceTpe string) (entity.Wallet, Status, error) {
	var walletFuture entity.Wallet
	err := wu.Db.Model(&entity.Wallet{}).
		WithContext(ctx).
		Where("user_id = ?", userId).
		Where("type", balanceTpe).
		First(&walletFuture).
		Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return walletFuture, http.StatusNotFound, err
	}

	walletFuture.Balance = balance

	err = wu.Db.Save(&walletFuture).Error

	if err != nil {
		return walletFuture, http.StatusInternalServerError, err
	}
	return walletFuture, http.StatusNoContent, nil
}

func (wu *WalletUseCase) GetDepositByID(ctx context.Context, depositId uint) (*entity.DepositLog, Status, error) {
	depositLogs := new(entity.DepositLog)
	wu.Postgres.Db.WithContext(ctx).Where("id = ?", depositId).First(&depositLogs)
	return depositLogs, http.StatusOK, nil
}
