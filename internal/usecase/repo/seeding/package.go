package seeding

import (
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/pkg/postgres"
	"context"
	"encoding/json"
	"gorm.io/datatypes"
	"time"
)

var priceBytes, _ = json.Marshal([]entity.PackagePrice{
	{
		Value:       1.99,
		TotalMonths: 1,
	},
	{
		Value:       5.99,
		TotalMonths: 3,
	},
	{
		Value:       14.99,
		TotalMonths: 6,
	},
	{
		Value:       19.99,
		TotalMonths: 12,
	},
})

var packages = []*entity.Package{
	{
		ID:       1,
		BotID:    1,
		Type:     enums.BTC,
		PairWith: enums.USDT,
		Prices:   datatypes.JSON(priceBytes),
		TimeStamp: entity.TimeStamp{
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
	},
	{
		ID:       2,
		BotID:    1,
		Type:     enums.ETH,
		PairWith: enums.USDT,
		Prices:   datatypes.JSON(priceBytes),
		TimeStamp: entity.TimeStamp{
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
	},
	{
		ID:       3,
		BotID:    1,
		Type:     enums.BNB,
		PairWith: enums.USDT,
		Prices:   datatypes.JSON(priceBytes),
		TimeStamp: entity.TimeStamp{
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
	},
}

func SeedPackages(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, pkg := range packages {
		var exists bool
		_ = pg.Db.Model(pkg).
			Select("count(*) > 0").
			Where("id = ?", pkg.ID).
			Find(&exists).
			Error
		if !exists {
			pg.Db.WithContext(ctx).Create(pkg)
		}
	}
}
