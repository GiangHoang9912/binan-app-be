package seeding

import (
	"binan/internal/entity"
	"binan/pkg/postgres"
	"context"
)

var TipMechanisms = []entity.TipMechanism{
	{
		ID:          1,
		LevelID:     1,
		Tip:         30,
		TipTypeID:   1,
		TotalTipRef: 1,
		TipList:     []float64{20},
	},
	{
		ID:          2,
		LevelID:     1,
		Tip:         10,
		TipTypeID:   2,
		TotalTipRef: 1,
		TipList:     []float64{50},
	},
	{
		ID:          3,
		LevelID:     1,
		Tip:         10,
		TipTypeID:   3,
		TotalTipRef: 1,
		TipList:     []float64{50},
	},
	{
		ID:          4,
		LevelID:     2,
		Tip:         30,
		TipTypeID:   1,
		TotalTipRef: 2,
		TipList:     []float64{20, 10},
	},
	{
		ID:          5,
		LevelID:     2,
		Tip:         10,
		TipTypeID:   2,
		TotalTipRef: 2,
		TipList:     []float64{50, 20},
	},
	{
		ID:      6,
		LevelID: 2,
		Tip:     10,

		TipTypeID:   3,
		TotalTipRef: 2,
		TipList:     []float64{50, 20},
	},
	{
		ID:          7,
		Tip:         30,
		LevelID:     3,
		TipTypeID:   1,
		TotalTipRef: 3,
		TipList:     []float64{20, 10, 5},
	},
	{
		ID:          8,
		LevelID:     3,
		TipTypeID:   2,
		Tip:         10,
		TotalTipRef: 3,
		TipList:     []float64{50, 20, 10},
	},
	{
		ID:          9,
		Tip:         10,
		LevelID:     3,
		TipTypeID:   3,
		TotalTipRef: 3,
		TipList:     []float64{50, 20, 10},
	},
}

func SeedTipMechanism(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, mechanism := range TipMechanisms {
		if pg.Db.WithContext(ctx).Model(mechanism).Where("id = ?", mechanism.ID).Updates(mechanism).RowsAffected == 0 {
			pg.Db.WithContext(ctx).Create(&mechanism)
		}
	}
}
