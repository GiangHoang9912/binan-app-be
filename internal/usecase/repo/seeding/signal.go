package seeding

import (
	"binan/internal/entity"
	"binan/pkg/postgres"
	"context"
)

var signals = []*entity.Signal{
	{
		ID:         1,
		BotId:      1,
		Symbol:     "BTCUSDT",
		Base:       "LONG",
		Message:    "🤖 BOT - BTCUSDT\nSymbol: BTCUSDT\n\n    —————🌟—————\n「1. 🌟 Trend: LONG-\n↗️ Đỉnh: 20718.4 \n\n↘️ Đáy: 20350\n-\n👉 Entry 1: 20411.05\n👉 Entry 2: 20431.40\n👉 Entry 3: 20451.75\n\n   —————🌟—————\n「2. 🌟 Stop-loss:\n-\n👉 Stop-loss: 20288.95\n\n   —————🌟—————\n「3. Take-profit:\n-\n👉 TP1: 20553.50\n👉 TP2: 20643.04\n👉 TP3: 20728.51\n👉 TP4: 20838.40\n👉 TP5: 20960.50\n👉 TP6: 21123.30\n👉 TP7: 21265.75\n👉 TP8: 21448.90\n👉 TP9: 21652.40\n👉 TP10: 21794.85\n👉 TP11: 21978.00\n",
		Entry1:     20411.05,
		Entry2:     20431.4,
		Entry3:     20451.75,
		Tp1:        20553.5,
		Tp2:        20643.04,
		Tp3:        20728.51,
		Tp4:        20838.4,
		Tp5:        20960.5,
		Tp6:        21123.3,
		Tp7:        21265.75,
		Tp8:        21448.9,
		Tp9:        21652.4,
		Tp10:       21794.85,
		Tp11:       21978,
		EntryPrice: 20411.05,
		StopLoss:   20288.95,
		Safety:     "DAY",
		IsDelete:   false,
	},
	{
		ID:         2,
		BotId:      1,
		Symbol:     "BTCUSDT",
		Base:       "SHORT",
		Message:    "🤖 BOT - BTCUSDT\nSymbol: BTCUSDT\n\n    —————🌟—————\n「1. 🌟 Trend: SHORT-\n↗️ Đỉnh: 21297 \n\n↘️ Đáy: 20350\n-\n👉 Entry 1: 21233.11\n👉 Entry 2: 21211.81\n👉 Entry 3: 21190.51\n\n   —————🌟—————\n「2. 🌟 Stop-loss:\n-\n👉 Stop-loss: 21360.89\n\n   —————🌟—————\n「3. Take-profit:\n-\n👉 TP1: 21084.03\n👉 TP2: 20990.32\n👉 TP3: 20900.88\n👉 TP4: 20785.87\n👉 TP5: 20658.09\n👉 TP6: 20487.71\n👉 TP7: 20338.63\n👉 TP8: 20146.96\n👉 TP9: 19933.99\n👉 TP10: 19784.91\n👉 TP11: 19593.24\n",
		Entry1:     21233.11,
		Entry2:     21211.81,
		Entry3:     21190.51,
		Tp1:        21084.03,
		Tp2:        20990.32,
		Tp3:        20900.88,
		Tp4:        20785.87,
		Tp5:        20658.09,
		Tp6:        20487.71,
		Tp7:        20338.63,
		Tp8:        20146.96,
		Tp9:        19933.99,
		Tp10:       19784.91,
		Tp11:       19593.24,
		EntryPrice: 21233.11,
		StopLoss:   21360.89,
		Safety:     "DAY",
		IsDelete:   false,
	},
	{
		ID:         3,
		BotId:      1,
		Symbol:     "BTCUSDT",
		Base:       "LONG",
		Message:    "🤖 BOT - BTCUSDT\nSymbol: BTCUSDT\n\n    —————🌟—————\n「1. 🌟 Trend: LONG-\n↗️ Đỉnh: 21297 \n\n↘️ Đáy: 20660\n-\n👉 Entry 1: 20721.98\n👉 Entry 2: 20742.64\n👉 Entry 3: 20763.30\n\n   —————🌟—————\n「2. 🌟 Stop-loss:\n-\n👉 Stop-loss: 20598.02\n\n   —————🌟—————\n「3. Take-profit:\n-\n👉 TP1: 20866.60\n👉 TP2: 20957.50\n👉 TP3: 21044.28\n👉 TP4: 21155.84\n👉 TP5: 21279.80\n👉 TP6: 21445.08\n👉 TP7: 21589.70\n👉 TP8: 21775.64\n👉 TP9: 21982.24\n👉 TP10: 22126.86\n👉 TP11: 22312.80\n",
		Entry1:     20721.98,
		Entry2:     20742.64,
		Entry3:     20763.3,
		Tp1:        20866.6,
		Tp2:        20957.5,
		Tp3:        21044.28,
		Tp4:        21155.84,
		Tp5:        21279.8,
		Tp6:        21445.08,
		Tp7:        21589.7,
		Tp8:        21775.64,
		Tp9:        21982.24,
		Tp10:       22126.86,
		Tp11:       22312.8,
		EntryPrice: 20721.98,
		StopLoss:   20598.02,
		Safety:     "DAY",
		IsDelete:   false,
	},
	{
		ID:         4,
		BotId:      1,
		Symbol:     "BTCUSDT",
		Base:       "SHORT",
		Message:    "🤖 BOT - BTCUSDT\nSymbol: BTCUSDT\n\n    —————🌟—————\n「1. 🌟 Trend: SHORT-\n↗️ Đỉnh: 21450 \n\n↘️ Đáy: 20660\n-\n👉 Entry 1: 21385.65\n👉 Entry 2: 21364.20\n👉 Entry 3: 21342.75\n\n   —————🌟—————\n「2. 🌟 Stop-loss:\n-\n👉 Stop-loss: 21514.35\n\n   —————🌟—————\n「3. Take-profit:\n-\n👉 TP1: 21235.50\n👉 TP2: 21141.12\n👉 TP3: 21051.03\n👉 TP4: 20935.20\n👉 TP5: 20806.50\n👉 TP6: 20634.90\n👉 TP7: 20484.75\n👉 TP8: 20291.70\n👉 TP9: 20077.20\n👉 TP10: 19927.05\n👉 TP11: 19734.00\n",
		Entry1:     21385.65,
		Entry2:     21364.2,
		Entry3:     21342.75,
		Tp1:        21235.5,
		Tp2:        21141.12,
		Tp3:        21051.03,
		Tp4:        20935.2,
		Tp5:        20806.5,
		Tp6:        20634.9,
		Tp7:        20484.75,
		Tp8:        20291.7,
		Tp9:        20077.2,
		Tp10:       19927.05,
		Tp11:       19734,
		EntryPrice: 21385.65,
		StopLoss:   21514.35,
		Safety:     "DAY",
		IsDelete:   false,
	},
}

func SeedSignals(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, signal := range signals {
		var exists bool
		_ = pg.Db.Model(signal).
			Select("count(*) > 0").
			Where("id = ?", signal.ID).
			Find(&exists).
			Error
		if !exists {
			pg.Db.WithContext(ctx).Create(signal)
		}
	}
}
