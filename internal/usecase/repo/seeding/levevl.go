package seeding

import (
	"binan/internal/entity"
	"binan/pkg/postgres"
	"context"
)

var Levels = []entity.Level{
	{
		ID:          1,
		Name:        "FC",
		Invest:      1000,
		TotalInvest: 0,
		TotalRef:    0,
	},
	{
		ID:          2,
		Name:        "MG",
		Invest:      5000,
		TotalInvest: 50000,
		TotalRef:    5,
	},
	{
		ID:          3,
		Name:        "DR",
		Invest:      10000,
		TotalInvest: 500000,
		TotalRef:    10,
	},
}

func SeedLevels(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, level := range Levels {
		if pg.Db.WithContext(ctx).Model(level).Where("id = ?", level.ID).Updates(level).RowsAffected == 0 {
			pg.Db.WithContext(ctx).Create(&level)
		}
	}
}
