package seeding

import (
	"binan/internal/entity"
	"binan/pkg/postgres"
	"context"
	"time"
)

var bots = []*entity.Bot{
	{
		ID:               1,
		Name:             "BTCUSDT",
		Symbol:           "BTCUSDT",
		Base:             "LONG",
		WinRate:          0,
		ProfitRate7d:     0,
		ProfitRate30d:    0,
		NumberOfRegister: 0,
		DailyTradeNumber: 10,
		IsActive:         true,
		IsDelete:         false,
		TimeStamp: entity.TimeStamp{
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
	},
}

func SeedBots(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, bot := range bots {
		var exists bool
		_ = pg.Db.Model(bot).
			Select("count(*) > 0").
			Where("id = ?", bot.ID).
			Find(&exists).
			Error
		if !exists {
			pg.Db.WithContext(ctx).Create(bot)
		}
	}
}
