package seeding

import (
	"binan/internal/entity"
	"binan/pkg/postgres"
	"context"
)

var TipTypes = []entity.TipType{
	{
		ID:   1,
		Name: "profit",
	},
	{
		ID:   2,
		Name: "service_fee",
	},
	{
		ID:   3,
		Name: "deposit",
	},
}

func SeedTipTypes(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, tiptype := range TipTypes {
		if pg.Db.WithContext(ctx).Model(tiptype).Where("id = ?", tiptype.ID).Updates(tiptype).RowsAffected == 0 {
			pg.Db.WithContext(ctx).Create(&tiptype)
		}
	}
}
