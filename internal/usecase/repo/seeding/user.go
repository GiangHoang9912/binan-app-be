package seeding

import (
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/pkg/postgres"
	"context"
)

var Pass = "bingvip"

var Users = []entity.User{
	{
		ID:       1,
		Email:    "admin@gmail.com",
		Phone:    "0395853000",
		Password: "3E4wk8qA7MCd",
		Role:     enums.Admin,
	},
	//{
	//	ID:       2,
	//	Email:    "master@gmail.com",
	//	Phone:    "0395853001",
	//	Password: Pass,
	//	Role:     enums.Master,
	//},
	//{
	//	ID:       3,
	//	Email:    "user1@gmail.com",
	//	Phone:    "0395853001",
	//	Password: Pass,
	//	Role:     enums.User,
	//},
}

func SeedUsers(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, user := range Users {
		user.HashPassword()
		if pg.Db.WithContext(ctx).Model(user).Where("id = ?", user.ID).Updates(user).RowsAffected == 0 {
			pg.Db.WithContext(ctx).Create(&user)
		}
	}
}
