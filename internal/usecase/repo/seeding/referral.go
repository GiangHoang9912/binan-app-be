package seeding

import (
	"binan/internal/entity"
	"binan/pkg/postgres"
	"context"
)

var Referrals = []entity.Referral{
	{
		ID:          1,
		PublisherID: 1,
		UserID:      2,
	},
	{
		ID:          2,
		PublisherID: 1,
		UserID:      3,
	},
	{
		ID:          3,
		PublisherID: 1,
		UserID:      4,
	},
}

func SeedReferrals(pg *postgres.Postgres) {
	ctx := context.Background()
	for _, referral := range Referrals {
		if pg.Db.WithContext(ctx).Model(referral).Where("id = ?", referral.ID).Updates(referral).RowsAffected == 0 {
			pg.Db.WithContext(ctx).Create(&referral)
		}
	}
}
