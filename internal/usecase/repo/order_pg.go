package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// OrderRepo -.
type OrderRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewOrderRepo -.
func NewOrderRepo(pg *postgres.Postgres) *OrderRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)
	//if err != nil {
	//	l.Error(fmt.Sprintf("SeedUsers error: %v", err.Error()))
	//}
	return &OrderRepo{pg, l}
}

// Create -.
func (u *OrderRepo) Create(ctx context.Context, user *entity.User) error {
	result := u.Db.WithContext(ctx).Create(&user)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
