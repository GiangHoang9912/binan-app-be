package repo

import (
	"binan/config"
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"errors"
	"fmt"
	"gorm.io/gorm"
)

// WalletRepo -.
type WalletRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewWalletRepo -.
func NewWalletRepo(pg *postgres.Postgres) *WalletRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)

	return &WalletRepo{pg, l}
}

func (wr *WalletRepo) GetByID(ctx context.Context, id uint) (*entity.Wallet, error) {
	var wallet entity.Wallet
	err := wr.Postgres.GetById(ctx, id, &wallet)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, fmt.Errorf(constants.UserNotFoundMessage)
		}
		return nil, err
	}

	return &wallet, nil
}

func (wr *WalletRepo) GetByType(ctx context.Context, userId uint, type_ enums.WalletType) (*entity.Wallet, error) {
	var wallet entity.Wallet
	res := wr.Db.WithContext(ctx).Where("user_id = ?", userId).Where("type = ?", type_).First(&wallet)
	if res.Error != nil {
		return nil, res.Error
	}
	return &wallet, nil
}

func (wr *WalletRepo) GetMany(ctx context.Context, userId uint) ([]entity.Wallet, error) {
	wallets := make([]entity.Wallet, 0)
	res := wr.Db.WithContext(ctx).Where("user_id = ?", userId).Find(&wallets)
	if res.Error != nil {
		return nil, res.Error
	}
	return wallets, nil
}
