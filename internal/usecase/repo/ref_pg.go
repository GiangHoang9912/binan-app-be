package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"

	"gorm.io/gorm/clause"
)

// ReferralRepo -.
type ReferralRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewReferralRepo -.
func NewReferralRepo(pg *postgres.Postgres) *ReferralRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)

	return &ReferralRepo{pg, l}
}

// Create -.
func (u *ReferralRepo) Create(ctx context.Context, referral *entity.Referral) error {
	result := u.Db.WithContext(ctx).Create(&referral)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (u *ReferralRepo) GetRef(ctx context.Context, ref entity.Referral) ([]*entity.Referral, error) {
	var referrals []*entity.Referral
	err := u.Postgres.Db.Where(ref).Preload(clause.Associations).Find(&referrals)
	if err.Error != nil {
		return nil, err.Error
	}
	return referrals, nil
}
