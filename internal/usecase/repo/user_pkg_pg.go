package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// UserPackageRepo -.
type UserPackageRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewUserPackageRepo -.
func NewUserPackageRepo(pg *postgres.Postgres) *UserPackageRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)
	//if err != nil {
	//	l.Error(fmt.Sprintf("SeedUsers error: %v", err.Error()))
	//}
	return &UserPackageRepo{pg, l}
}

// GetOne -.
func (u *OrderRepo) GetOne(ctx context.Context, userPackage *entity.UserPackage) error {
	result := u.Db.WithContext(ctx).First(&userPackage)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
