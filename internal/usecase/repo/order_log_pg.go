package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// OrderLogRepo -.
type OrderLogRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewOrderLogRepo -.
func NewOrderLogRepo(pg *postgres.Postgres) *OrderLogRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)
	return &OrderLogRepo{pg, l}
}

// CreateLog -.
func (olr *OrderLogRepo) CreateLog(ctx context.Context, input *entity.CreateOrderLogInput) error {
	result := olr.Db.WithContext(ctx).Create(&entity.OrderLog{
		OrderID:       input.OrderID,
		Message:       input.Message,
		Symbol:        input.Symbol,
		Base:          input.Base,
		OrderType:     input.OrderType,
		UserPackageID: input.UserPackageID,
	})
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (olr *OrderLogRepo) GetOrderLogs(ctx context.Context, userId uint) ([]entity.OrderLog, error) {
	orderLogs := make([]entity.OrderLog, 0)
	err := olr.Postgres.Db.WithContext(ctx).
		Table(entity.OrderLogTable).
		Select("order_logs.*").
		Joins("JOIN user_packages AS up ON order_logs.user_package_id = up.id").
		Where("up.user_id = ?", userId).
		Find(&orderLogs).
		Error
	return orderLogs, err
}
