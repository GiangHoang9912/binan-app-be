package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

// TipHistoryRepo -.
type TipHistoryRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewTipHistoryRepo -.
func NewTipHistoryRepo(pg *postgres.Postgres) *TipHistoryRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)

	return &TipHistoryRepo{pg, l}
}

// Create -.
func (u *TipHistoryRepo) Create(ctx context.Context, tipHistory *entity.TipHistory) error {
	result := u.Db.WithContext(ctx).Create(&tipHistory)

	return result.Error
}

func (u *TipHistoryRepo) GetByID(ctx context.Context, id uint) (*entity.TipHistory, error) {
	var tipHistory entity.TipHistory
	err := u.Postgres.GetById(ctx, id, &tipHistory)
	if err != nil {
		return nil, err
	}

	return &tipHistory, nil
}

func (u *TipHistoryRepo) GetAll(ctx context.Context) ([]*entity.TipHistory, error) {
	var tipHistorys []*entity.TipHistory
	err := u.Postgres.GetAll(ctx, &tipHistorys)
	if err != nil {
		return nil, err
	}
	return tipHistorys, nil
}

func CheckFilterType(tx *gorm.DB, filter *entity.TipHistory) *gorm.DB {
	if filter.TipMechanism.TipTypeID != 0 {
		return tx.Where("tip_type_id = ?", filter.TipMechanism.TipTypeID)
	}
	return tx
}

func CheckFilterUser(tx *gorm.DB, filter *entity.TipHistory) *gorm.DB {
	if filter.Referral.UserID != 0 {
		return tx.Where("user_id = ?", filter.Referral.UserID)
	}
	return tx
}
func (u *TipHistoryRepo) GetByReferral(ctx context.Context, filter *entity.TipHistory) ([]*entity.TipHistory, error) {
	tipHistories := []*entity.TipHistory{}

	sql := CheckFilterUser(CheckFilterType(u.Postgres.Db.
		Table("tip_historys").
		Joins("Referral").
		Joins("TipMechanism").
		Preload("Referral.Publisher").
		Preload("Referral.User").
		Preload(clause.Associations).
		Where("publisher_id = ? ", filter.Referral.PublisherID), filter), filter)
	err := sql.Find(&tipHistories)
	if err.Error != nil {
		return nil, err.Error
	}
	return tipHistories, nil
}
