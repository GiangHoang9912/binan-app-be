package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// LevelRepo -.
type LevelRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewLevelRepo -.
func NewLevelRepo(pg *postgres.Postgres) *LevelRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)

	return &LevelRepo{pg, l}
}

// Create -.
func (u *LevelRepo) Create(ctx context.Context, level *entity.Level) error {
	result := u.Db.WithContext(ctx).Create(&level)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (u *LevelRepo) GetByID(ctx context.Context, id uint) (*entity.Level, error) {
	var level entity.Level
	err := u.Postgres.GetById(ctx, id, &level)
	if err != nil {
		return nil, err
	}

	return &level, nil
}

func (u *LevelRepo) GetAll(ctx context.Context) ([]*entity.Level, error) {
	var levels []*entity.Level
	err := u.Postgres.GetAll(ctx, &levels)
	if err != nil {
		return nil, err
	}
	return levels, nil
}
