package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// TipTypeRepo -.
type TipTypeRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewTipTypeRepo -.
func NewTipTypeRepo(pg *postgres.Postgres) *TipTypeRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)

	return &TipTypeRepo{pg, l}
}

// Create -.
func (u *TipTypeRepo) Create(ctx context.Context, tipType *entity.TipType) error {
	result := u.Db.WithContext(ctx).Create(&tipType)

	return result.Error
}

func (u *TipTypeRepo) GetByID(ctx context.Context, id uint) (*entity.TipType, error) {
	var tipType entity.TipType
	err := u.Postgres.GetById(ctx, id, &tipType)
	if err != nil {
		return nil, err
	}

	return &tipType, nil
}

func (u *TipTypeRepo) GetAll(ctx context.Context) ([]*entity.TipType, error) {
	var tipTypes []*entity.TipType
	err := u.Postgres.GetAll(ctx, &tipTypes)
	if err != nil {
		return nil, err
	}
	return tipTypes, nil
}
