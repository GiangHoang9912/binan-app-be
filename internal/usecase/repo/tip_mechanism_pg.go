package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// TipMechanismRepo -.
type TipMechanismRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewTipMechanismRepo -.
func NewTipMechanismRepo(pg *postgres.Postgres) *TipMechanismRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)

	return &TipMechanismRepo{pg, l}
}

// Create -.
func (u *TipMechanismRepo) Create(ctx context.Context, tipMechanism *entity.TipMechanism) error {
	result := u.Db.WithContext(ctx).Create(&tipMechanism)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (u *TipMechanismRepo) GetByID(ctx context.Context, id uint) (*entity.TipMechanism, error) {
	var tipMechanism entity.TipMechanism
	err := u.Postgres.GetById(ctx, id, &tipMechanism)
	if err != nil {
		return nil, err
	}

	return &tipMechanism, nil
}

func (u *TipMechanismRepo) GetAll(ctx context.Context) ([]*entity.TipMechanism, error) {
	var tipMechanisms []*entity.TipMechanism
	err := u.Postgres.GetAll(ctx, &tipMechanisms)
	if err != nil {
		return nil, err
	}
	return tipMechanisms, nil
}

func (u *TipMechanismRepo) GetByFilter(ctx context.Context, filter *entity.TipMechanism) ([]*entity.TipMechanism, error) {
	var tipMechanisms []*entity.TipMechanism
	err := u.Postgres.Db.Where(filter).Find(&tipMechanisms)
	if err.Error != nil {
		return nil, err.Error
	}
	return tipMechanisms, nil
}

func (u *TipMechanismRepo) GetByLevelAndType(ctx context.Context, levelId uint, typeId uint) (*entity.TipMechanism, error) {
	tipMechanism := &entity.TipMechanism{}
	res := u.Postgres.Db.Where("level_id = ?", levelId).Where("tip_type_id = ?", typeId).First(tipMechanism)
	return tipMechanism, res.Error
}
