package repo

import (
	"binan/config"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
)

// BinanceKeysRepo -.
type BinanceKeysRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewBinanceKeysRepo -.
func NewBinanceKeysRepo(pg *postgres.Postgres) *BinanceKeysRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)
	return &BinanceKeysRepo{pg, l}
}

func (bkr *BinanceKeysRepo) GetAvailableKeys(ctx context.Context) ([]entity.CreateBinanceOrderInput, error) {
	binanceKeys := make([]entity.CreateBinanceOrderInput, 0)
	var err error
	err = bkr.Db.WithContext(ctx).
		Table("users").
		Select(`
			distinct bk.id, bk.*,
			concat(packages.type, packages.pair_with) as symbol,
			up.id as user_package_id,
			cm.order_value,
			cm.margin,
			cm.entry,
			cm.tp`).
		Joins("JOIN user_packages AS up ON users.id = up.user_id").
		Joins("JOIN packages ON packages.id = up.package_id").
		Joins("JOIN binance_keys AS bk ON users.id = bk.user_id").
		Joins("JOIN capital_managements AS cm ON users.id = cm.user_id AND up.id = cm.user_package_id").
		Where("up.expired_at > now()").
		Find(&binanceKeys).
		Error

	return binanceKeys, err
}

func (bkr *BinanceKeysRepo) GetApiKeyByUserId(ctx context.Context, userId uint) (*entity.BinanceKeys, error) {
	var key entity.BinanceKeys
	err := bkr.Db.WithContext(ctx).Find(&key).Where("user_id = ?", userId).Error
	if err != nil {
		return nil, err
	}
	return &key, nil
}
