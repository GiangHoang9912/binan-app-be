package repo

import (
	"binan/config"
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"errors"
	"fmt"

	"gorm.io/gorm"
)

// UserRepo -.
type UserRepo struct {
	*postgres.Postgres
	logger *logger.Logger
}

// NewUserRepo -.
func NewUserRepo(pg *postgres.Postgres) *UserRepo {
	cfg, _ := config.Get()
	l := logger.New(cfg.Level)

	//seeding.SeedUsers(pg)
	//seeding.SeedBots(pg)
	//seeding.SeedPackages(pg)
	//seeding.SeedSignals(pg)

	return &UserRepo{pg, l}
}

// Create -.
func (u *UserRepo) Create(ctx context.Context, user *entity.User) error {
	result := u.Db.WithContext(ctx).Create(&user)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (u *UserRepo) GetByID(ctx context.Context, id uint) (*entity.User, error) {
	var user entity.User
	err := u.Postgres.GetById(ctx, id, &user)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, fmt.Errorf(constants.UserNotFoundMessage)
		}
		return nil, err
	}

	return &user, nil
}

func (u *UserRepo) GetByEmail(ctx context.Context, email string) (*entity.User, error) {
	var user *entity.User
	res := u.Db.WithContext(ctx).Where("email = ?", email).First(&user)
	if res.Error != nil {
		return nil, res.Error
	}
	return user, nil
}

func (u *UserRepo) GetByPhone(ctx context.Context, phone string) (*entity.User, error) {
	var user *entity.User
	res := u.Db.WithContext(ctx).Where("phone = ?", phone).First(&user)
	if res.Error != nil {
		return nil, res.Error
	}
	return user, nil
}

func (u *UserRepo) GetByRefCode(ctx context.Context, refCode string) (*entity.User, error) {
	var user *entity.User
	res := u.Db.WithContext(ctx).Where("referral_code = ?", refCode).First(&user)
	if res.Error != nil {
		return nil, res.Error
	}
	return user, nil
}
