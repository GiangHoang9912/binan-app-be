package usecase

import (
	"binan/config"
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/internal/usecase/repo"
	"binan/pkg/logger"
	"binan/pkg/mail"
	"binan/pkg/postgres"
	"binan/pkg/redis"
	"binan/utils"
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/adshao/go-binance/v2/delivery"

	"github.com/devfeel/mapper"
	"github.com/pquerna/otp/totp"
	"gorm.io/gorm"
)

// UserUseCase -.
type UserUseCase struct {
	userRepo       IUserRepo
	referralRepo   IReferralRepo
	binanceKeyRepo *repo.BinanceKeysRepo
	*redis.Redis
	*postgres.Postgres
	logger *logger.Logger
	cfg    *config.Config
}

// NewUserUseCase -.
func NewUserUseCase(u IUserRepo, r IReferralRepo, bkr *repo.BinanceKeysRepo, rdb *redis.Redis, p *postgres.Postgres, cfg *config.Config) *UserUseCase {
	return &UserUseCase{
		userRepo:       u,
		referralRepo:   r,
		binanceKeyRepo: bkr,
		Redis:          rdb,
		Postgres:       p,
		cfg:            cfg,
	}
}

func createAuthResponse(user *entity.User) *entity.AuthUserRes {
	tokenData := entity.UserInfo{
		ID:    user.ID,
		Role:  user.Role,
		Email: user.Email,
		Phone: user.Phone,
	}
	accessToken, _ := utils.GenerateToken(tokenData, constants.AccessToken)
	refreshToken, _ := utils.GenerateToken(tokenData, constants.RefreshToken)
	return &entity.AuthUserRes{
		Token: entity.Token{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		},
		UserInfo: entity.UserInfo{
			ID:           user.ID,
			Role:         user.Role,
			Email:        user.Email,
			Phone:        user.Phone,
			ReferralCode: user.ReferralCode,
			LevelId      : user.LevelId,
		},
	}
}

// Register - create a new user.
func (uc *UserUseCase) Register(ctx context.Context, registerInfo entity.RegReqBody, refCode string) (*entity.AuthUserRes, Status, error) {
	existedUser, err := uc.userRepo.GetByEmail(ctx, registerInfo.Email)
	if existedUser != nil {
		return nil, http.StatusBadRequest, fmt.Errorf(constants.DuplicatedEmailErrorMessage)
	}

	// key := utils.GetRedisKey(mail.RreRegConfirmationKey, registerInfo.Email)
	// val, err := uc.Rdb.Get(ctx, key).Result()
	// if err != nil || val != registerInfo.ConfirmationCode {
	// 	return nil, http.StatusBadRequest, fmt.Errorf("invalid confirmation code")
	// }
	var publisher *entity.User
	if refCode != "" {
		publisher, _ = uc.userRepo.GetByRefCode(ctx, refCode)
		if publisher == nil {
			return nil, http.StatusBadRequest, fmt.Errorf(constants.InvalidRefCode)
		}

	}

	var user entity.User
	user.Email = registerInfo.Email
	user.Password = registerInfo.Password
	user.HashPassword()
	user.Role = enums.User
	user.EmailVerified = true
	user.LevelId = 1
	err = uc.userRepo.Create(ctx, &user)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	if refCode != "" {
		floor := 1
		referral := entity.Referral{
			PublisherID: publisher.ID,
			UserID:      user.ID,
			Floor:       uint(floor),
		}
		uc.referralRepo.Create(ctx, &referral)
		for {
			floor++
			ref, _ := uc.referralRepo.GetRef(ctx, entity.Referral{UserID: referral.PublisherID, Floor: 1})
			fmt.Println("referral", uc.cfg.MaxRef)
			if len(ref) <= 0 || floor > int(uc.cfg.MaxRef) {
				break
			}
			referral = entity.Referral{
				PublisherID: ref[0].PublisherID,
				UserID:      user.ID,
				Floor:       uint(floor),
			}
			uc.referralRepo.Create(ctx, &referral)
		}

	}
	respData := createAuthResponse(&user)
	// uc.Rdb.Del(ctx, key)
	return respData, http.StatusCreated, nil
}

// Login -.
func (uc *UserUseCase) Login(ctx context.Context, loginInfo entity.LoginUserReqBody) (*entity.AuthUserRes, Status, error) {
	user, _ := uc.userRepo.GetByEmail(ctx, loginInfo.Email)
	if user == nil {
		return nil, http.StatusUnauthorized, fmt.Errorf(constants.IncorrectUserCredentialsErrorMessage)
	}
	if !user.CheckHashPassword(loginInfo.Password) {
		return nil, http.StatusUnauthorized, fmt.Errorf(constants.IncorrectUserCredentialsErrorMessage)
	}

	if !user.Check2fa(loginInfo.AuthCode) {
		return nil, http.StatusUnauthorized, fmt.Errorf(constants.Invalid2faCode)
	}

	respData := createAuthResponse(user)
	return respData, http.StatusOK, nil
}

func (uc *UserUseCase) ChangePassword(ctx context.Context, userId uint, input entity.ChangePwInput) (Status, error) {
	user, _ := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return http.StatusNotFound, fmt.Errorf(constants.IncorrectUserCredentialsErrorMessage)
	}
	if !user.CheckHashPassword(input.CurrentPassword) {
		return http.StatusForbidden, fmt.Errorf(constants.IncorrectUserCredentialsErrorMessage)
	}

	user.Password = input.NewPassword
	user.HashPassword()
	uc.Db.Save(user)

	return http.StatusOK, nil
}

func (uc *UserUseCase) ForgetPassword(ctx context.Context, input entity.ForgetPwInput) (Status, error) {
	user, err := uc.userRepo.GetByEmail(ctx, input.Email)
	if user == nil {
		return http.StatusNotFound, err
	}

	key := utils.GetRedisKey(mail.ForgetPasswordKey, input.Email)
	val, err := uc.Rdb.Get(ctx, key).Result()
	if err != nil || val != input.EmailVerificationCode {
		return http.StatusBadRequest, fmt.Errorf("invalid verification code")
	}

	user.Password = input.NewPassword
	user.HashPassword()
	uc.Db.Save(user)
	uc.Rdb.Del(ctx, key)

	return http.StatusOK, nil
}

func (uc *UserUseCase) CreateAccessToken(ctx context.Context, refreshToken string) (*entity.AuthUserRes, Status, error) {
	claims, err := utils.ValidateToken(refreshToken)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	fmt.Println(claims.Data)
	userInfo := claims.Data.(map[string]interface{})
	user, _ := uc.userRepo.GetByEmail(ctx, userInfo["email"].(string))
	respData := createAuthResponse(user)
	return respData, http.StatusOK, nil
}

func (uc *UserUseCase) GetByID(ctx context.Context, id uint) (*entity.UserInfo, Status, error) {
	user, err := uc.userRepo.GetByID(ctx, id)
	if user == nil {
		return nil, http.StatusNotFound, err
	}

	userInfo := &entity.UserInfo{}
	_ = mapper.Mapper(user, userInfo)
	var sum float64
	uc.Postgres.Db.Table("transactions").Where("user_id = ?", id).Select("sum(value)").Row().Scan(&sum)
	userInfo.TotalInvestment = sum

	return userInfo, http.StatusOK, nil
}

func (uc *UserUseCase) GenerateOTP(ctx context.Context, userId uint) (*entity.GenerateOtpResponse, Status, error) {
	user, err := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, err
	}

	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      "binan.fly.dev",
		AccountName: user.Email,
		SecretSize:  15,
	})
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	otpSecret, otpAuthUrl := key.Secret(), key.URL()
	uc.Db.WithContext(ctx).Model(user).Updates(&entity.User{
		OtpAuthSecret: otpSecret,
	})

	return &entity.GenerateOtpResponse{
		Base32:     otpSecret,
		OtpAuthUrl: otpAuthUrl,
	}, http.StatusOK, nil
}

func (uc *UserUseCase) Activate2fa(ctx context.Context, userId uint, input entity.Activate2faIn) (*entity.Activate2faOut, Status, error) {
	user, err := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, err
	}

	key := utils.GetRedisKey(mail.Activate2faVerificationCodeKey, user.Email)
	val, err := uc.Rdb.Get(ctx, key).Result()
	if err != nil || val != input.EmailVerificationCode {
		return nil, http.StatusBadRequest, fmt.Errorf(constants.InvalidVerificationCode)
	}

	valid := totp.Validate(input.AuthenticatorCode, user.OtpAuthSecret)
	if !valid {
		return nil, http.StatusBadRequest, fmt.Errorf(constants.Invalid2faCode)
	}

	user.AuthenticatorEnabled = true
	uc.Db.Save(user)
	uc.Rdb.Del(ctx, key)

	userInfo := &entity.UserInfo{}
	_ = mapper.Mapper(user, userInfo)
	return &entity.Activate2faOut{OtpActivated: true, User: userInfo}, http.StatusOK, nil
}

func (uc *UserUseCase) VerifyOTP(ctx context.Context, userId uint, otpInput entity.VerifyOtpInput) (*entity.VerifyOtpOutput, Status, error) {
	user, err := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, err
	}

	valid := totp.Validate(otpInput.AuthenticatorCode, user.OtpAuthSecret)

	if valid {
		userInfo := &entity.UserInfo{}
		_ = mapper.Mapper(user, userInfo)
		return &entity.VerifyOtpOutput{OtpVerified: true, User: userInfo}, http.StatusOK, nil
	}

	return &entity.VerifyOtpOutput{OtpVerified: false, User: nil}, http.StatusOK, nil
}

func (uc *UserUseCase) DisableOTP(ctx context.Context, userId uint) (*entity.DisableOtpOutput, Status, error) {
	user, err := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, err
	}
	user.AuthenticatorEnabled = false
	uc.Db.Save(user)

	userInfo := &entity.UserInfo{}
	_ = mapper.Mapper(user, userInfo)
	return &entity.DisableOtpOutput{OtpDisabled: true, User: userInfo}, http.StatusOK, nil
}

func (uc *UserUseCase) SetOtpBackupKey(ctx context.Context, userId uint, input *entity.SetOtpBackupKeyInput) (*entity.SetOtpBackupKeyOutput, Status, error) {
	user, err := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, err
	}
	user.OtpAuthBackupKey = input.Code
	uc.Db.Save(user)

	userInfo := &entity.UserInfo{}
	_ = mapper.Mapper(user, userInfo)
	return &entity.SetOtpBackupKeyOutput{OtpBackupKeyEnabled: true, User: userInfo}, http.StatusOK, nil
}

func (uc *UserUseCase) FollowMaster(ctx context.Context, userId uint, followUser entity.FollowUserInput) (*entity.UserFollow, Status, error) {
	if userId == followUser.FollowerID {
		return nil, http.StatusBadRequest, errors.New(constants.InvalidMaster)
	}
	master, _ := uc.userRepo.GetByID(ctx, followUser.FollowerID)
	if master == nil {
		return nil, http.StatusNotFound, errors.New(constants.UserNotFoundMessage)
	}
	if master.Role != enums.Master {
		return nil, http.StatusBadRequest, errors.New(constants.InvalidMaster)
	}
	user, _ := uc.userRepo.GetByID(ctx, userId)
	if user == nil {
		return nil, http.StatusNotFound, errors.New(constants.UserNotFoundMessage)
	}

	userFollow := entity.UserFollow{
		UserID:     userId,
		FollowerID: followUser.FollowerID,
		Balance:    followUser.Balance,
	}
	err := uc.Db.Model(&entity.UserFollow{}).
		Where("user_id = ?", userId).
		Where("following_id = ?", followUser.FollowerID).
		First(&userFollow).
		Error
	if err != nil {
		uc.Db.Create(&userFollow)
	} else {
		newBalanceTrade := userFollow.Balance + followUser.Balance
		userFollow.Balance = newBalanceTrade
		uc.Db.Save(&userFollow)
	}

	return &userFollow, http.StatusCreated, nil
}

func (uc *UserUseCase) UnFollow(ctx context.Context, userId uint, followUser entity.FollowUserInput) (*entity.UserFollow, Status, error) {
	var follow entity.UserFollow
	err := uc.Db.Model(&entity.UserFollow{}).
		WithContext(ctx).
		Where("user_id = ?", userId).
		Where("following_id = ?", followUser.FollowerID).
		First(&follow).
		Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, http.StatusNotFound, err
	}
	err = uc.Db.Delete(&follow).Error
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	return nil, http.StatusNoContent, nil
}

func (uc *UserUseCase) GetMasters(ctx context.Context) ([]entity.MasterInfo, Status, error) {
	masters := make([]entity.MasterInfo, 0)
	binanceKey, err := uc.binanceKeyRepo.GetApiKeyByUserId(ctx, 1)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}
	binanceClient := delivery.NewClient(binanceKey.ApiKey, binanceKey.SecretKey)
	//acc, _ := binanceClient.NewGetAccountService().Do(ctx)
	bl, err := binanceClient.NewGetBalanceService().Do(ctx)
	for _, b := range bl {
		fmt.Println(b.CrossUnPnl)
	}

	uc.Postgres.Db.WithContext(ctx).
		Raw(`Select * from (SELECT users.id, users.role, users.email, users.phone, users.created_at, count(uf.user_id) as total_followers
			FROM users LEFT JOIN  user_follows AS uf
			ON uf.following_id = users.id
			group by users.id) as New WHERE New.role = 'MASTER'
		`).Find(&masters)

	return masters, http.StatusOK, nil
}

func (uc *UserUseCase) GetFollowers(ctx context.Context, userId uint) ([]entity.FollowerInfo, Status, error) {
	var master entity.User
	err := uc.Postgres.Db.Where("id = ?", userId).First(&master).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, http.StatusNotFound, errors.New(fmt.Sprintf("user with id %d not found", userId))
	} else if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	if master.Role != enums.Master {
		return nil, http.StatusForbidden, errors.New(fmt.Sprintf("user with id %d is not a master", userId))
	}

	followers := make([]entity.FollowerInfo, 0)

	uc.Postgres.Db.WithContext(ctx).
		Table("user_follows AS uf").
		Select(`distinct users.id, users.role, users.email, users.phone`).
		Joins("LEFT JOIN users ON users.id = uf.user_id").
		Where("users.role = ?", "USER").
		Where("uf.following_id = ?", userId).
		Find(&followers)

	return followers, http.StatusOK, nil
}

func (uc *UserUseCase) GetFollowings(ctx context.Context, userId uint) ([]entity.UserFollow, Status, error) {
	followings := make([]entity.UserFollow, 0)

	uc.Postgres.Db.Model(&entity.UserFollow{}).
		WithContext(ctx).
		Select(`users.id, users.role, users.email, users.phone, user_follows.*`).
		Joins("INNER JOIN users ON users.id = user_follows.user_id").
		Where("users.id = ?", userId).
		Find(&followings)

	return followings, http.StatusOK, nil
}

func (uc *UserUseCase) GetUserWallet(ctx context.Context, userId uint, balanceType string) (*entity.WalletInfo, Status, error) {
	walletInfo := new(entity.WalletInfo)
	uc.Postgres.Db.WithContext(ctx).
		Table("users").
		Joins("INNER JOIN wallets as ws ON users.id = ws.user_id").
		Select("ws.id, ws.type, ws.balance").
		Where("ws.type", balanceType).
		Where("users.id = ?", userId).
		First(&walletInfo)

	return walletInfo, http.StatusOK, nil
}

func (uc *UserUseCase) SetNewBalance(ctx context.Context, userId uint, balance float64, balanceTpe string) (entity.Wallet, Status, error) {
	var walletFuture entity.Wallet
	err := uc.Db.Model(&entity.Wallet{}).
		WithContext(ctx).
		Where("user_id = ?", userId).
		Where("type", balanceTpe).
		First(&walletFuture).
		Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return walletFuture, http.StatusNotFound, err
	}

	walletFuture.Balance = balance

	err = uc.Db.Save(&walletFuture).Error

	if err != nil {
		return walletFuture, http.StatusInternalServerError, err
	}
	return walletFuture, http.StatusNoContent, nil
}
