package usecase

import (
	"binan/pkg/logger"
	"context"
	_ "embed"
	"encoding/json"
	"fmt"
)

type Account struct {
	AccountName string `json:"account_name"`
	PaymentId   string `json:"payment_id"`
	QrUrl       string `json:"qr_url"`
	image       any
}

type AccountPaymentUseCase struct {
	accountPaymentRepo IAccountPayment
	logger             *logger.Logger
}

var (
	//go:embed assets/account.json
	file string
)

func (apc *AccountPaymentUseCase) GetPaymentInformation(_ context.Context) (error, any) {
	data := Account{}
	if err := json.Unmarshal([]byte(file), &data); err != nil {
		fmt.Printf("failed to unmarshal json file, error: %v", err)
		return err, nil
	}
	return nil, data
}

// NewAccountPaymentUseCase -.
func NewAccountPaymentUseCase() *AccountPaymentUseCase {
	return &AccountPaymentUseCase{}
}
