package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"net/http"
)

// CapitalManagementsUseCase -.
type CapitalManagementsUseCase struct {
	userRepo IUserRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewCapitalManagementsUseCase -.
func NewCapitalManagementsUseCase(ur IUserRepo, p *postgres.Postgres) *CapitalManagementsUseCase {
	return &CapitalManagementsUseCase{
		userRepo: ur,
		Postgres: p,
	}
}

func (cu *CapitalManagementsUseCase) CreateCapitalManagementsPackage(ctx context.Context, input entity.CreateCapitalManagementsInput, userId uint) (*entity.CapitalManagements, Status, error) {
	user, _ := cu.userRepo.GetByID(ctx, userId)

	if user == nil {
		return nil, http.StatusNotFound, fmt.Errorf(constants.UserNotFoundMessage)
	}

	var userPkg entity.UserPackage
	err := cu.Postgres.GetById(ctx, input.UserPackageId, &userPkg)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, http.StatusNotFound, fmt.Errorf("package not found")
		}
		return nil, http.StatusInternalServerError, err
	}

	CapitalManagement := &entity.CapitalManagements{
		OrderValue:    input.OrderValue,
		Margin:        input.Margin,
		Entry:         input.Entry,
		Tp:            input.Tp,
		StopLoss:      input.StopLoss,
		LimitValue:    input.LimitValue,
		UserID:        userId,
		UserPackageID: input.UserPackageId,
		Disabled:      input.Disabled,
	}
	err = cu.Postgres.Create(ctx, CapitalManagement)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	return CapitalManagement, http.StatusCreated, nil
}
