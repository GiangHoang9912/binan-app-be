package usecase

import (
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"net/http"
)

// WalletUseCase -.
type LevelUseCase struct {
	levelRepo ILevelRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewLevelUseCase -.
func NewLevelUseCase(lr ILevelRepo, p *postgres.Postgres, l *logger.Logger) *LevelUseCase {
	return &LevelUseCase{
		levelRepo: lr,
		Postgres:  p,
		logger:    l,
	}
}

func (lu *LevelUseCase) GetAllLevels(ctx context.Context) ([]*entity.Level, error) {
	return lu.levelRepo.GetAll(ctx)
}

func (lu *LevelUseCase) GetById(ctx context.Context, levelId uint) (*entity.Level, error) {
	return lu.levelRepo.GetByID(ctx, levelId)
}

func (lu *LevelUseCase) Create(ctx context.Context, level *entity.Level) (Status, error) {
	err := lu.levelRepo.Create(ctx, level)
	if err != nil {
		return http.StatusForbidden, err
	}
	return http.StatusOK, err
}
