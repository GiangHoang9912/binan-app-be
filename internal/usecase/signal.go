package usecase

import (
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"errors"
	"time"
)

// SignalUseCase -.
type SignalUseCase struct {
	signalRepo ISignalRepo
	*postgres.Postgres
	logger *logger.Logger
}

func (sc *SignalUseCase) GetSignals(ctx context.Context, botId uint, userId uint) (error, []entity.Signal) {
	var pkg entity.Package
	err := sc.Postgres.Db.WithContext(ctx).First(&pkg, "bot_id = ?", botId).Error
	if err != nil {
		return err, nil
	}

	var userPkg entity.UserPackage
	err = sc.Postgres.Db.WithContext(ctx).Where("user_id = ?", userId).Where("package_id", pkg.ID).First(&userPkg).Error
	if err != nil {
		return err, nil
	}

	now := time.Now()
	if userPkg.ExpiredAt.Before(now) {
		return errors.New("package is expired"), nil
	}

	signals := make([]entity.Signal, 0)
	sc.Postgres.Db.WithContext(ctx).Where("bot_id = ?", botId).Where("is_delete = ?", false).Find(&signals)
	return nil, signals
}

// NewSignalUseCase -.
func NewSignalUseCase(s ISignalRepo, p *postgres.Postgres) *SignalUseCase {
	return &SignalUseCase{
		signalRepo: s,
		Postgres:   p,
	}
}
