package usecase

import (
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"net/http"
)

// TipHistoryUseCase -.
type TipHistoryUseCase struct {
	tipHistoryRepo ITipHistoryRepo
	referalRepo    IReferralRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewTipHistoryUseCase -.
func NewTipHistoryUseCase(mr ITipHistoryRepo, rr IReferralRepo, p *postgres.Postgres, l *logger.Logger) *TipHistoryUseCase {
	return &TipHistoryUseCase{
		tipHistoryRepo: mr,
		referalRepo:    rr,
		Postgres:       p,
		logger:         l,
	}
}

func (mu *TipHistoryUseCase) GetAllLevel(ctx context.Context) ([]*entity.TipHistory, error) {
	return mu.tipHistoryRepo.GetAll(ctx)
}

func (mu *TipHistoryUseCase) GetById(ctx context.Context, tipHistoryId uint) (*entity.TipHistory, error) {
	return mu.tipHistoryRepo.GetByID(ctx, tipHistoryId)
}

func (mu *TipHistoryUseCase) Create(ctx context.Context, tipHistory *entity.TipHistory) (Status, error) {
	err := mu.tipHistoryRepo.Create(ctx, tipHistory)
	if err != nil {
		return http.StatusForbidden, err
	}
	return http.StatusOK, err
}

func (mu *TipHistoryUseCase) GetTipHistoryByUser(ctx context.Context, filter *entity.TipHistory, userID uint) ([]*entity.TipHistory, error) {
	tipHistories, err := mu.tipHistoryRepo.GetByReferral(ctx, filter)
	if err != nil {
		return nil, err
	}
	return tipHistories, err
}
