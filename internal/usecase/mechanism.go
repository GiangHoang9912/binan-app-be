package usecase

import (
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"net/http"
)

// MechanismUseCase -.
type MechanismUseCase struct {
	mechanismRepo ITipMechanismRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewMechanismUseCase -.
func NewMechanismUseCase(mr ITipMechanismRepo, p *postgres.Postgres, l *logger.Logger) *MechanismUseCase {
	return &MechanismUseCase{
		mechanismRepo: mr,
		Postgres:      p,
		logger:        l,
	}
}

func (mu *MechanismUseCase) GetAllMechanism(ctx context.Context) ([]*entity.TipMechanism, error) {
	return mu.mechanismRepo.GetAll(ctx)
}

func (mu *MechanismUseCase) GetMechanismByFilter(ctx context.Context, filter *entity.TipMechanism) ([]*entity.TipMechanism, error) {
	mechanism, err := mu.mechanismRepo.GetByFilter(ctx, filter)
	return mechanism, err
}
func (mu *MechanismUseCase) GetById(ctx context.Context, mechanismId uint) (*entity.TipMechanism, error) {
	return mu.mechanismRepo.GetByID(ctx, mechanismId)
}

func (mu *MechanismUseCase) Create(ctx context.Context, mechanism *entity.TipMechanism) (Status, error) {
	err := mu.mechanismRepo.Create(ctx, mechanism)
	if err != nil {
		return http.StatusForbidden, err
	}
	return http.StatusOK, err
}
