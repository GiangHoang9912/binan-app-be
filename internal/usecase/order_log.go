package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/usecase/repo"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"binan/utils"
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/adshao/go-binance/v2/futures"
)

// OrderLogUseCase -.
type OrderLogUseCase struct {
	*postgres.Postgres
	orderLogRepo   *repo.OrderLogRepo
	binanceKeyRepo *repo.BinanceKeysRepo
	logger         *logger.Logger
}

func (ou *OrderLogUseCase) GetAll(ctx context.Context, userId uint) ([]*futures.PositionRisk, Status, error) {
	binanceKey, err := ou.binanceKeyRepo.GetApiKeyByUserId(ctx, userId)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	orderLogs, err := ou.orderLogRepo.GetOrderLogs(ctx, userId)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}
	futureClient := futures.NewClient(binanceKey.ApiKey, binanceKey.SecretKey)

	var symbols []string
	for _, v := range orderLogs {
		symbols = append(symbols, v.Symbol)

	}
	symbols = utils.RemoveDuplicate(symbols)
	positionRisks := make([]*futures.PositionRisk, 0)
	for _, s := range symbols {
		openOrders, err := futureClient.NewGetPositionRiskService().Symbol(s).
			Do(ctx)
		if err != nil {
			fmt.Println("error: ", err)
		} else {
			for _, v := range openOrders {
				positionRisks = append(positionRisks, v)
			}
		}
	}
	for _, v := range positionRisks {
		fmt.Println(v)
	}

	return positionRisks, http.StatusCreated, nil
}

func (ou *OrderLogUseCase) GetAllSpot(ctx context.Context, userId int) ([]entity.OrderLog, Status, error) {
	orderArray := make([]entity.OrderLog, 0)
	ou.Postgres.Db.WithContext(ctx).Where("user_id = ?", userId).Find(&orderArray)
	return orderArray, 200, nil
}

// NewOrderLogUseCase -.
func NewOrderLogUseCase(olr *repo.OrderLogRepo, bkr *repo.BinanceKeysRepo, p *postgres.Postgres) *OrderLogUseCase {
	return &OrderLogUseCase{
		orderLogRepo:   olr,
		binanceKeyRepo: bkr,
		Postgres:       p,
	}
}
