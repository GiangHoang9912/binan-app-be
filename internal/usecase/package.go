package usecase

import (
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"net/http"
)

// PackageUseCase -.
type PackageUseCase struct {
	userRepo IUserRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewPackageUseCase -.
func NewPackageUseCase(ur IUserRepo, p *postgres.Postgres, l *logger.Logger) *PackageUseCase {
	return &PackageUseCase{
		userRepo: ur,
		Postgres: p,
		logger:   l,
	}
}

func (pu *PackageUseCase) GetAllPackages(ctx context.Context) ([]entity.Package, Status, error) {
	packages := make([]entity.Package, 0)
	pu.Postgres.Db.WithContext(ctx).Find(&packages)
	return packages, http.StatusOK, nil
}

func (pu *PackageUseCase) GetPurchasedPackages(ctx context.Context, userId uint) ([]entity.UserPackage, Status, error) {
	userPkgs := make([]entity.UserPackage, 0)
	pu.Postgres.Db.WithContext(ctx).Where("user_id = ?", userId).Find(&userPkgs)
	return userPkgs, http.StatusOK, nil
}

func (pu *PackageUseCase) GetTotalInvestment(ctx context.Context, userId uint) ([]entity.UserPackage, Status, error) {
	userPkgs := make([]entity.UserPackage, 0)
	pu.Postgres.Db.WithContext(ctx).Joins("Package").Where("user_id = ?", userId).Select("sum(salary) as total")
	return userPkgs, http.StatusOK, nil
}
