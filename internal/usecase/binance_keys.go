package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"errors"
	"gorm.io/gorm"
	"net/http"
)

// BinanceKeysUseCase -.
type BinanceKeysUseCase struct {
	userRepo IUserRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewBinanceKeysUseCase -.
func NewBinanceKeysUseCase(ur IUserRepo, p *postgres.Postgres) *BinanceKeysUseCase {
	return &BinanceKeysUseCase{
		userRepo: ur,
		Postgres: p,
	}
}

func (bc *BinanceKeysUseCase) SaveBinanceKey(ctx context.Context, body entity.SaveBinanceKeyInput, u uint) (*entity.BinanceKeys, Status, error) {
	user, err := bc.userRepo.GetByID(ctx, u)
	if user == nil {
		return nil, http.StatusNotFound, err
	}

	keys := new(entity.BinanceKeys)
	err = bc.Postgres.Db.WithContext(ctx).Where("user_id = ?", u).First(&keys).Error

	BinanceKeys := &entity.BinanceKeys{
		ApiKey:    body.ApiKey,
		SecretKey: body.SecretKey,
		UserID:    u,
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		err = bc.Postgres.Create(ctx, BinanceKeys)
	} else {
		keys.SecretKey = BinanceKeys.SecretKey
		keys.ApiKey = BinanceKeys.ApiKey
		bc.Postgres.Db.Save(keys)
	}

	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	return BinanceKeys, http.StatusCreated, nil
}

func (bc *BinanceKeysUseCase) GetBinanceKey(ctx context.Context, u uint) ([]entity.BinanceKeys, Status, error) {
	user, err := bc.userRepo.GetByID(ctx, u)
	if user == nil {
		return nil, http.StatusNotFound, err
	}

	keys := make([]entity.BinanceKeys, 0)
	err = bc.Postgres.Db.WithContext(ctx).Where("user_id = ?", u).First(&keys).Error

	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	return keys, http.StatusCreated, nil
}
