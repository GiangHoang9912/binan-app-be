// Package usecase implements application business logic. Each logic group in own file.
package usecase

import (
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/pkg/mail"
	"context"

	"github.com/adshao/go-binance/v2"

	"github.com/adshao/go-binance/v2/futures"
)

//go:generate mockgen -source=interfaces.go -destination=./mocks_test.go -package=usecase_test

type (
	// Translation -.
	Translation interface {
		Translate(context.Context, entity.Translation) (entity.Translation, error)
		History(context.Context) ([]entity.Translation, error)
	}

	// TranslationRepo -.
	TranslationRepo interface {
		Store(context.Context, entity.Translation) error
		GetHistory(context.Context) ([]entity.Translation, error)
	}

	// TranslationWebAPI -.
	TranslationWebAPI interface {
		Translate(entity.Translation) (entity.Translation, error)
	}
)

type (
	IUser interface {
		Register(ctx context.Context, user entity.RegReqBody, refCode string) (*entity.AuthUserRes, Status, error)
		Login(ctx context.Context, user entity.LoginUserReqBody) (*entity.AuthUserRes, Status, error)
		ChangePassword(ctx context.Context, userId uint, input entity.ChangePwInput) (Status, error)
		ForgetPassword(ctx context.Context, user entity.ForgetPwInput) (Status, error)
		CreateAccessToken(ctx context.Context, refreshToken string) (*entity.AuthUserRes, Status, error)
		GetByID(ctx context.Context, id uint) (*entity.UserInfo, Status, error)
		GenerateOTP(ctx context.Context, id uint) (*entity.GenerateOtpResponse, Status, error)
		Activate2fa(ctx context.Context, userId uint, input entity.Activate2faIn) (*entity.Activate2faOut, Status, error)
		VerifyOTP(ctx context.Context, userId uint, otpInput entity.VerifyOtpInput) (*entity.VerifyOtpOutput, Status, error)
		DisableOTP(ctx context.Context, userId uint) (*entity.DisableOtpOutput, Status, error)
		SetOtpBackupKey(ctx context.Context, userId uint, input *entity.SetOtpBackupKeyInput) (*entity.SetOtpBackupKeyOutput, Status, error)
		FollowMaster(ctx context.Context, userId uint, followUser entity.FollowUserInput) (*entity.UserFollow, Status, error)
		UnFollow(ctx context.Context, userId uint, followUser entity.FollowUserInput) (*entity.UserFollow, Status, error)
		GetMasters(ctx context.Context) ([]entity.MasterInfo, Status, error)
		GetFollowers(ctx context.Context, userId uint) ([]entity.FollowerInfo, Status, error)
		GetFollowings(ctx context.Context, userId uint) ([]entity.UserFollow, Status, error)
		GetUserWallet(ctx context.Context, userId uint, balanceType string) (*entity.WalletInfo, Status, error)
		SetNewBalance(ctx context.Context, userId uint, balanceAfterFee float64, balanceType string) (entity.Wallet, Status, error)
	}

	IUserRepo interface {
		Create(ctx context.Context, user *entity.User) error
		GetByID(ctx context.Context, id uint) (*entity.User, error)
		GetByEmail(ctx context.Context, email string) (*entity.User, error)
		GetByPhone(ctx context.Context, phone string) (*entity.User, error)
		GetByRefCode(ctx context.Context, refCode string) (*entity.User, error)
	}

	IMail interface {
		SendEmail(ctx context.Context, input mail.SendMailInput) (Status, error)
	}
)

type (
	IBot interface {
		GetAllBot(ctx context.Context) (error, []entity.Bot)
		GetBot(ctx context.Context, botId uint) (error, *entity.Bot)
	}

	IBotRepo interface {
	}
)

type (
	ISignal interface {
		GetSignals(ctx context.Context, botId uint, userId uint) (error, []entity.Signal)
	}

	ISignalRepo interface {
	}
)

type (
	IOrder interface {
		CreateOrder(ctx context.Context, userId uint, input entity.CreateOrderInput) (*entity.Order, Status, error)
	}
)

type (
	IOrderLog interface {
		GetAll(ctx context.Context, userId uint) ([]*futures.PositionRisk, Status, error)
		GetAllSpot(ctx context.Context, userId int) ([]entity.OrderLog, Status, error)
	}
)

type (
	IAccountPayment interface {
		GetPaymentInformation(ctx context.Context) (error, any)
	}
)

type (
	IWallet interface {
		GetUserWallets(ctx context.Context, userId uint) ([]entity.Wallet, Status, error)
		MakeDeposit(ctx context.Context, depositInput entity.MakeDepositInput) (*entity.DepositLog, Status, error)
		ProcessDeposit(ctx context.Context, depositInput entity.ProcessDepositInput) (*entity.DepositLog, Status, error)
		PurchasePackage(ctx context.Context, userId uint, purchasePackageInput entity.PurchasePackageInput) (*entity.UserPackage, Status, error)
		GetDepositLogs(ctx context.Context) ([]entity.DepositLog, Status, error)
		GetTransactions(ctx context.Context, userId uint) ([]entity.Transaction, Status, error)
		GetAvailableBinanceKeys(ctx context.Context) ([]entity.CreateBinanceOrderInput, error)
		SetNewBalance(ctx context.Context, userId uint, balance float64, balanceTpe string) (entity.Wallet, Status, error)
		GetDepositByID(ctx context.Context, depositId uint) (*entity.DepositLog, Status, error)
		GetUserWalletWithType(ctx context.Context, userId uint, balanceType string) (*entity.WalletInfo, Status, error)
	}

	IWalletRepo interface {
		GetByID(ctx context.Context, id uint) (*entity.Wallet, error)
		GetByType(ctx context.Context, userId uint, type_ enums.WalletType) (*entity.Wallet, error)
		GetMany(ctx context.Context, userId uint) ([]entity.Wallet, error)
	}
)

type (
	IPackage interface {
		GetAllPackages(ctx context.Context) ([]entity.Package, Status, error)
		GetPurchasedPackages(ctx context.Context, userId uint) ([]entity.UserPackage, Status, error)
	}
)

type (
	IUserPackages interface {
		GetUserPackageUser(ctx context.Context, userId uint) ([]entity.UserPackage, Status, error)
	}
)

type (
	ICapitalManagements interface {
		CreateCapitalManagementsPackage(ctx context.Context, body entity.CreateCapitalManagementsInput, u uint) (*entity.CapitalManagements, Status, error)
	}

	ICapitalManagementsRepo interface {
	}
)

type (
	IBinanceKeys interface {
		SaveBinanceKey(ctx context.Context, body entity.SaveBinanceKeyInput, u uint) (*entity.BinanceKeys, Status, error)
		GetBinanceKey(ctx context.Context, u uint) ([]entity.BinanceKeys, Status, error)
	}

	IBinanceKeysRepo interface {
		GetAvailableKeys(ctx context.Context) ([]entity.CreateBinanceOrderInput, error)
	}
)

type (
	ILevelRepo interface {
		Create(ctx context.Context, level *entity.Level) error
		GetByID(ctx context.Context, id uint) (*entity.Level, error)
		GetAll(ctx context.Context) ([]*entity.Level, error)
	}

	ILevel interface {
		GetAllLevels(ctx context.Context) ([]*entity.Level, error)
	}
)

type (
	ITipTypeRepo interface {
		Create(ctx context.Context, tipType *entity.TipType) error
		GetByID(ctx context.Context, id uint) (*entity.TipType, error)
		GetAll(ctx context.Context) ([]*entity.TipType, error)
	}
)

type (
	ITipMechanismRepo interface {
		GetByID(ctx context.Context, id uint) (*entity.TipMechanism, error)
		GetAll(ctx context.Context) ([]*entity.TipMechanism, error)
		Create(ctx context.Context, mechanism *entity.TipMechanism) error
		GetByFilter(ctx context.Context, filter *entity.TipMechanism) ([]*entity.TipMechanism, error)
		GetByLevelAndType(ctx context.Context, levelId uint, typeId uint) (*entity.TipMechanism, error)
	}
)

type (
	IReferralRepo interface {
		Create(ctx context.Context, referral *entity.Referral) error
		GetRef(ctx context.Context, ref entity.Referral) ([]*entity.Referral, error)
	}
)

type (
	ITip interface {
		Commission(ctx context.Context, userId uint, amount float64, typeTipId uint) (Status, error)
	}
	ITipType interface {
		GetAllTipType(ctx context.Context) ([]*entity.TipType, error)
	}
	ITipHistory interface {
		GetTipHistoryByUser(ctx context.Context, filter *entity.TipHistory, userID uint) ([]*entity.TipHistory, error)
	}
	ITipMechanism interface {
		GetMechanismByFilter(ctx context.Context, filter *entity.TipMechanism) ([]*entity.TipMechanism, error)
		GetAllMechanism(ctx context.Context) ([]*entity.TipMechanism, error)
	}
	IReferral interface {
		GetRefByFilter(ctx context.Context, ref entity.Referral) ([]*entity.Referral, Status, error)
	}
)
type (
	ITipHistoryRepo interface {
		Create(ctx context.Context, tip *entity.TipHistory) error
		GetByID(ctx context.Context, id uint) (*entity.TipHistory, error)
		GetAll(ctx context.Context) ([]*entity.TipHistory, error)
		GetByReferral(ctx context.Context, filter *entity.TipHistory) ([]*entity.TipHistory, error)
	}
)

type (
	ICopyTrade interface {
		SetCopyTrade(ctx context.Context, userId uint, input entity.CopyTradeSettingInput) (*entity.CopyTrade, Status, error)
		CopyTradeFuture(ctx context.Context, userId uint, input entity.CopyTradeFutureInput) (Status, error)
		GetFutureOrder(ctx context.Context, userId uint, input entity.GetSpotInfoInput) (*futures.Order, Status, error)
		CopyTradeSpot(ctx context.Context, userId uint, input entity.CopyTradeSpotInput) (*binance.CreateOrderResponse, Status, error)
		GetSpotOrder(ctx context.Context, userId uint, input entity.GetSpotInfoInput) (*binance.Order, Status, error)
		GetFollowings(ctx context.Context, userId uint) ([]entity.UserFollow, Status, error)
		CreateLog(ctx context.Context, input entity.CopyTradeSpotInput, order *binance.CreateOrderResponse, userId uint) error
		CreateLogTest(ctx context.Context, input entity.CopyTradeSpotInput, userId uint) error
	}
)
