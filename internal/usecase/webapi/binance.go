package webapi

import (
	"binan/internal/entity"
	"binan/internal/usecase/repo"
	"binan/pkg/logger"
	"context"
	"errors"
	"fmt"
	"strconv"

	"github.com/adshao/go-binance/v2/futures"
)

// Binance -.
type Binance struct {
	binanceKeysRepo *repo.BinanceKeysRepo
	orderLogRepo    *repo.OrderLogRepo
	logger          *logger.Logger
}

// NewBinanceClient -.
func NewBinanceClient(bkr *repo.BinanceKeysRepo, olr *repo.OrderLogRepo, l *logger.Logger) *Binance {
	return &Binance{
		binanceKeysRepo: bkr,
		orderLogRepo:    olr,
		logger:          l,
	}
}

// CreateBinanceOrders symbol: "BNBETH"
func (b *Binance) CreateBinanceOrders(signal entity.Signal) ([]*futures.CreateOrderResponse, error) {
	orders := make([]*futures.CreateOrderResponse, 0)
	var type_ futures.SideType
	if signal.Base == "LONG" {
		type_ = futures.SideTypeBuy
	} else {
		type_ = futures.SideTypeSell
	}
	ctx := context.Background()
	apiKeys, err := b.binanceKeysRepo.GetAvailableKeys(context.Background())
	if err != nil {
		b.logger.Error("GetAvailableKeys error: ", err.Error())
	}
	fmt.Println("apiKeys: ", apiKeys)

	for _, k := range apiKeys {
		futureClient := futures.NewClient(k.ApiKey, k.SecretKey)
		var price float64
		fmt.Println(k.Entry, signal.Entry1, signal.Entry2, signal.Entry3)
		switch k.Entry {
		case 1:
			price = signal.Entry1
		case 2:
			price = signal.Entry2
		case 3:
			price = signal.Entry3
		}
		symbol, err := futureClient.NewChangeLeverageService().Symbol(k.Symbol).Leverage(int(k.Margin)).Do(ctx)
		fmt.Println("leverage", symbol, err)
		err = futureClient.NewChangeMarginTypeService().Symbol(k.Symbol).MarginType(futures.MarginTypeIsolated).Do(ctx)
		fmt.Println("margin type", err)
		prices, err := futureClient.NewListPricesService().Symbol(k.Symbol).Do(ctx)
		var symbolPrice float64
		if len(prices) > 0 {
			symbolPrice, _ = strconv.ParseFloat(prices[len(prices)-1].Price, 32)
		} else {
			return nil, errors.New("something went wrong")
		}
		quantity := k.OrderValue * float64(k.Margin) / symbolPrice
		fmt.Println("quantity: ", k.OrderValue, k.Margin, symbolPrice, quantity)
		// quantity = 0.001
		order, err := futureClient.NewCreateOrderService().Symbol(k.Symbol).
			Side(type_).Type(futures.OrderTypeLimit).
			TimeInForce(futures.TimeInForceTypeGTC).Quantity(fmt.Sprintf("%.3f", quantity)).
			Price(fmt.Sprintf("%.1f", price)).Do(ctx)
		if err != nil {
			b.logger.Error("CreateBinanceOrders error: ", err.Error())
			return orders, err
		}
		err = b.orderLogRepo.CreateLog(ctx, &entity.CreateOrderLogInput{
			Message:       "",
			Symbol:        k.Symbol,
			Base:          signal.Base,
			OrderID:       fmt.Sprintf("%d", order.OrderID),
			UserPackageID: k.UserPackageID,
		})
		if err != nil {
			fmt.Println("CreateLog err: ", err)
		}
		orders = append(orders, order)
	}
	fmt.Println(orders)

	return orders, nil
}

func WebSocket(consumer chan futures.WsAccountUpdate, closeC chan struct{}) {
	doneC := make(chan struct{})
	stopC := make(chan struct{})
	fmt.Println(stopC)
	wsUserDataHandler := func(event *futures.WsUserDataEvent) {
		fmt.Println(event.AccountUpdate)
		consumer <- event.AccountUpdate
	}
	go func() {
		client := futures.NewClient("ueY7R80UCI6daxVQeUucz4T59YXR1ALbs9caUpxkbYxUEfWu9xMBoIaJ7QpYtFXK", "6VTfheIWn9dBcOh4L3bVCowvGRzABQ3XAfq8c1wtN16pWZrL4euy71wqN0eSR3Hf")
		// client.NewGetOpenOrderService()
		openOrders, err := client.NewGetPositionRiskService().Symbol("BTCUSDT").
			Do(context.Background())
		if err != nil {
			fmt.Println("error: ", err)
		}
		fmt.Println(openOrders)
		for _, o := range openOrders {
			fmt.Println(o)
		}
		prices, err := client.NewListPricesService().Symbol("BTCUSDT").Do(context.Background())
		for i, p := range prices {
			fmt.Println(i, p)
		}
	}()

	//go func() {
	//	for {
	//		_, ok := <-closeC
	//		fmt.Println(ok)
	//		if ok {
	//			<-stopC
	//			break
	//		}
	//	}
	//}()
	errHandler := func(err error) {
		fmt.Println(err)
	}
	var err error
	doneC, stopC, err = futures.WsUserDataServe("qpg46j0BveFO48GBgc6XcEOrA7qwa5RZ026VoVpj52IciyJGgES8T2kBuG3IyJJO", wsUserDataHandler, errHandler)
	if err != nil {
		fmt.Println(err)
		return
	}
	<-doneC
}
