package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/internal/usecase/repo"
	"binan/pkg/postgres"
	"context"
	"errors"
	"fmt"
	"github.com/adshao/go-binance/v2"
	"github.com/adshao/go-binance/v2/futures"
	"gorm.io/gorm"
	"net/http"
)

// CopyTradeUseCase -.
type CopyTradeUseCase struct {
	userRepo       IUserRepo
	binanceKeyRepo *repo.BinanceKeysRepo
	orderLogRepo   *repo.OrderLogRepo
	*postgres.Postgres
}

// NewCopyTradeUseCase -.
func NewCopyTradeUseCase(ur IUserRepo, bkr *repo.BinanceKeysRepo, olr *repo.OrderLogRepo, p *postgres.Postgres) *CopyTradeUseCase {
	return &CopyTradeUseCase{
		userRepo:       ur,
		binanceKeyRepo: bkr,
		orderLogRepo:   olr,
		Postgres:       p,
	}
}

func (ctu *CopyTradeUseCase) SetCopyTrade(ctx context.Context, userId uint, input entity.CopyTradeSettingInput) (*entity.CopyTrade, Status, error) {
	user, _ := ctu.userRepo.GetByID(ctx, userId)

	if user == nil {
		return nil, http.StatusNotFound, fmt.Errorf(constants.UserNotFoundMessage)
	}

	var copyTrade entity.CopyTrade
	err := ctu.Postgres.Db.Where("user_id = ?", userId).First(&copyTrade).Error
	copyTrade.UserID = userId
	copyTrade.Value = input.Value
	copyTrade.DailyValue = input.DailyValue
	copyTrade.TotalDeposit = input.TotalDeposit
	copyTrade.StopLossRate = input.StopLossRate
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = ctu.Postgres.Db.Create(&copyTrade).Error
			if err != nil {
				return nil, http.StatusInternalServerError, err
			} else {
				return &copyTrade, http.StatusCreated, nil
			}
		}
		return nil, http.StatusInternalServerError, err
	}

	ctu.Postgres.Db.Save(&copyTrade)

	return &copyTrade, http.StatusOK, nil
}

func (ctu *CopyTradeUseCase) CopyTradeFuture(ctx context.Context, userId uint, input entity.CopyTradeFutureInput) (Status, error) {
	binanceKey, err := ctu.binanceKeyRepo.GetApiKeyByUserId(ctx, userId)
	if err != nil {
		return http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}
	futureClient := futures.NewClient(binanceKey.ApiKey, binanceKey.SecretKey)
	order, err := futureClient.NewCreateOrderService().Symbol(input.Symbol).
		Side(input.Type).Type(futures.OrderTypeLimit).
		TimeInForce(futures.TimeInForceTypeGTC).Quantity(fmt.Sprintf("%.3f", input.Quantity)).
		Price(fmt.Sprintf("%.1f", input.Price)).Do(ctx)
	if err != nil {
		fmt.Println("CreateBinanceOrders error: ", err.Error())
		return http.StatusInternalServerError, err
	}
	err = ctu.orderLogRepo.CreateLog(ctx, &entity.CreateOrderLogInput{
		Message: "",
		Symbol:  input.Symbol,
		Base:    string(input.Type),
		OrderID: fmt.Sprintf("%d", order.OrderID),
	})
	return http.StatusOK, nil
}

func (ctu *CopyTradeUseCase) GetFutureOrder(ctx context.Context, userId uint, input entity.GetSpotInfoInput) (*futures.Order, Status, error) {
	binanceKey, err := ctu.binanceKeyRepo.GetApiKeyByUserId(ctx, userId)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}
	futureClient := futures.NewClient(binanceKey.ApiKey, binanceKey.SecretKey)
	order, err := futureClient.NewGetOrderService().Symbol(input.Symbol).OrderID(input.OrderID).Do(ctx)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	return order, http.StatusOK, nil
}

func (ctu *CopyTradeUseCase) CopyTradeSpot(ctx context.Context, userId uint, input entity.CopyTradeSpotInput) (*binance.CreateOrderResponse, Status, error) {
	binanceKey, err := ctu.binanceKeyRepo.GetApiKeyByUserId(ctx, userId)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}
	spotClient := binance.NewClient(binanceKey.ApiKey, binanceKey.SecretKey)
	order, err := spotClient.NewCreateOrderService().Symbol(input.Symbol).
		Side(input.Type).Type(binance.OrderTypeLimit).
		TimeInForce(binance.TimeInForceTypeGTC).Quantity(fmt.Sprintf("%.3f", input.Quantity)).
		Price(fmt.Sprintf("%.1f", input.Price)).Do(ctx)
	fmt.Println(order)
	if err != nil {
		fmt.Println("CreateBinanceOrders error: ", err.Error())
		return nil, http.StatusInternalServerError, err
	}
	return order, http.StatusOK, nil
}

func (ctu *CopyTradeUseCase) GetSpotOrder(ctx context.Context, userId uint, input entity.GetSpotInfoInput) (*binance.Order, Status, error) {
	binanceKey, err := ctu.binanceKeyRepo.GetApiKeyByUserId(ctx, userId)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}
	spotClient := binance.NewClient(binanceKey.ApiKey, binanceKey.SecretKey)
	order, err := spotClient.NewGetOrderService().Symbol(input.Symbol).OrderID(input.OrderID).Do(ctx)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	fmt.Println(order)
	return order, http.StatusOK, nil
}

func (ctu *CopyTradeUseCase) GetFollowings(ctx context.Context, userId uint) ([]entity.UserFollow, Status, error) {
	var master entity.User
	err := ctu.Postgres.Db.Model(&entity.User{}).Where("id = ?", userId).First(&master).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, http.StatusNotFound, errors.New(fmt.Sprintf("user with id %d not found", userId))
	} else if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	if master.Role != enums.Master {
		return nil, http.StatusForbidden, errors.New(fmt.Sprintf("user with id %d is not a master", userId))
	}

	followers := make([]entity.UserFollow, 0)

	ctu.Postgres.Db.Model(&entity.UserFollow{}).WithContext(ctx).
		Select(`distinct users.id, users.role, users.email, users.phone`).
		Joins("LEFT JOIN users ON users.id = user_follows.user_id").
		Where("users.role = ?", "USER").
		Where("user_follows.following_id = ?", userId).
		Find(&followers)

	return followers, http.StatusOK, nil
}

func (ctu *CopyTradeUseCase) CreateLog(ctx context.Context, input entity.CopyTradeSpotInput, order *binance.CreateOrderResponse, userId uint) error {
	result := ctu.Db.WithContext(ctx).Create(&entity.OrderLog{
		OrderID:       fmt.Sprintf("%d", order.OrderID),
		Message:       "",
		Symbol:        input.Symbol,
		Base:          "",
		OrderType:     string(input.Type),
		UserPackageID: 0,
		Quantity:      input.Quantity,
		Price:         input.Price,
		Rate:          input.Rate,
		UserID:        userId,
	})
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (ctu *CopyTradeUseCase) CreateLogTest(ctx context.Context, input entity.CopyTradeSpotInput, userId uint) error {
	result := ctu.Db.WithContext(ctx).Create(&entity.OrderLog{
		Symbol:    input.Symbol,
		OrderType: string(input.Type),
		Quantity:  input.Quantity,
		Price:     input.Price,
		Rate:      input.Rate,
		UserID:    userId,
	})
	if result.Error != nil {
		return result.Error
	}

	return nil
}
