package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"errors"
	"net/http"
)

// WalletUseCase -.
type RefUseCase struct {
	referalRepo IReferralRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewRefUseCase -.
func NewRefUseCase(rr IReferralRepo, p *postgres.Postgres, l *logger.Logger) *RefUseCase {
	return &RefUseCase{
		referalRepo: rr,
		Postgres:    p,
		logger:      l,
	}
}

func (ru *RefUseCase) GetRefByFilter(ctx context.Context, referral entity.Referral) ([]*entity.Referral, Status, error) {

	refList, err := ru.referalRepo.GetRef(ctx, referral)

	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	return refList, http.StatusOK, nil
}

func (ru *RefUseCase) CreateRef(ctx context.Context, publisherId uint, referrerId uint) (Status, error) {
	ref := &entity.Referral{
		PublisherID: publisherId,
		UserID:      referrerId,
	}
	err := ru.referalRepo.Create(ctx, ref)
	if err != nil {
		return http.StatusForbidden, err
	}
	return http.StatusOK, err
}
