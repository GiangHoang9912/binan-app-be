package usecase

import (
	"binan/internal/constants"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"net/http"
)

// OrderUseCase -.
type OrderUseCase struct {
	*postgres.Postgres
	logger *logger.Logger
}

func (ou *OrderUseCase) CreateOrder(ctx context.Context, userId uint, input entity.CreateOrderInput) (*entity.Order, Status, error) {
	var pkg entity.Package
	err := ou.Postgres.GetById(ctx, input.PackageID, &pkg)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, http.StatusNotFound, fmt.Errorf("package not found")
		}
		return nil, http.StatusInternalServerError, err
	}

	order := &entity.Order{
		UserID:        userId,
		PackageID:     input.PackageID,
		Price:         input.Price,
		PriceCurrency: input.PriceCurrency,
		TotalMonths:   input.TotalMonths,
		Type:          input.Type,
		Status:        enums.Pending,
	}
	err = ou.Postgres.Create(ctx, order)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New(constants.InternalServerErrorMessage)
	}

	return order, http.StatusCreated, nil
}

// NewOrderUseCase -.
func NewOrderUseCase(p *postgres.Postgres) *OrderUseCase {
	return &OrderUseCase{
		Postgres: p,
	}
}
