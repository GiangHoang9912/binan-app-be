package usecase

import (
	"binan/internal/constants"
	"binan/pkg/mail"
	"binan/pkg/redis"
	"binan/utils"
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

// MailUseCase -.
type MailUseCase struct {
	userRepo IUserRepo
	*redis.Redis
}

func (uc *MailUseCase) SendEmail(ctx context.Context, input mail.SendMailInput) (Status, error) {
	user, err := uc.userRepo.GetByEmail(ctx, input.Email)
	code := utils.RandomBetween(100000, 999999)
	redisKey, subject, durr := "", "", time.Minute*3

	switch input.Type {
	case mail.Register:
		if user != nil {
			return http.StatusBadRequest, fmt.Errorf(constants.DuplicatedEmailErrorMessage)
		}
		redisKey, subject = utils.GetRedisKey(mail.RreRegConfirmationKey, input.Email), "Registering"
	case mail.Activate2fa:
		if user == nil {
			return http.StatusNotFound, err
		} else if user.AuthenticatorEnabled {
			return http.StatusBadRequest, errors.New("2fa da duoc kich hoat cho user nay")
		}
		redisKey, subject = utils.GetRedisKey(mail.Activate2faVerificationCodeKey, input.Email), "Activating 2fa"
	case mail.ForgetPassword:
		if user == nil {
			return http.StatusNotFound, err
		}
		redisKey, subject = utils.GetRedisKey(mail.ForgetPasswordKey, input.Email), "Forget password"
	default:
		return http.StatusBadRequest, errors.New("invalid type")
	}

	existedCode, err := uc.Redis.Get(ctx, redisKey)

	if existedCode != "" {
		return http.StatusBadRequest, errors.New("chỉ có thể gửi 1 email trong vong 3 phut")
	}

	go func() {
		err := mail.Send([]string{input.Email}, subject, mail.EmailCode{Code: strconv.Itoa(code)})
		if err == nil {
			err = uc.Redis.Set(context.Background(), redisKey, code, durr)
		}
	}()

	return http.StatusOK, nil
}

// NewMailUseCase -.
func NewMailUseCase(u IUserRepo, rdb *redis.Redis) *MailUseCase {
	return &MailUseCase{
		userRepo: u,
		Redis:    rdb,
	}
}
