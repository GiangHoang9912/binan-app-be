package usecase

import (
	"binan/internal/entity"
	"binan/pkg/logger"
	"binan/pkg/postgres"
	"context"
	"net/http"
)

// WalletUseCase -.
type TipTypeUseCase struct {
	tipTypeRepo ITipTypeRepo
	*postgres.Postgres
	logger *logger.Logger
}

// NewTipTypeUseCase -.
func NewTipTypeUseCase(ttr ITipTypeRepo, p *postgres.Postgres, l *logger.Logger) *TipTypeUseCase {
	return &TipTypeUseCase{
		tipTypeRepo: ttr,
		Postgres:    p,
		logger:      l,
	}
}

func (lu *TipTypeUseCase) GetAllTipType(ctx context.Context) ([]*entity.TipType, error) {
	return lu.tipTypeRepo.GetAll(ctx)
}

func (lu *TipTypeUseCase) GetById(ctx context.Context, tipTypeId uint) (*entity.TipType, error) {
	return lu.tipTypeRepo.GetByID(ctx, tipTypeId)
}

func (lu *TipTypeUseCase) Create(ctx context.Context, tipType *entity.TipType) (Status, error) {
	err := lu.tipTypeRepo.Create(ctx, tipType)
	if err != nil {
		return http.StatusForbidden, err
	}
	return http.StatusOK, err
}
