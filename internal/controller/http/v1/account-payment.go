package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"net/http"
)

type AccountPaymentRoutes struct {
	AP usecase.IAccountPayment
	L  logger.Interface
}

func newAccountPaymentRoutes(handler *gin.RouterGroup, ap usecase.IAccountPayment, l logger.Interface) {
	r := &AccountPaymentRoutes{ap, l}

	h := handler.Group("/payment")
	//h.Use(middlewares.JwtMiddleware)
	h.GET("", r.GetAccountPayment)
}

// GetAccountPayment
// @Summary     get-account-payment
// @Description
// @ID          get-account-payment
// @Tags  	    payment
// @Produce json
// @Router      /payment [get]
func (ac *AccountPaymentRoutes) GetAccountPayment(c *gin.Context) {
	err, account := ac.AP.GetPaymentInformation(c)
	if err != nil {
		ac.L.Error(err, "http - v1 - GetAccountPayment")
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    account,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}
