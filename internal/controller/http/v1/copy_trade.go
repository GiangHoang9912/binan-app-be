package v1

import (
	"binan/internal/constants"
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/entity"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"io/ioutil"
	"net/http"
)

type CopyTradeRoutes struct {
	Ct usecase.ICopyTrade
	L  logger.Interface
	V  *validator.Validate
}

func newCopyTradeRoutes(handler *gin.RouterGroup, ct usecase.ICopyTrade, l logger.Interface, v *validator.Validate) {
	r := &CopyTradeRoutes{ct, l, v}

	h := handler.Group("/copy-trades")
	h.Use(middlewares.JwtMiddleware)
	h.POST("/", r.SetCopyTrade)
	h.GET("/symbol", r.GetSymbol)
	h.POST("/future", r.CopyTradeFuture)
	h.POST("/spot", r.CopyTradeSpot)
	h.POST("/info/spot", r.GetSpotOrder)
	h.POST("/info/future", r.GetFutureOrder)
}

func (ctr *CopyTradeRoutes) GetSymbol(c *gin.Context) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodGet, "https://api3.binance.com/api/v3/ticker/price", nil)
	if err != nil {
		common.ResponseWithError(c, http.StatusInternalServerError, "")
	}
	req.Header.Add("X-CMC_PRO_API_KEY", "a6e404fd-8111-4cce-8ac1-8e44f37e8302")
	req.Header.Add("CMC_PRO_API_KEY", "a6e404fd-8111-4cce-8ac1-8e44f37e8302")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("No response from request")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body) // response body is []byte

	var result []SymbolRes
	if err := json.Unmarshal(body, &result); err != nil { // Parse []byte to the go struct pointer
		fmt.Println("Can not unmarshal JSON")
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    result,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}

type SymbolRes struct {
	Symbol string `json:"symbol"`
	Price  string `json:"price"`
}

// SetCopyTrade
// @Summary     set copy trade
// @Description
// @ID          set-copy-trade
// @Tags  	    copy-trade
// @Produce json
// @Success     200 {object} entity.Order
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /orders [get]
func (ctr *CopyTradeRoutes) SetCopyTrade(c *gin.Context) {
	var body entity.CopyTradeSettingInput
	if err := c.ShouldBindJSON(&body); err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ctr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")
	order, status, err := ctr.Ct.SetCopyTrade(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}

func (ctr *CopyTradeRoutes) CopyTradeFuture(c *gin.Context) {
	var body entity.CopyTradeFutureInput
	if err := c.ShouldBindJSON(&body); err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ctr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")
	status, err := ctr.Ct.CopyTradeFuture(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
	}
	common.ResponseWithData(c, status, res)
}

func (ctr *CopyTradeRoutes) GetFutureOrder(c *gin.Context) {
	var body entity.GetSpotInfoInput
	if err := c.ShouldBindJSON(&body); err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ctr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")
	order, status, err := ctr.Ct.GetFutureOrder(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}

func (ctr *CopyTradeRoutes) CopyTradeSpot(c *gin.Context) {
	var body entity.CopyTradeSpotInput
	if err := c.ShouldBindJSON(&body); err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ctr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")
	// get follower
	followings, _, err := ctr.Ct.GetFollowings(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		return
	}

	order, status, err := ctr.Ct.CopyTradeSpot(c.Request.Context(), uint(userId.(int)), body)

	err = ctr.Ct.CreateLog(c.Request.Context(), body, order, uint(userId.(int)))
	//err = ctr.Ct.CreateLogTest(c.Request.Context(), body, uint(userId.(int)))
	if err != nil {
		return
	}

	if err != nil {
		ctr.L.Error(err, "http - v1 - GetAllBot")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	for i := 0; i < len(followings); i++ {
		body.Quantity = body.Quantity / 100 * body.Rate
		fmt.Print(followings[i].ID)
		order, _, err := ctr.Ct.CopyTradeSpot(c.Request.Context(), followings[i].ID, body)
		err = ctr.Ct.CreateLog(c.Request.Context(), body, order, followings[i].ID)
		//err = ctr.Ct.CreateLogTest(c.Request.Context(), body, followings[i].ID)
		if err != nil {
			continue
		}
	}

	res := common.Response{
		Success: true,
		Message: "",
	}
	common.ResponseWithData(c, status, res)
	//common.ResponseWithData(c, 200, res)
}

func (ctr *CopyTradeRoutes) GetSpotOrder(c *gin.Context) {
	var body entity.GetSpotInfoInput
	if err := c.ShouldBindJSON(&body); err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ctr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")
	order, status, err := ctr.Ct.GetSpotOrder(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}
