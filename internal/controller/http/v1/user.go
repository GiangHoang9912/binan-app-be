package v1

import (
	"binan/config"
	"binan/internal/constants"
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/utils"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/devfeel/mapper"
	"github.com/go-playground/validator/v10"

	"github.com/gin-gonic/gin"

	"binan/internal/entity"
	"binan/internal/usecase"
	"binan/pkg/logger"
)

type UserRoutes struct {
	U usecase.IUser
	L logger.Interface
	V *validator.Validate
}

func newUserRoutes(handler *gin.RouterGroup, u usecase.IUser, l logger.Interface, v *validator.Validate) {
	r := &UserRoutes{u, l, v}

	h := handler.Group("/user")
	h.POST("/auth/register", r.Register)
	h.POST("/auth/register/:ref_code", r.RegisterWithRefCode)
	h.POST("/auth/login", r.Login)
	h.POST("/auth/forget-password", r.ForgetPassword)
	h.POST("/auth/access-token", r.CreateAccessToken)
	h.GET("/:id", r.GetUserByID)
	h.GET("/masters", r.GetUserMaster)
	h.GET("/:id/followers", r.GetFollowersHandlerFunc)
	h.GET("/:id/followings", r.GetFollowingsHandlerFunc)
	h.Use(middlewares.JwtMiddleware).POST("/auth/change-password", r.ChangePassword)
	h.Use(middlewares.JwtMiddleware).POST("/auth/2fa/generate-otp", r.GenerateOTP)
	h.Use(middlewares.JwtMiddleware).POST("/auth/2fa/activate-2fa", r.Activate2fa)
	h.Use(middlewares.JwtMiddleware).POST("/auth/2fa/verify-otp", r.VerifyOTP)
	h.Use(middlewares.JwtMiddleware).POST("/auth/2fa/disable-otp", r.DisableOTP)
	h.Use(middlewares.JwtMiddleware).POST("/auth/2fa/backup-key", r.SetOtpBackupKey)
	h.Use(middlewares.JwtMiddleware).POST("/follow-master", r.FollowMaster)
	h.Use(middlewares.JwtMiddleware).POST("/un-follow", r.UnFollow)
}

// Register
// @Summary     Register
// @Description Register with email and password
// @ID          do-Register
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.RegReqBody true "Params structure"
// @Success     200 {object} entity.AuthUserRes
// @Failure     400 {object} common.Response
// @Router      /user/auth/register [post]
func (ur *UserRoutes) Register(c *gin.Context) {
	var body entity.RegReqBody
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - PreRegister")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	refCode := c.Param("ref_code")
	authUser, status, err := ur.U.Register(c.Request.Context(), body, refCode)
	if err != nil {
		ur.L.Error(err, "http - v1 - PreRegister")
		errMsg := err.Error()
		common.ResponseWithError(c, status, errMsg)
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    authUser,
	}
	common.ResponseWithData(c, status, res)
}

// RegisterWithRefCode
// @Summary     Register with ref-code
// @Description Register with email and password
// @ID          do-RegisterWithRefCode
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.RegReqBody true "Params structure"
// @Param       ref-code path string true "referral code"
// @Success     200 {object} entity.AuthUserRes
// @Failure     400 {object} common.Response
// @Router      /user/auth/register/{ref-code} [post]
func (ur *UserRoutes) RegisterWithRefCode(c *gin.Context) {
	ur.Register(c)
}

// Login
// @Summary     Login
// @Description Login with email and password
// @ID          do-login
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.LoginUserReqBody true "Set up translation"
// @Success     200 {object} entity.AuthUserRes
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/login [post]
func (ur *UserRoutes) Login(c *gin.Context) {
	var body entity.LoginUserReqBody
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - Login")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid request body")
		return
	}

	authUser, status, err := ur.U.Login(c.Request.Context(), body)
	if err != nil {
		ur.L.Error(err, "http - v1 - Login")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *authUser,
	}

	cfg, _ := config.Get()
	allowOrigins := strings.Split(cfg.AllowedOrigins, ",")
	for _, v := range allowOrigins {
		http.SetCookie(c.Writer, &http.Cookie{
			Name:     "accessToken",
			Value:    authUser.AccessToken,
			Expires:  time.Now().Add(time.Hour * time.Duration(utils.AccessTokenExpirationHours)),
			Path:     "/",
			Domain:   v,
			Secure:   cfg.IsProd(),
			HttpOnly: cfg.IsProd(),
		})
		http.SetCookie(c.Writer, &http.Cookie{
			Name:     "user_id",
			Value:    strconv.FormatUint(uint64(authUser.ID), 10),
			Expires:  time.Now().Add(time.Hour * time.Duration(utils.AccessTokenExpirationHours)),
			Path:     "/",
			Domain:   v,
			Secure:   cfg.IsProd(),
			HttpOnly: cfg.IsProd(),
		})
		http.SetCookie(c.Writer, &http.Cookie{
			Name:     "refreshToken",
			Value:    authUser.RefreshToken,
			Expires:  time.Now().Add(time.Hour * time.Duration(utils.RefreshTokenExpirationHours)),
			Path:     "/",
			Domain:   v,
			Secure:   cfg.IsProd(),
			HttpOnly: cfg.IsProd(),
		})
	}

	common.ResponseWithData(c, status, res)
}

// ChangePassword
// @Summary     change password
// @Description change password
// @ID          change-password
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.ChangePwInput true "Change pw input"
// @Success     200 {object} entity.AuthUserRes
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/change-password [post]
func (ur *UserRoutes) ChangePassword(c *gin.Context) {
	var body entity.ChangePwInput
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - ChangePassword")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid request body")
		return
	}

	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	if body.NewPassword != body.ConfirmPassword {
		common.ResponseWithError(c, http.StatusBadRequest, "new pw and confirm pw does not match")
		return
	}

	userId, _ := c.Get("user_id")

	status, err := ur.U.ChangePassword(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		ur.L.Error(err, "http - v1 - ChangePassword")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "Ok",
		Data:    nil,
	}

	common.ResponseWithData(c, status, res)
}

// ForgetPassword
// @Summary     forget password
// @Description forget password
// @ID          forget-password
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.ForgetPwInput true "Set up translation"
// @Success     200 {object} entity.AuthUserRes
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/forget-password [post]
func (ur *UserRoutes) ForgetPassword(c *gin.Context) {
	var body entity.ForgetPwInput
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - ForgetPassword")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid request body")
		return
	}

	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	if body.NewPassword != body.ConfirmPassword {
		common.ResponseWithError(c, http.StatusBadRequest, "new pw and confirm pw does not match")
		return
	}

	status, err := ur.U.ForgetPassword(c.Request.Context(), body)
	if err != nil {
		ur.L.Error(err, "http - v1 - ForgetPassword")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "Ok",
		Data:    nil,
	}

	common.ResponseWithData(c, status, res)
}

// CreateAccessToken
// @Summary     create access token
// @Description
// @ID          create-access-token
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.CreateAccessTokenReqBody true "Set up translation"
// @Success     200 {object} entity.AuthUserRes
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/access-token [post]
func (ur *UserRoutes) CreateAccessToken(c *gin.Context) {
	var body entity.CreateAccessTokenReqBody
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - CreateAccessToken")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid request body")
		return
	}

	authUser, status, err := ur.U.CreateAccessToken(c.Request.Context(), body.RefreshToken)
	if err != nil {
		ur.L.Error(err, "http - v1 - CreateAccessToken")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *authUser,
	}
	common.ResponseWithData(c, status, res)
}

// GetUserByID
// @Summary     get a user by id
// @Description
// @ID          get-user-by-id
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       id path int true "user id"
// @Success     200 {object} entity.UserInfo
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/{id} [get]
func (ur *UserRoutes) GetUserByID(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		ur.L.Error(err, "http - v1 - GetUserByID")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid user id")
		return
	}
	user, status, err := ur.U.GetByID(c.Request.Context(), uint(id))
	if err != nil {
		ur.L.Error(err, "http - v1 - GetUserByID")
		common.ResponseWithError(c, status, err.Error())
		return
	}
	userInfo := &entity.UserInfo{}
	err = mapper.Mapper(user, userInfo)
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}
	res := common.Response{
		Success: true,
		Message: "",
		Data:    *userInfo,
	}
	common.ResponseWithData(c, status, res)
}

// GenerateOTP
// @Summary     generate a secret key and an otp-auth-url for authenticator
// @Description
// @ID          generate-otp
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Success     200 {object} entity.GenerateOtpResponse
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/2fa/generate-otp [post]
func (ur *UserRoutes) GenerateOTP(c *gin.Context) {
	userId, _ := c.Get("user_id")

	otpOutput, status, err := ur.U.GenerateOTP(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *otpOutput,
	}
	common.ResponseWithData(c, status, res)
}

// Activate2fa
// @Summary     activate 2fa
// @Description
// @ID          activate-2fa
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.Activate2faIn true "Params structure"
// @Success     200 {object} entity.Activate2faOut
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/2fa/activate-2fa [post]
func (ur *UserRoutes) Activate2fa(c *gin.Context) {
	userId, _ := c.Get("user_id")
	var body entity.Activate2faIn
	if err := c.ShouldBindJSON(&body); err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}
	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	otpOutput, status, err := ur.U.Activate2fa(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *otpOutput,
	}
	common.ResponseWithData(c, status, res)
}

// VerifyOTP
// @Summary     verify otp
// @Description
// @ID          verify-otp
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.VerifyOtpInput true "Params structure"
// @Success     200 {object} entity.GenerateOtpResponse
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/2fa/verify-otp [post]
func (ur *UserRoutes) VerifyOTP(c *gin.Context) {
	userId, _ := c.Get("user_id")

	var body entity.VerifyOtpInput
	if err := c.ShouldBindJSON(&body); err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	otpOutput, status, err := ur.U.VerifyOTP(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *otpOutput,
	}
	common.ResponseWithData(c, status, res)
}

// DisableOTP
// @Summary     disable otp
// @Description
// @ID          disable-otp
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.VerifyOtpInput true "Params structure"
// @Success     200 {object} entity.DisableOtpOutput
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/2fa/disable-otp [post]
func (ur *UserRoutes) DisableOTP(c *gin.Context) {
	userId, _ := c.Get("user_id")

	otpOutput, status, err := ur.U.DisableOTP(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *otpOutput,
	}
	common.ResponseWithData(c, status, res)
}

// SetOtpBackupKey
// @Summary     set otp 2fa backup key
// @Description
// @ID          set-otp-backhup-key
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Param       request body entity.SetOtpBackupKeyInput true "Params structure"
// @Success     200 {object} entity.SetOtpBackupKeyOutput
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/auth/2fa/backup-key [post]
func (ur *UserRoutes) SetOtpBackupKey(c *gin.Context) {
	userId, _ := c.Get("user_id")

	var body entity.SetOtpBackupKeyInput
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - SetOtpBackupKey")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	if body.Code != body.ConfirmationCode {
		common.ResponseWithError(c, http.StatusBadRequest, "incorrect confirmation code")
		return
	}

	otpOutput, status, err := ur.U.SetOtpBackupKey(c.Request.Context(), uint(userId.(int)), &body)
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *otpOutput,
	}
	common.ResponseWithData(c, status, res)
}

// FollowMaster
// @Summary     follow master user
// @Description
// @ID          follow-master-user
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Success     200 {object} entity.Response
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/follow-master [post]
func (ur *UserRoutes) FollowMaster(c *gin.Context) {
	userId, _ := c.Get("user_id")

	var body entity.FollowUserInput
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - FollowMaster")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	balance := body.Balance
	// get user wallet
	wallet, _, err := ur.U.GetUserWallet(c.Request.Context(), uint(userId.(int)), "FUTURE")
	if err != nil {
		return
	}
	fmt.Print(wallet.Balance)
	if wallet.Balance < balance {
		return
	}
	walletBinan, _, err := ur.U.GetUserWallet(c.Request.Context(), uint(userId.(int)), "BINAN_TRADE")
	if err != nil {
		return
	}
	newBianaBalance := walletBinan.Balance + balance
	// update balance - 10% balance
	binanBalance, _, err := ur.U.SetNewBalance(c.Request.Context(), uint(userId.(int)), newBianaBalance, "BINAN_TRADE")
	fmt.Print(binanBalance)
	if err != nil {
		return
	}
	// update future balance - tiền ủy quyền - 10%
	newFutureBalance := wallet.Balance - balance - (balance / 10)
	newBalance, _, err := ur.U.SetNewBalance(c.Request.Context(), uint(userId.(int)), newFutureBalance, "FUTURE")
	if err != nil {
		return
	}
	fmt.Print(newBalance)
	follow, status, err := ur.U.FollowMaster(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    *follow,
	}
	common.ResponseWithData(c, status, res)
}

// UnFollow
// @Summary     un-follow user
// @Description
// @ID          un-follow
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Success     200 {object} entity.Response
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/un-follow [post]
func (ur *UserRoutes) UnFollow(c *gin.Context) {
	userId, _ := c.Get("user_id")

	var body entity.FollowUserInput
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - FollowMaster")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	_, status, err := ur.U.UnFollow(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		ur.L.Error(err)
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
	}
	common.ResponseWithData(c, status, res)
}

// GetUserMaster
// @Summary     Get User Master
// @Description
// @ID          Get-User-Master
// @Tags  	    user
// @Accept      json
// @Produce     json
// @Success     200 {object} entity.UserInfo
// @Failure     400 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user/{id} [get]
func (ur *UserRoutes) GetUserMaster(c *gin.Context) {
	userMaster, status, err := ur.U.GetMasters(c.Request.Context())
	if err != nil {
		common.ResponseWithError(c, http.StatusInternalServerError, err.Error())
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    userMaster,
	}
	common.ResponseWithData(c, status, res)
}

func (ur *UserRoutes) GetFollowersHandlerFunc(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		ur.L.Error(err, "http - v1 - GetUserByID")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid user id")
		return
	}
	followers, status, err := ur.U.GetFollowers(c.Request.Context(), uint(id))
	if err != nil {
		common.ResponseWithError(c, http.StatusInternalServerError, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    followers,
	}
	common.ResponseWithData(c, status, res)
}

func (ur *UserRoutes) GetFollowingsHandlerFunc(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		ur.L.Error(err, "http - v1 - GetUserByID")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid user id")
		return
	}
	followers, status, err := ur.U.GetFollowings(c.Request.Context(), uint(id))
	if err != nil {
		common.ResponseWithError(c, http.StatusInternalServerError, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    followers,
	}
	common.ResponseWithData(c, status, res)
}
