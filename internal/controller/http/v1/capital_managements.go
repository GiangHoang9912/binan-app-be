package v1

import (
	"binan/internal/constants"
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/entity"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
)

type CapitalManagementsRoutes struct {
	CM usecase.ICapitalManagements
	L  logger.Interface
	V  *validator.Validate
}

func newCapitalManagementsRoutes(handler *gin.RouterGroup, CM usecase.ICapitalManagements, l logger.Interface, v *validator.Validate) {
	r := &CapitalManagementsRoutes{CM, l, v}

	h := handler.Group("/capital-managements")
	h.Use(middlewares.JwtMiddleware)
	h.POST("/", r.CreateCapitalManagements)
}

// CreateCapitalManagements
// @Summary     create-capital-manage
// @Description
// @ID          create-capital-manage
// @Tags  	    capital-manage
// @Produce json
// @Success     200 {object} entity.CapitalManagements
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /capital-managements [post]
func (cmr *CapitalManagementsRoutes) CreateCapitalManagements(c *gin.Context) {
	var body entity.CreateCapitalManagementsInput
	err1 := c.ShouldBindJSON(&body)
	cmr.L.Info("&body", &body)
	if err1 != nil {
		cmr.L.Error(err1, "http - v1 - CreateCapitalManagements")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := cmr.V.Struct(body)
	cmr.L.Info("err", err)

	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")

	CapitalManagements, status, err := cmr.CM.CreateCapitalManagementsPackage(c.Request.Context(), body, uint(userId.(int)))
	if err != nil {
		cmr.L.Error(err, "http - v1 - CreateCapitalManagements")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    CapitalManagements,
	}
	common.ResponseWithData(c, status, res)
}
