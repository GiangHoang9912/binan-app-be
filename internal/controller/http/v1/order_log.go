package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
	"strconv"
)

type OrderLogRoutes struct {
	O usecase.IOrderLog
	L logger.Interface
	V *validator.Validate
}

func newOrderLogRoutes(handler *gin.RouterGroup, o usecase.IOrderLog, l logger.Interface, v *validator.Validate) {
	r := &OrderLogRoutes{o, l, v}

	h := handler.Group("/order-logs")
	h.Use(middlewares.JwtMiddleware)
	h.GET("/", r.GetAll)
	h.GET("/spot/:id", r.GetAllSpot)
	h.GET("/:id", r.GetOrderByUserId)
}

// GetAll
// @Summary     get order logs
// @Description
// @ID          get-order-logs
// @Tags  	    order-logs
// @Produce json
// @Success     200 {object} entity.OrderLog
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /order-logs [get]
func (or *OrderLogRoutes) GetAll(c *gin.Context) {
	userId, _ := c.Get("user_id")
	order, status, err := or.O.GetAll(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}

// GetOrderByUserId
// @Summary     Get-Order-By-User-Id
// @Description
// @ID          Get-Order-By-User-Id
// @Tags  	    Get-Order-By-User-Id
// @Produce json
// @Success     200 {object} entity.OrderLog
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /order-logs [get]
func (or *OrderLogRoutes) GetOrderByUserId(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		or.L.Error(err, "http - v1 - GetUserByID")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid user id")
		return
	}

	orders, status, err := or.O.GetAll(c.Request.Context(), uint(id))
	if err != nil {
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    orders,
	}
	common.ResponseWithData(c, status, res)
}

func (or *OrderLogRoutes) GetAllSpot(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		or.L.Error(err, "http - v1 - GetUserByID")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid user id")
		return
	}
	order, status, err := or.O.GetAllSpot(c.Request.Context(), id)
	if err != nil {
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}
