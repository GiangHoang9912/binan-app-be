package v1

import (
	"binan/internal/constants"
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/entity"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
)

type OrderRoutes struct {
	O usecase.IOrder
	L logger.Interface
	V *validator.Validate
}

func newOrderRoutes(handler *gin.RouterGroup, o usecase.IOrder, l logger.Interface, v *validator.Validate) {
	r := &OrderRoutes{o, l, v}

	h := handler.Group("/orders")
	h.Use(middlewares.JwtMiddleware)
	h.POST("/", r.CreateOrder)
}

// CreateOrder
// @Summary     create-order
// @Description
// @ID          create-order
// @Tags  	    order
// @Produce json
// @Success     200 {object} entity.Order
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /orders [get]
func (or *OrderRoutes) CreateOrder(c *gin.Context) {
	var body entity.CreateOrderInput
	if err := c.ShouldBindJSON(&body); err != nil {
		or.L.Error(err, "http - v1 - CreateOrder")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := or.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")
	order, status, err := or.O.CreateOrder(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		or.L.Error(err, "http - v1 - GetAllBot")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}
