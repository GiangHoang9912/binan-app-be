package v1

import (
	"binan/internal/constants"
	"binan/internal/controller/http/common"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"binan/pkg/mail"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
)

type MailRoutes struct {
	M usecase.IMail
	L logger.Interface
	V *validator.Validate
}

func newMailRoutes(handler *gin.RouterGroup, m usecase.IMail, l logger.Interface, v *validator.Validate) {
	r := &MailRoutes{m, l, v}

	h := handler.Group("/mail")
	h.POST("/send-confirmation-code", r.SendConfirmationCode)
}

// SendConfirmationCode
// @Summary     SendConfirmationCode
// @Description SendConfirmationCode
// @ID          send-mail
// @Tags  	    mail
// @Accept      json
// @Produce     json
// @Param       request body mail.SendMailInput true "Params structure"
// @Success     200 {object} entity.AuthUserRes
// @Failure     400 {object} common.Response
// @Router      /mail/send-confirmation-code [post]
func (ur *MailRoutes) SendConfirmationCode(c *gin.Context) {
	var body mail.SendMailInput
	if err := c.ShouldBindJSON(&body); err != nil {
		ur.L.Error(err, "http - v1 - PreRegister")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := ur.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}
	status, err := ur.M.SendEmail(c.Request.Context(), body)
	if err != nil {
		ur.L.Error(err, "http - v1 - SendEmail")
		errMsg := err.Error()
		common.ResponseWithError(c, status, errMsg)
		return
	}

	res := common.Response{
		Success: true,
		Message: "Email sent",
		Data:    nil,
	}
	common.ResponseWithData(c, status, res)
}
