package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
	"strconv"
)

type BotRoutes struct {
	B usecase.IBot
	L logger.Interface
	V *validator.Validate
}

func newBotRoutes(handler *gin.RouterGroup, b usecase.IBot, l logger.Interface, v *validator.Validate) {
	r := &BotRoutes{b, l, v}

	h := handler.Group("/bots")
	//h.Use(middlewares.JwtMiddleware)
	h.GET("/", r.GetAllBot)
	h.GET("/:id", r.GetBot)
}

// GetAllBot
// @Summary     get all bot
// @Description
// @ID          get-all-bot
// @Tags  	    bot
// @Produce json
// @Success     200 {object} entity.Bot
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /bots [get]
func (br *BotRoutes) GetAllBot(c *gin.Context) {
	err, bots := br.B.GetAllBot(c.Request.Context())
	if err != nil {
		br.L.Error(err, "http - v1 - GetAllBot")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    bots,
	}
	common.ResponseWithData(c, 200, res)
}

// GetBot
// @Summary     get bot by id
// @Description
// @ID          get-bot-by-id
// @Tags  	    bot
// @Produce json
// @Param       id path int true "bot id"
// @Success     200 {object} entity.Bot
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /bots/{id} [get]
func (br *BotRoutes) GetBot(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		br.L.Error(err, "http - v1 - GetBot")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid bot id")
		return
	}
	err, bot := br.B.GetBot(c.Request.Context(), uint(id))
	if err != nil {
		br.L.Error(err, "http - v1 - GetBot")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    bot,
	}
	common.ResponseWithData(c, 200, res)
}
