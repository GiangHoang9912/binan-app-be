package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/usecase"
	binanceClient "binan/internal/usecase/webapi"
	"binan/pkg/logger"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/adshao/go-binance/v2/futures"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/websocket"
)

type SignalRoutes struct {
	S usecase.ISignal
	L logger.Interface
	V *validator.Validate
}

func newSignalRoutes(handler *gin.RouterGroup, s usecase.ISignal, l logger.Interface, v *validator.Validate) {
	r := &SignalRoutes{s, l, v}

	h := handler.Group("/signal")
	h.GET("/ws", r.HandleWs)
	h.Use(middlewares.JwtMiddleware).GET("/:bot_id", r.GetSignals)
}

// GetSignals
// @Summary     get signal by bot id
// @Description
// @ID          get-signal-by-bot-id
// @Tags  	    signal
// @Produce json
// @Success     200 {object} entity.Signal
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /signal/{bot_id} [get]
func (sr *SignalRoutes) GetSignals(c *gin.Context) {
	idParam := c.Param("bot_id")
	botId, err := strconv.Atoi(idParam)
	if err != nil {
		sr.L.Error(err, "http - v1 - GetSignals")
		common.ResponseWithError(c, http.StatusBadRequest, "invalid bot id")
		return
	}

	userId, _ := c.Get("user_id")
	err, bots := sr.S.GetSignals(c.Request.Context(), uint(botId), uint(userId.(int)))
	if err != nil {
		sr.L.Error(err, "http - v1 - GetAllBot")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    bots,
	}
	common.ResponseWithData(c, 200, res)
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// HandleWs
// @Summary     websocket handler
// @Description
// @ID          websocket-handler
// @Tags  	    signal
// @Produce json
// @Success     200 {object} entity.Signal
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /signal/ws [get]
func (sr *SignalRoutes) HandleWs(c *gin.Context) {
	//upgrade get request to websocket protocol
	ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	consumer := make(chan futures.WsAccountUpdate)
	closeC := make(chan struct{})
	fmt.Println("ws connected")
	defer func(ws *websocket.Conn) {
		err := ws.Close()
		if err != nil {
			sr.L.Error("close ws error: ", err)
		} else {
			fmt.Println("ws closed")
		}
		closeC <- struct{}{}
		close(consumer)
	}(ws)
	go binanceClient.WebSocket(consumer, closeC)
	for c := range consumer {
		b, err := json.Marshal(c)
		err = ws.WriteMessage(1, b)
		if err != nil {
			fmt.Println(err)
			break
		}
	}
	//for {
	//	//Read Message from client
	//	mt, message, err := ws.ReadMessage()
	//	if err != nil {
	//		fmt.Println(err)
	//		break
	//	}
	//	//If client message is ping will return pong
	//	if string(message) == "ping" {
	//		message = []byte("pong")
	//	}
	//	//Response message to client
	//	err = ws.WriteMessage(mt, message)
	//	if err != nil {
	//		fmt.Println(err)
	//		break
	//	}
	//}
}
