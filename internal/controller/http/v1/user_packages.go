package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type UserPackageRoutes struct {
	UP usecase.IUserPackages
	L  logger.Interface
	V  *validator.Validate
}

func newUserPackageRoutes(handler *gin.RouterGroup, UP usecase.IUserPackages, l logger.Interface, v *validator.Validate) {
	r := &UserPackageRoutes{UP, l, v}
	h := handler.Group("/user-package")
	h.Use(middlewares.JwtMiddleware)
	h.GET("/", r.GetUserPackageOfUser)
}

// GetUserPackageOfUser
// @Summary     get user-package
// @Description
// @ID          get user-package
// @Tags  	    user-package
// @Produce json
// @Success     200 {object} entity.UserPackage
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /user-package [get]
func (upr *UserPackageRoutes) GetUserPackageOfUser(c *gin.Context) {
	userId, _ := c.Get("user_id")
	userPackage, status, err := upr.UP.GetUserPackageUser(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		upr.L.Error(err, "http - v1 - GetAllBot")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    userPackage,
	}
	common.ResponseWithData(c, status, res)
}
