package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type TipRoutes struct {
	TU  usecase.ITip
	TMU usecase.ITipMechanism
	TTU usecase.ITipType
	THU usecase.ITipHistory
	RU  usecase.IReferral
	L   logger.Interface
	V   *validator.Validate
}

func newTipRoutes(handler *gin.RouterGroup, tu usecase.ITip, ttu usecase.ITipType, tmu usecase.ITipMechanism, thu usecase.ITipHistory, ru usecase.IReferral, l logger.Interface, v *validator.Validate) {
	r := &TipRoutes{tu, tmu, ttu, thu, ru, l, v}

	h := handler.Group("/tips")
	h.GET("/types", r.GetAllTipType)
	h.GET("/mechanism", r.GetTipMechanism)
	h.Use(middlewares.JwtMiddleware).GET("/history", r.GetTipHistory)
	h.GET("/referrer", r.GetRef)
}

func (tr *TipRoutes) GetAllTipType(c *gin.Context) {
	tiptypes, err := tr.TTU.GetAllTipType(c.Request.Context())
	if err != nil {
		tr.L.Error(err, "http - v1 - GetAllTipType")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    tiptypes,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}
func (tr *TipRoutes) GetTipMechanism(c *gin.Context) {
	paramPairs := c.Request.URL.Query()

	if len(paramPairs) == 0 {
		mechanism, err := tr.TMU.GetAllMechanism(c.Request.Context())
		if err != nil {
			tr.L.Error(err, "http - v1 - GetMechanism")
			common.ResponseWithError(c, 500, err.Error())
			return
		}
		res := common.Response{
			Success: true,
			Message: "",
			Data:    mechanism,
		}
		common.ResponseWithData(c, http.StatusOK, res)
		return
	}

	filter := entity.TipMechanism{}
	c.BindQuery(&filter)

	mechanism, err := tr.TMU.GetMechanismByFilter(c.Request.Context(), &filter)

	if err != nil {
		tr.L.Error(err, "http - v1 - GetMechanism")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    mechanism,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}
func (tr *TipRoutes) GetTipHistory(c *gin.Context) {
	filter := entity.TipHistory{}
	c.BindQuery(&filter)

	role, _ := c.Get("user_role")
	user_id, _ := c.Get("user_id")

	if role != enums.Admin {
		if filter.Referral.PublisherID != uint(user_id.(int)) {
			tr.L.Error("http - v1 - GetMechanism")
			common.ResponseWithError(c, 500, "Bad Request")
			return
		}
	}

	tipHistories, err := tr.THU.GetTipHistoryByUser(c, &filter, filter.Referral.UserID)
	if err != nil {
		tr.L.Error(err, "http - v1 - GetMechanism")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    tipHistories,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}
func (tr *TipRoutes) GetRef(c *gin.Context) {
	filter := entity.Referral{}
	c.BindQuery(&filter)
	referral, _, err := tr.RU.GetRefByFilter(c, filter)
	if err != nil {
		tr.L.Error(err, "http - v1 - GetMechanism")
		common.ResponseWithError(c, 500, err.Error())
		return
	}
	res := common.Response{
		Success: true,
		Message: "",
		Data:    referral,
	}
	common.ResponseWithData(c, http.StatusOK, res)

}
