// Package v1 implements routing paths. Each services in own file.
package v1

import (
	"binan/config"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	// Swagger docs.
	_ "binan/docs"
	"binan/internal/usecase"
	"binan/pkg/logger"
)

// NewRouter -.
// Swagger spec:
// @title       Binan API
// @description binan api
// @version     1.0
// @host        binan.fly.dev
// @BasePath    /v1
func NewRouter(
	handler *gin.Engine,
	l logger.Interface,
	v *validator.Validate,
	mail usecase.IMail,
	user usecase.IUser,
	bot usecase.IBot,
	signal usecase.ISignal,
	order usecase.IOrder,
	wallet usecase.IWallet,
	package_ usecase.IPackage,
	accountPayment usecase.IAccountPayment,
	userPackageUseCase usecase.IUserPackages,
	capitalManagementsUserCase usecase.ICapitalManagements,
	binanceKeysUseCase usecase.IBinanceKeys,
	orderLogUseCase usecase.IOrderLog,
	copyTradeUseCase usecase.ICopyTrade,
	level usecase.ILevel,
	tip usecase.ITip,
	mechanism usecase.ITipMechanism,
	tiptype usecase.ITipType,
	tipHistory usecase.ITipHistory,
	referral usecase.IReferral,
) {
	cfg, _ := config.Get()
	// Options
	handler.Use(gin.Logger())
	handler.Use(gin.Recovery())

	// Swagger
	if !cfg.IsProd() {
		handler.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	// K8s probe
	handler.GET("/healthz", func(c *gin.Context) { c.Status(http.StatusOK) })

	// Prometheus metrics
	handler.GET("/metrics", gin.WrapH(promhttp.Handler()))

	// Routers
	h := handler.Group("/v1")
	newMailRoutes(h, mail, l, v)
	newCmcRoutes(h, l, v)
	newUserRoutes(h, user, l, v)
	newBotRoutes(h, bot, l, v)
	newSignalRoutes(h, signal, l, v)
	newOrderRoutes(h, order, l, v)
	newAccountPaymentRoutes(h, accountPayment, l)
	newWalletRoutes(h, wallet, tip, l, v)
	newPackageRoutes(h, package_, l, v)
	newUserPackageRoutes(h, userPackageUseCase, l, v)
	newCapitalManagementsRoutes(h, capitalManagementsUserCase, l, v)
	newBinanceKeysRoutes(h, binanceKeysUseCase, l, v)
	newOrderLogRoutes(h, orderLogUseCase, l, v)
	newCopyTradeRoutes(h, copyTradeUseCase, l, v)
	newLevelRoutes(h, level, l, v)
	newTipRoutes(h, tip, tiptype, mechanism, tipHistory, referral, l, v)
}
