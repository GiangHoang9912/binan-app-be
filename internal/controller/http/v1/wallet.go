package v1

import (
	"binan/config"
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/entity"
	"binan/internal/enums"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type WalletRoutes struct {
	WU usecase.IWallet
	T  usecase.ITip
	L  logger.Interface
	V  *validator.Validate
}

func newWalletRoutes(handler *gin.RouterGroup, wu usecase.IWallet, tu usecase.ITip, l logger.Interface, v *validator.Validate) {
	r := &WalletRoutes{wu, tu, l, v}

	h := handler.Group("/wallet")
	h.Use(middlewares.JwtMiddleware)
	h.GET("/my-wallets", r.GetUserWallets)
	h.POST("/make-deposit", r.MakeDeposit)
	h.POST("/purchase-package", r.PurchasePackage)
	h.Use(middlewares.ProtectedMiddleware).POST("/process-deposit", r.ProcessDeposit)
	h.Use(middlewares.ProtectedMiddleware).GET("/deposit-logs", r.GetDepositLogs)
	h.GET("/transactions", r.GetTransactions)

	cfg, _ := config.Get()
	if cfg.GinMode == gin.DebugMode {
		handler.Group("/binance").GET("/keys", r.GetBinanceKeys)
	}
}

// GetUserWallets
// @Summary     get user wallet
// @Description
// @ID          get-user-wallet
// @Tags  	    wallet
// @Produce json
// @Success     200 {object} entity.Wallet
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /wallet/my-wallets [get]
func (wr *WalletRoutes) GetUserWallets(c *gin.Context) {
	userId, _ := c.Get("user_id")
	wallet, status, err := wr.WU.GetUserWallets(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		wr.L.Error(err, "http - v1 - GetUserWallets")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    wallet,
	}
	common.ResponseWithData(c, status, res)
}

// MakeDeposit
// @Summary     deposit money into the wallet
// @Description
// @ID          make-deposit
// @Tags  	    wallet
// @Produce json
// @Param       request body entity.MakeDepositInput true "make deposit input"
// @Success     200 {object} entity.DepositLog
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /wallet/make-deposit [post]
func (wr *WalletRoutes) MakeDeposit(c *gin.Context) {
	var body entity.MakeDepositInput
	if err := c.ShouldBindJSON(&body); err != nil {
		wr.L.Error(err, "http - v1 - MakeDeposit")
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}
	err := wr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	order, status, err := wr.WU.MakeDeposit(c.Request.Context(), body)
	if err != nil {
		wr.L.Error(err, "http - v1 - MakeDeposit")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}

// PurchasePackage
// @Summary     purchase package
// @Description
// @ID          purchase-package
// @Tags  	    wallet
// @Produce json
// @Param       request body entity.PurchasePackageInput true "purchase package input"
// @Success     200 {object} entity.UserPackage
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /wallet/purchase-package [post]
func (wr *WalletRoutes) PurchasePackage(c *gin.Context) {
	var body entity.PurchasePackageInput
	if err := c.ShouldBindJSON(&body); err != nil {
		wr.L.Error(err, "http - v1 - PurchasePackage")
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}
	err := wr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")
	order, status, err := wr.WU.PurchasePackage(c.Request.Context(), uint(userId.(int)), body)
	if err != nil {
		wr.L.Error(err, "http - v1 - PurchasePackage")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}

// ProcessDeposit
// @Summary     process deposit
// @Description
// @ID          process-deposit
// @Tags  	    wallet
// @Produce json
// @Param       request body entity.ProcessDepositInput true "process deposit input"
// @Success     200 {object} entity.DepositLog
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /wallet/process-deposit [post]
func (wr *WalletRoutes) ProcessDeposit(c *gin.Context) {
	userId, _ := c.Get("user_id")
	var body entity.ProcessDepositInput
	if err := c.ShouldBindJSON(&body); err != nil {
		wr.L.Error(err, "http - v1 - ProcessDeposit")
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}
	err := wr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}
	fmt.Print(body.Status)

	order, status, err := wr.WU.ProcessDeposit(c.Request.Context(), body)
	if err != nil {
		wr.L.Error(err, "http - v1 - ProcessDeposit")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	if body.Status == enums.DepositStatus("COMPLETED") {
		fmt.Println("done")
		// get balance
		BalancewithType, _, err := wr.WU.GetUserWalletWithType(c.Request.Context(), uint(userId.(int)), "FUTURE")
		if err != nil {
			return
		}
		// Get deposit by id
		currentDeposit, _, err := wr.WU.GetDepositByID(c.Request.Context(), body.DepositID)
		if err != nil {
			return
		}
		fee := currentDeposit.Value * 10 / 100
		// update future balance
		newBalance := BalancewithType.Balance + currentDeposit.Value - fee
		balance, _, err := wr.WU.SetNewBalance(c.Request.Context(), uint(userId.(int)), newBalance, "FUTURE")
		if err != nil {
			return
		}
		fmt.Println(balance)

		wr.T.Commission(c.Request.Context(), uint(userId.(int)), fee, 1)
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    order,
	}
	common.ResponseWithData(c, status, res)
}

// GetDepositLogs
// @Summary     get deposits
// @Description
// @ID          get-deposits
// @Tags  	    wallet
// @Produce json
// @Success     200 {object} entity.DepositLog
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /wallet/deposit-logs [get]
func (wr *WalletRoutes) GetDepositLogs(c *gin.Context) {
	depositLogs, status, err := wr.WU.GetDepositLogs(c.Request.Context())
	if err != nil {
		wr.L.Error(err, "http - v1 - GetDepositLogs")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    depositLogs,
	}
	common.ResponseWithData(c, status, res)
}

// GetTransactions
// @Summary     get transactions
// @Description
// @ID          get transactions
// @Tags  	    wallet
// @Produce json
// @Success     200 {object} entity.Transaction
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /wallet/transactions [get]
func (wr *WalletRoutes) GetTransactions(c *gin.Context) {
	userId, _ := c.Get("user_id")
	depositLogs, status, err := wr.WU.GetTransactions(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		wr.L.Error(err, "http - v1 - GetTransactions")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    depositLogs,
	}
	common.ResponseWithData(c, status, res)
}

func (wr *WalletRoutes) GetBinanceKeys(c *gin.Context) {
	depositLogs, err := wr.WU.GetAvailableBinanceKeys(c.Request.Context())
	if err != nil {
		wr.L.Error(err, "http - v1 - GetBinanceKeys")
		common.ResponseWithError(c, http.StatusInternalServerError, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    depositLogs,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}
