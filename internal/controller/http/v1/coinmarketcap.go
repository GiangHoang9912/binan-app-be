package v1

import (
	"binan/internal/controller/http/common"
	"binan/pkg/logger"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type CmcRoutes struct {
	L logger.Interface
}

func newCmcRoutes(handler *gin.RouterGroup, l logger.Interface, v *validator.Validate) {
	r := &CmcRoutes{l}

	h := handler.Group("/coinmarketcap")
	h.GET("/listing", r.GetListing)
	h.GET("/logo/:id", r.GetListing)
	h.GET("/chart/:id", r.GetListing)
}

func (cr *CmcRoutes) GetListing(c *gin.Context) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodGet, "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest", nil)
	if err != nil {
		common.ResponseWithError(c, http.StatusInternalServerError, "")
	}
	req.Header.Add("X-CMC_PRO_API_KEY", "a6e404fd-8111-4cce-8ac1-8e44f37e8302")
	req.Header.Add("CMC_PRO_API_KEY", "a6e404fd-8111-4cce-8ac1-8e44f37e8302")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("No response from request")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body) // response body is []byte

	var result CmcListingRes
	if err := json.Unmarshal(body, &result); err != nil { // Parse []byte to the go struct pointer
		fmt.Println("Can not unmarshal JSON")
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    result,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}

type CmcListingRes struct {
	Status struct {
		Timestamp    time.Time   `json:"timestamp"`
		ErrorCode    int         `json:"error_code"`
		ErrorMessage interface{} `json:"error_message"`
		Elapsed      int         `json:"elapsed"`
		CreditCount  int         `json:"credit_count"`
		Notice       interface{} `json:"notice"`
		TotalCount   int         `json:"total_count"`
	} `json:"status"`
	Data []struct {
		ID                            int         `json:"id"`
		Name                          string      `json:"name"`
		Symbol                        string      `json:"symbol"`
		Slug                          string      `json:"slug"`
		NumMarketPairs                int         `json:"num_market_pairs"`
		DateAdded                     time.Time   `json:"date_added"`
		Tags                          []string    `json:"tags"`
		MaxSupply                     int         `json:"max_supply"`
		CirculatingSupply             int         `json:"circulating_supply"`
		TotalSupply                   int         `json:"total_supply"`
		Platform                      interface{} `json:"platform"`
		CmcRank                       int         `json:"cmc_rank"`
		SelfReportedCirculatingSupply interface{} `json:"self_reported_circulating_supply"`
		SelfReportedMarketCap         interface{} `json:"self_reported_market_cap"`
		TvlRatio                      interface{} `json:"tvl_ratio"`
		LastUpdated                   time.Time   `json:"last_updated"`
		Quote                         struct {
			Usd struct {
				Price                 float64     `json:"price"`
				Volume24H             float64     `json:"volume_24h"`
				VolumeChange24H       float64     `json:"volume_change_24h"`
				PercentChange1H       float64     `json:"percent_change_1h"`
				PercentChange24H      float64     `json:"percent_change_24h"`
				PercentChange7D       float64     `json:"percent_change_7d"`
				PercentChange30D      float64     `json:"percent_change_30d"`
				PercentChange60D      float64     `json:"percent_change_60d"`
				PercentChange90D      float64     `json:"percent_change_90d"`
				MarketCap             float64     `json:"market_cap"`
				MarketCapDominance    float64     `json:"market_cap_dominance"`
				FullyDilutedMarketCap float64     `json:"fully_diluted_market_cap"`
				Tvl                   interface{} `json:"tvl"`
				LastUpdated           time.Time   `json:"last_updated"`
			} `json:"USD"`
		} `json:"quote"`
	} `json:"data"`
}
