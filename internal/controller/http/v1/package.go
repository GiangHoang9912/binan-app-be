package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type PackageRoutes struct {
	PU usecase.IPackage
	L  logger.Interface
	V  *validator.Validate
}

func newPackageRoutes(handler *gin.RouterGroup, pu usecase.IPackage, l logger.Interface, v *validator.Validate) {
	r := &PackageRoutes{pu, l, v}

	h := handler.Group("/packages")
	h.GET("/", r.GetAllPackages)
	h.Use(middlewares.JwtMiddleware).GET("/purchased", r.GetPurchasedPackages)
}

// GetAllPackages
// @Summary     get all packages
// @Description
// @ID          get-all-packages
// @Tags  	    package
// @Produce json
// @Success     200 {object} common.Response
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /packages [get]
func (pr *PackageRoutes) GetAllPackages(c *gin.Context) {
	packages, status, err := pr.PU.GetAllPackages(c.Request.Context())
	if err != nil {
		pr.L.Error(err, "http - v1 - GetAllPackages")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    packages,
	}
	common.ResponseWithData(c, status, res)
}

// GetPurchasedPackages
// @Summary     get purchased packages
// @Description
// @ID          get purchased packages
// @Tags  	    package
// @Produce json
// @Success     200 {object} entity.UserPackage
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /packages/purchased [get]
func (pr *PackageRoutes) GetPurchasedPackages(c *gin.Context) {
	userId, _ := c.Get("user_id")
	packages, status, err := pr.PU.GetPurchasedPackages(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		pr.L.Error(err, "http - v1 - GetAllPackages")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    packages,
	}
	common.ResponseWithData(c, status, res)
}
