package v1

import (
	"binan/internal/constants"
	"binan/internal/controller/http/common"
	"binan/internal/controller/http/middlewares"
	"binan/internal/entity"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
)

type BinanceKeysRoutes struct {
	BK usecase.IBinanceKeys
	L  logger.Interface
	V  *validator.Validate
}

func newBinanceKeysRoutes(handler *gin.RouterGroup, BK usecase.IBinanceKeys, l logger.Interface, v *validator.Validate) {
	r := &BinanceKeysRoutes{BK, l, v}

	h := handler.Group("/binance-keys")
	h.Use(middlewares.JwtMiddleware)
	h.POST("/", r.SaveBinanceKey)
	h.GET("/", r.GetBinanceKey)
}

// SaveBinanceKey
// @Summary     create-capital-manage
// @Description
// @ID          create-capital-manage
// @Tags  	    capital-manage
// @Produce json
// @Success     200 {object} entity.CapitalManagements
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /capital-managements [post]
func (bkr *BinanceKeysRoutes) SaveBinanceKey(c *gin.Context) {
	var body entity.SaveBinanceKeyInput
	err1 := c.ShouldBindJSON(&body)
	if err1 != nil {
		bkr.L.Error(err1, "http - v1 - CreateCapitalManagements")
		common.ResponseWithError(c, http.StatusBadRequest, constants.UserInfoErrorMessage)
		return
	}
	err := bkr.V.Struct(body)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, _ := c.Get("user_id")

	BinanceKeys, status, err := bkr.BK.SaveBinanceKey(c.Request.Context(), body, uint(userId.(int)))
	if err != nil {
		bkr.L.Error(err, "http - v1 - CreateCapitalManagements")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    BinanceKeys,
	}
	common.ResponseWithData(c, status, res)
}

// GetBinanceKey
// @Summary     create-capital-manage
// @Description
// @ID          create-capital-manage
// @Tags  	    capital-manage
// @Produce json
// @Success     200 {object} entity.CapitalManagements
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /capital-managements [post]
func (bkr *BinanceKeysRoutes) GetBinanceKey(c *gin.Context) {
	userId, _ := c.Get("user_id")

	BinanceKeys, status, err := bkr.BK.GetBinanceKey(c.Request.Context(), uint(userId.(int)))
	if err != nil {
		bkr.L.Error(err, "http - v1 - CreateCapitalManagements")
		common.ResponseWithError(c, status, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    BinanceKeys,
	}
	common.ResponseWithData(c, status, res)
}
