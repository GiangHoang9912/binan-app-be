package v1

import (
	"binan/internal/controller/http/common"
	"binan/internal/usecase"
	"binan/pkg/logger"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type LevelRoutes struct {
	PU usecase.ILevel
	L  logger.Interface
	V  *validator.Validate
}

func newLevelRoutes(handler *gin.RouterGroup, pu usecase.ILevel, l logger.Interface, v *validator.Validate) {
	r := &LevelRoutes{pu, l, v}

	h := handler.Group("/levels")
	h.GET("/", r.GetAllLevels)
}

// GetAllLevels
// @Summary     get all levels
// @Description
// @ID          get-all-levels
// @Tags  	    level
// @Produce json
// @Success     200 {object} common.Response
// @Failure     400 {object} common.Response
// @Failure     401 {object} common.Response
// @Failure     500 {object} common.Response
// @Router      /levels [get]
func (pr *LevelRoutes) GetAllLevels(c *gin.Context) {
	levels, err := pr.PU.GetAllLevels(c.Request.Context())
	if err != nil {
		pr.L.Error(err, "http - v1 - GetAllLevels")
		common.ResponseWithError(c, 500, err.Error())
		return
	}

	res := common.Response{
		Success: true,
		Message: "",
		Data:    levels,
	}
	common.ResponseWithData(c, http.StatusOK, res)
}
