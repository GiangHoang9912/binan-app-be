package middlewares

import (
	"binan/config"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func CORSMiddleware(c *gin.Context) {
	cfg, _ := config.Get()
	origin := c.Request.Header.Get("Origin")
	if !strings.Contains(cfg.AllowedOrigins, origin) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}
	c.Writer.Header().Set("Access-Control-Allow-Origin", origin)
	c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

	if c.Request.Method == "OPTIONS" {
		c.AbortWithStatus(http.StatusNoContent)
		return
	}

	c.Next()
}
