package middlewares

import (
	"binan/internal/constants"
	"binan/internal/controller/http/common"
	"binan/internal/enums"
	"binan/utils"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
	"strings"
)

type authHeader struct {
	Authorization string `header:"Authorization"`
}

func JwtMiddleware(c *gin.Context) {
	accessToken, err := c.Cookie("accessToken")
	fmt.Println("cookie accessToken", accessToken)
	if err != nil {
		h := authHeader{}

		// bind Authorization Header to h and check for validation errors
		if err := c.ShouldBindHeader(&h); err != nil {
			v := validator.New()
			err = v.Struct(h)
			if err := v.Struct(h); err != nil {
				common.ResponseWithError(c, http.StatusInternalServerError, err.Error())
				c.Abort()
				return
			}

			// otherwise error type is unknown
			common.ResponseWithError(c, http.StatusInternalServerError, constants.InternalServerErrorMessage)
			c.Abort()
			return
		}

		arr := strings.Split(h.Authorization, "Bearer ")

		if len(arr) < 2 {
			err := errors.New("must provide Authorization header with format `Bearer {token}`")
			common.ResponseWithError(c, http.StatusBadRequest, err.Error())
			c.Abort()
			return
		}
		accessToken = arr[1]
	}

	user, err := utils.ValidateToken(accessToken)
	if err != nil {
		common.ResponseWithError(c, http.StatusBadRequest, err.Error())
		c.Abort()
		return
	}
	userData := user.Data.(map[string]any)

	c.Set("user", user)
	c.Set("user_id", int(userData["id"].(float64)))
	c.Set("user_email", userData["email"].(string))
	c.Set("user_role", userData["role"].(string))
	c.Next()
}

func ProtectedMiddleware(c *gin.Context) {
	role, _ := c.Get("user_role")
	if role != string(enums.Admin) {
		common.ResponseWithError(c, http.StatusForbidden, constants.ForbiddenMessage)
		c.Abort()
		return
	}
	c.Next()
}
