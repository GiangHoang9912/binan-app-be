package common

import (
	"binan/internal/usecase"
	"github.com/gin-gonic/gin"
)

type Response struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
	Data    any    `json:"data"`
}

type errorResponse Response

func ResponseWithError(c *gin.Context, code usecase.Status, message string) {
	c.AbortWithStatusJSON(int(code), errorResponse{
		Success: false,
		Message: message,
		Data:    nil,
	})
}

func ResponseWithData(c *gin.Context, code usecase.Status, data interface{}) {
	c.JSON(int(code), data)
}
