package enums

type UserRole string

const (
	Admin  UserRole = "ADMIN"
	Master UserRole = "MASTER"
	User   UserRole = "USER"
)

type CoinType string

const (
	BTC  CoinType = "BTC"
	ETH  CoinType = "ETH"
	BNB  CoinType = "BNB"
	USDT CoinType = "USDT"
)

type Subscription string

const (
	Weekly  Subscription = "WEEKLY"
	Monthly Subscription = "MONTHLY"
	Yearly  Subscription = "YEARLY"
)

type OrderStatus string

const (
	Pending   OrderStatus = "PENDING"
	Cancelled OrderStatus = "CANCELLED"
	Rejected  OrderStatus = "REJECTED"
	Completed OrderStatus = "COMPLETED"
)

type WalletType string

const (
	Future     WalletType = "FUTURE"      //ALL
	BinanTrade WalletType = "BINAN_TRADE" //Trade
	Ensurance  WalletType = "ENSURANCE"   // Phí đảm bảo
	Rewarding  WalletType = "REWARDING"   // + vào đây
)

type CurrencyType string

const (
	Usd CurrencyType = "USD"
	Vnd CurrencyType = "VND"
)

type DepositStatus string

type TransactionStatus string

const (
	Success TransactionStatus = "SUCCESS"
	Failed  TransactionStatus = "FAILED"
)
