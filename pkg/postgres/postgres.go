// Package postgres implements postgres connection.
package postgres

import (
	"binan/config"
	"context"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"time"

	"github.com/Masterminds/squirrel"
)

const (
	_defaultMaxPoolSize  = 1
	_defaultConnAttempts = 10
	_defaultConnTimeout  = time.Second
)

type Filter struct {
	Field string      `json:"field"`
	Op    string      `json:"op"`
	Value interface{} `json:"value"`
}

type Pagination struct {
	Limit  uint64 `json:"limit"`
	Offset uint64 `json:"offset"`
}

type OrderBy struct {
	Field string `json:"field"`
}

type GetManyRequestBody struct {
	Filters    []Filter    `json:"filters"`
	Pagination *Pagination `json:"pagination"`
	OrderBy    []OrderBy   `json:"orderBy"`
}

// Postgres -.
type Postgres struct {
	maxPoolSize  int
	connAttempts int
	connTimeout  time.Duration

	Db *gorm.DB
}

// New -.
func New(opts ...Option) (*Postgres, error) {
	cfg, _ := config.Get()
	pg := &Postgres{
		maxPoolSize:  _defaultMaxPoolSize,
		connAttempts: _defaultConnAttempts,
		connTimeout:  _defaultConnTimeout,
	}

	// Custom options
	for _, opt := range opts {
		opt(pg)
	}

	var err error
	//dsn := "host=localhost user=postgres password=backhug dbname=gorm port=5432 sslmode=disable"
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s",
		cfg.DbHost, cfg.DbUser, cfg.DbPwd, cfg.DbName, cfg.DbPort,
	)

	for pg.connAttempts > 0 {
		pg.Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
		if err == nil {
			break
		}

		log.Printf("Postgres is trying to connect, attempts left: %d", pg.connAttempts)

		time.Sleep(pg.connTimeout)

		pg.connAttempts--
	}

	//err = pg.Db.AutoMigrate(&entity.User{})
	sqlDb, _ := pg.Db.DB()
	sqlDb.SetMaxIdleConns(10)
	sqlDb.SetMaxOpenConns(100)

	if err != nil {
		return nil, fmt.Errorf("postgres - NewPostgres - connAttempts == 0: %w", err)
	}

	return pg, nil
}

// Close -.
func (p *Postgres) Close() {
	if p.Db != nil {
		sqlDB, _ := p.Db.DB()
		_ = sqlDB.Close()
	}
}

// Create -.
func (p *Postgres) Create(ctx context.Context, record any) error {
	result := p.Db.WithContext(ctx).Create(record)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

// Update -.
func (p *Postgres) Update(ctx context.Context, record any) error {
	result := p.Db.WithContext(ctx).Updates(record)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (p *Postgres) GetById(ctx context.Context, id uint, data any) error {
	res := p.Db.WithContext(ctx).First(data, id)
	return res.Error
}

func (p *Postgres) GetByIds(data *any, ids []uint) error {
	res := p.Db.First(data, ids)
	return res.Error
}

func (p *Postgres) WithFilters(selectBuilder squirrel.SelectBuilder, filters []Filter) squirrel.SelectBuilder {
	for _, filter := range filters {
		switch filter.Op {
		case "eq":
			selectBuilder = selectBuilder.Where(filter.Field+" = ?", filter.Value)
		case "neq":
			selectBuilder = selectBuilder.Where(filter.Field+" != ?", filter.Value)
		case "gt":
			selectBuilder = selectBuilder.Where(filter.Field+" > ?", filter.Value)
		case "lt":
			selectBuilder = selectBuilder.Where(filter.Field+" < ?", filter.Value)
		case "gte":
			selectBuilder = selectBuilder.Where(filter.Field+" >= ?", filter.Value)
		case "lte":
			selectBuilder = selectBuilder.Where(filter.Field+" <= ?", filter.Value)
		//case "contains":
		//    selectBuilder = selectBuilder.Where(filter.Field+" @> ?", filter.Value)
		case "str-contains":
			selectBuilder = selectBuilder.Where(filter.Field+" like ?", "%"+filter.Value.(string)+"%")
		case "str-starts-with":
			selectBuilder = selectBuilder.Where(filter.Field+" like ?", filter.Value.(string)+"%")
		case "str-ends-with":
			selectBuilder = selectBuilder.Where(filter.Field+" like ?", "%"+filter.Value.(string))
		case "in":
			selectBuilder = selectBuilder.Where(filter.Field+" in ?", filter.Value)
		}
	}

	return selectBuilder
}

func (p *Postgres) WithSorting(selectBuilder squirrel.SelectBuilder, orderBy []OrderBy) squirrel.SelectBuilder {
	for _, order := range orderBy {
		selectBuilder = selectBuilder.OrderBy(order.Field)
	}

	return selectBuilder
}

func (p *Postgres) WithPagination(selectBuilder squirrel.SelectBuilder, pagination *Pagination) squirrel.SelectBuilder {
	if pagination == nil {
		pagination = &Pagination{
			Limit:  10,
			Offset: 0,
		}
	}
	if pagination.Offset > 0 {
		pagination.Offset -= 1
	}
	if pagination.Offset < 0 {
		pagination.Offset = 0
	}
	selectBuilder = selectBuilder.Limit(pagination.Limit).Offset(pagination.Offset)

	return selectBuilder
}

func (p *Postgres) GetAll(ctx context.Context, data any) error {
	res := p.Db.WithContext(ctx).Find(data)
	return res.Error
}
