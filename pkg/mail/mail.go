package mail

import (
	"binan/config"
	"bytes"
	"embed"
	"gopkg.in/gomail.v2"
	"html/template"
	"io/fs"
)

type EmailType string

const (
	Register       EmailType = "register"
	Activate2fa    EmailType = "activate2fa"
	ForgetPassword EmailType = "forget-password"
)

type EmailCode struct {
	Code string
}

type SendMailInput struct {
	Email string    `json:"email" validate:"required,email"`
	Type  EmailType `json:"type" validate:"required"`
}

var (
	RreRegConfirmationKey          = "pre-reg-confirmation"
	Activate2faVerificationCodeKey = "get-verification-code-key"
	ForgetPasswordKey              = "forget-password-key"
)

const (
	templatesDir = "templates"
	extension    = "/*.html"
)

var (
	//go:embed templates/*
	files     embed.FS
	templates map[string]*template.Template
)

func init() {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	tmplFiles, err := fs.ReadDir(files, templatesDir)
	if err != nil {
		return
	}

	for _, tmpl := range tmplFiles {
		if tmpl.IsDir() {
			continue
		}

		pt, err := template.ParseFS(files, templatesDir+"/"+tmpl.Name(), templatesDir+extension)
		if err != nil {
			return
		}

		templates[tmpl.Name()] = pt
	}
}

func Send(to []string, subject string, data any) error {
	var err error
	cfg, _ := config.Get()
	from, name, password := cfg.Mail.Mail, cfg.Mail.NAME, cfg.Mail.Password
	host := "smtp.gmail.com"
	port := 587

	t, _ := templates["mail.html"]

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, data); err != nil {
		return err
	}

	result := tpl.String()
	m := gomail.NewMessage()
	m.SetAddressHeader("From", from, name)
	m.SetHeader("Name", from)
	m.SetHeader("To", to...)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", result)
	//m.Attach("template.html") // attach whatever you want

	d := gomail.NewDialer(host, port, from, password)

	err = d.DialAndSend(m)
	return err
}
