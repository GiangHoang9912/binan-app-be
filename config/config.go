package config

import (
	"github.com/ilyakaznacheev/cleanenv"
)

type (
	// Config -.
	Config struct {
		App  `yaml:"app"`
		HTTP `yaml:"http"`
		Log  `yaml:"logger"`
		PG   `yaml:"postgres"`
		Redis
		Mail
		JWT            `yaml:"jwt"`
		GinMode        string `yaml:"ginMode" env:"GIN_MODE"`
		AllowedOrigins string `env:"ALLOWED_ORIGINS"`
		Ref            `yaml:"ref"`
	}

	Ref struct {
		MaxRef uint `env:"REF_MAX"`
	}

	// App -.
	App struct {
		Name    string `env:"APP_NAME"`
		Version string `env:"APP_VERSION"`
	}

	// HTTP -.
	HTTP struct {
		Port string `env:"PORT"`
	}

	// Log -.
	Log struct {
		Level string `env:"LOG_LEVEL"`
	}

	// PG -.
	PG struct {
		PoolMax int    `env:"PG_POOL_MAX"`
		Url     string `env:"PG_URL"`
		DbHost  string `env:"DB_HOST"`
		DbPort  string `env:"DB_PORT"`
		DbUser  string `env:"DB_USER"`
		DbPwd   string `env:"DB_PASSWORD"`
		DbName  string `env:"DB_NAME"`
	}

	// Redis -.
	Redis struct {
		RedisHost string `env:"REDIS_HOST"`
		RedisPort string `env:"REDIS_PORT"`
		RedisUser string `env:"REDIS_USER"`
		RedisPwd  string `env:"REDIS_PASSWORD"`
	}

	Mail struct {
		Mail     string `env:"MAIL"`
		NAME     string `env:"MAIL_NAME"`
		Password string `env:"MAIL_PASSWORD"`
	}

	JWT struct {
		SecretKey string `env:"JWT_SECRET_KEY"`
	}
)

func (c *Config) IsProd() bool {
	return len(c.GinMode) > 0 && c.GinMode != "debug"
}

var _cfg *Config

// Get returns app config.
func Get() (*Config, error) {
	if _cfg != nil {
		return _cfg, nil
	}
	cfg := &Config{}

	var err error
	_ = cleanenv.ReadConfig("./config/config.yml", cfg)

	err = cleanenv.ReadConfig(".env", cfg)

	err = cleanenv.ReadEnv(cfg)
	if err != nil {
		return nil, err
	}
	_cfg = cfg

	return cfg, nil
}

func init() {
	_, _ = Get()
}
