drop table if exists apikey_logs;
drop table if exists authen_codes;
drop table if exists copy_trades;
drop table if exists order_logs;
drop table if exists signal_histories;
drop table if exists user_packages;
drop table if exists orders;
drop table if exists packages;
drop table if exists bots;
drop table if exists users;
drop table if exists user_bonus_histories;
drop table if exists user_bots;
drop table if exists user_copy_trades;
drop table if exists user_follows;
drop table if exists config_settings;
drop table if exists botsettings;
drop table if exists bots;
drop type if exists user_role;
drop type if exists coin_type;
drop type if exists subscription_type;
drop type if exists order_status;
drop type if exists currency_type;
