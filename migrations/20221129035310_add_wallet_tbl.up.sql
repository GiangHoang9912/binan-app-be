create type wallet_type as enum ('FUTURE', 'BINAN_TRADE', 'REWARDING');

create type deposit_status as enum ('PENDING', 'CANCELLED', 'REJECTED', 'COMPLETED');

create table if not exists wallets
(
    id         serial primary key,
    user_id    integer                                 not null references users (id),
    type       wallet_type                             not null default 'BINAN_TRADE',
    balance    numeric       default 0,
    currency   currency_type default 'USD',

    created_at timestamp(3)  default CURRENT_TIMESTAMP not null,
    updated_at timestamp(3)  default CURRENT_TIMESTAMP not null,
    deleted_at timestamp(3)
);

create table if not exists deposit_logs
(
    id         serial primary key,
    wallet_id  integer                                  not null references wallets (id),
    value      numeric                                  not null,
    currency   currency_type  default 'USD',
    status     deposit_status default 'PENDING',

    created_at timestamp(3)   default CURRENT_TIMESTAMP not null,
    updated_at timestamp(3)   default CURRENT_TIMESTAMP not null,
    deleted_at timestamp(3)
);

create or replace function create_empty_wallet()
    returns trigger
    language PLPGSQL
    volatile
as
$$
begin
    if new.id is not null then
        insert into wallets (user_id, type, balance, currency) values (new.id, 'FUTURE', 0, 'USD');
        insert into wallets (user_id, type, balance, currency) values (new.id, 'BINAN_TRADE', 0, 'USD');
        insert into wallets (user_id, type, balance, currency) values (new.id, 'REWARDING', 0, 'USD');
    end if;
    return new;
end;
$$;

create trigger create_wallet__after_insert
    after insert
    on users
    for each row
execute procedure create_empty_wallet();
