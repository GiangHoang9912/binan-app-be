create table if not exists apikey_logs
(
    id         serial primary key,
    message    varchar(255)                           not null,

    created_at timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at timestamp(3)
);

create table if not exists authen_codes
(
    id          serial primary key,
    user_id     integer                                not null,
    code_verify varchar(255)                           not null,
    code_type   varchar(255) default 'REGISTER_EMAIL'::character varying,
    is_use      boolean      default false,
    expire_at   timestamp(3)                           not null,

    created_at  timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at  timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at  timestamp(3)
);

create table if not exists bots
(
    id                 serial primary key,
    name               varchar(255)                           not null,
    symbol             varchar(255)                           not null,
    base               varchar(255)                           not null,
    win_rate           float        default 0,
    profit_rate_7d     float        default 0,
    profit_rate_30d    float        default 0,
    number_of_register integer      default 0,
    daily_trade_number integer      default 10,
    is_active          boolean      default true,
    is_delete          boolean      default false,

    created_at         timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at         timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at         timestamp(3)
);

create table if not exists copy_trades
(
    id            serial primary key,
    symbol        varchar(255)                           not null,
    base          varchar(255)                           not null,
    entry_price   varchar(255)                           not null,
    sell_price    varchar(255),
    quantity      varchar(255)                           not null,
    trade_percent varchar(255),
    profit        varchar(255),
    stop_loss     numeric,
    funds         varchar(255),
    margin        varchar(255),
    order_type    varchar(255) default 'LIMIT_ORDER'::character varying,
    is_set_trade  boolean      default false,
    is_delete     boolean      default false,
    created_at    timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at    timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at    timestamp(3)
);

create table if not exists order_logs
(
    id         serial primary key,
    message    varchar(255)                           not null,
    symbol     varchar(255),
    base       varchar(255),
    order_type varchar(255),

    created_at timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at timestamp(3)
);

create table if not exists signal_histories
(
    id          serial primary key,
    bot_id      integer                                not null,
    symbol      varchar(255)                           not null,
    base        varchar(255)                           not null,
    message     text                                   not null,
    entry_1     numeric,
    entry_2     numeric,
    entry_3     numeric,
    tp1         numeric,
    tp2         numeric,
    tp3         numeric,
    tp4         numeric,
    tp5         numeric,
    tp6         numeric,
    tp7         numeric,
    tp8         numeric,
    tp9         numeric,
    tp10        numeric,
    tp11        numeric,
    entry_price numeric                                not null,
    sell_price  numeric,
    profit      numeric,
    stop_loss   numeric,
    margin      varchar(255),
    safety      varchar(255) default 'DAY'::character varying,
    is_delete   boolean      default false,

    created_at  timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at  timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at  timestamp(3)
);

create type USER_ROLE as enum ('ADMIN', 'MASTER', 'USER');

create table if not exists "users"
(
    id                                  serial primary key,
    email                               varchar(300),
    phone                               varchar(50),
    address                             text,
    password                            text                                   not null,
    role                                USER_ROLE                              NOT NULL DEFAULT 'USER',
    name                                varchar(100),
    google_id                           varchar(255),
    trade_account                       varchar(255),
    balance                             float        default 0,
    balance_usdt                        float        default 0,
    uid                                 varchar(255),
    api_key                             varchar(255),
    secret_key                          varchar(255),
    referral_code                       varchar(15) unique,
    f1                                  varchar(255),
    avenger_profit_percent              varchar(255),
    bonus_debt                          varchar(255) default '0'::character varying,
    total_bonus                         varchar(255) default '0'::character varying,
    total_profit                        varchar(255) default '0'::character varying,
    email_verified                      boolean      default false,
    phone_verified                      boolean      default false,
    authenticator_enabled               boolean      default false,
    otp_auth_backup_key                 varchar(100),
    otp_auth_secret                     varchar(100),
    otp_auth_url                        varchar(300),
    preventive_key_google_authenticator varchar(255),
    identity                            varchar(255),
    identity_verified                   boolean      default false,
    identity_verification_type          varchar(255) default 'PASSPORT'::character varying,
    identity_verification_image_front   varchar(255),
    identity_verification_image_back    varchar(255),
    country                             varchar(255),
    residential_address                 varchar(500),
    full_name                           varchar(255),
    birthday                            varchar(255),
    zipcode                             varchar(255),
    city                                varchar(255),
    avatar                              varchar(255),
    sign_in_type                        varchar(30),
    biometric_key                       varchar(255),
    receive_notice                      boolean      default false,
    biometric_enabled                   boolean      default false,
    face_id_enabled                     boolean      default false,
    active                              boolean      default true,
    email_authen_enabled                boolean      default false,
    phone_authen_enabled                boolean      default false,

    created_at                          timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at                          timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at                          timestamp(3)
);

create type COIN_TYPE as enum ('BTC', 'ETH', 'BNB', 'USDT');

create table if not exists packages
(
    id         serial primary key,
    bot_id     integer                                not null references bots (id),
    type       COIN_TYPE                              not null,
    pair_with  COIN_TYPE,
    prices     jsonb,

    created_at timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at timestamp(3)
);

create table if not exists user_packages
(
    id         serial primary key,
    user_id    integer      not null references users (id),
    package_id integer      not null references packages (id),
    expired_at timestamp(3),

    created_at timestamp(3) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(3) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(3)
);

create table if not exists user_bonus_histories
(
    id         serial primary key,
    inviter    varchar(255),
    email      varchar(255),
    symbol     varchar(255)                           not null,
    base       varchar(255)                           not null,
    bonus      varchar(255)                           not null,

    created_at timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at timestamp(3)
);

create table if not exists user_bots
(
    id              serial primary key,
    user_id         integer                                not null,
    bot_id          integer                                not null,
    bot_package     varchar(255) default 'Week'::character varying,
    payment_method  varchar(255) default 'CRYPTO'::character varying,
    trade_pair      varchar(255),
    active_btc      boolean      default false,
    active_eth      boolean      default false,
    active_bnb      boolean      default false,
    active_celo     boolean      default false,
    active_eos      boolean      default false,
    active_1inch    boolean      default false,
    active_alpha    boolean      default false,
    is_buy          boolean      default false,
    is_active_trade boolean      default false,
    is_auto_trade   boolean      default false,
    margin          integer,
    funds           varchar(255),
    funds_per_day   varchar(255),
    is_delete       boolean      default false,

    created_at      timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at      timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at      timestamp(3)
);

create table if not exists user_copy_trades
(
    id               serial primary key,
    user_id          integer                                not null,
    trader_type      varchar(255) default 'FAMOUS_TRADERS'::character varying,
    order_type       varchar(255) default 'FUTURE'::character varying,
    followers        integer      default 0,
    risk             integer,
    profit_rate      varchar(255) default '0'::character varying,
    profit_rate_30d  varchar(255) default '0'::character varying,
    investment_limit varchar(255) default '0'::character varying,

    created_at       timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at       timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at       timestamp(3)
);

create type subscription_type as enum ('WEEKLY', 'MONTHLY', 'YEARLY');

create type order_status as enum ('PENDING', 'CANCELLED', 'REJECTED', 'COMPLETED');

create type currency_type as enum ('USD', 'VND');

create table if not exists orders
(
    id             serial primary key,
    user_id        integer           not null references users (id),
    package_id     integer           not null references packages (id),

    price          numeric           not null,
    price_currency currency_type                default 'USD',
    total_months   int2              not null,
    type           subscription_type not null default 'MONTHLY',
    status         order_status      not null default 'PENDING',

    created_at     timestamp(3)      not null default CURRENT_TIMESTAMP,
    updated_at     timestamp(3)      not null default CURRENT_TIMESTAMP,
    deleted_at     timestamp(3)
);

create table if not exists user_follows
(
    id          serial primary key,
    user_id     integer                                not null,
    follower_id integer                                not null,

    created_at  timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at  timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at  timestamp(3)
);

create table if not exists config_settings
(
    id                         serial primary key,
    config_1d_top_bottom_ratio double precision default 0,
    config_h4_top_bottom_ratio double precision default 0,
    entry1                     double precision default 0,
    entry2                     double precision default 0,
    entry3                     double precision default 0,
    max                        double precision default 0,
    min                        double precision default 0,
    rebound_factor             double precision default 0,
    sl_ratio                   double precision default 0,
    top_bottom_ratio           double precision default 0,
    tp1                        double precision default 0,
    tp10                       double precision default 0,
    tp11                       double precision default 0,
    tp2                        double precision default 0,
    tp3                        double precision default 0,
    tp4                        double precision default 0,
    tp5                        double precision default 0,
    tp6                        double precision default 0,
    tp7                        double precision default 0,
    tp8                        double precision default 0,
    tp9                        double precision default 0,
    symbol                     text             default ''::text,
    max_hidden                 double precision default 0,
    min_hidden                 double precision default 0,

    created_at                 timestamp(3)     default CURRENT_TIMESTAMP not null,
    updated_at                 timestamp(3)     default CURRENT_TIMESTAMP not null,
    deleted_at                 timestamp(3)
);
