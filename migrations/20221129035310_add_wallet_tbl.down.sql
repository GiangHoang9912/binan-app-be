drop trigger if exists create_wallet__after_insert on users;
drop function if exists create_empty_wallet;
drop table if exists deposit_logs;
drop table if exists wallets;
drop type if exists wallet_type;
drop type if exists deposit_status;
