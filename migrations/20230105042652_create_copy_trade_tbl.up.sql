create table if not exists copy_trades_1
(
    id             serial primary key,
    user_id        integer      unique not null references users (id),
    value          numeric,
    daily_value    numeric,
    total_deposit  varchar(50),
    stop_loss_rate numeric,

    created_at     timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at     timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at     timestamp(3)
);

alter table user_follows
    rename column follower_id to following_id;
