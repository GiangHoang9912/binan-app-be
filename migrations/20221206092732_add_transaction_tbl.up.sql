create type transaction_status as enum ('SUCCESS', 'FAILED');

create table if not exists transactions
(
    id         serial primary key,
    user_id    integer                                      not null references users (id),
    package_id integer                                      not null references packages (id),
    value      numeric            default 0,
    currency   currency_type      default 'USD',
    status     transaction_status default 'SUCCESS',

    created_at timestamp(3)       default CURRENT_TIMESTAMP not null,
    updated_at timestamp(3)       default CURRENT_TIMESTAMP not null,
    deleted_at timestamp(3)
);

alter table deposit_logs
    add message varchar(255) default '',
    add code    varchar(15);
