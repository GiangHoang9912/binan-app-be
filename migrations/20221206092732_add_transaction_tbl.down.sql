drop table if exists transactions;
drop type if exists transaction_status;

alter table deposit_logs
    drop message,
    drop code;
