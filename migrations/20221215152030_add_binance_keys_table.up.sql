create table binance_keys
(
    id         serial primary key,
    api_key    text,
    secret_key text,
    user_id    integer
        unique
        constraint binance_keys_users_id_fk
            references users
            on delete cascade,

    created_at timestamp(3) default CURRENT_TIMESTAMP,
    updated_at timestamp(3) default CURRENT_TIMESTAMP,
    deleted_at timestamp(3) default CURRENT_TIMESTAMP
);

create index binance_keys_id_index
    on binance_keys (id);

create unique index binance_keys_id_uindex
    on binance_keys (id);

create index binance_keys_user_id_index
    on binance_keys (user_id);

create unique index binance_keys_user_id_uindex
    on binance_keys (user_id);
