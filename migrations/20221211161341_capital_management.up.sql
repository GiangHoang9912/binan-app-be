create table if not exists capital_managements
(
    id              serial primary key,
    order_value     numeric,
    margin          numeric,
    entry           numeric,
    tp              numeric,
    stop_loss       numeric,
    limit_value     numeric,
    user_id         integer                                not null references users (id),
    user_package_id integer                                null references packages (id),
    disabled        boolean      default false,

    created_at      timestamp(3) default CURRENT_TIMESTAMP not null,
    updated_at      timestamp(3) default CURRENT_TIMESTAMP not null,
    deleted_at      timestamp(3)
);
